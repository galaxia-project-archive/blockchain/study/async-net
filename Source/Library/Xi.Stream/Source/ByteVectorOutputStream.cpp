// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/ByteVectorOutputStream.hpp"

#include <Xi/ErrorModel.hh>

Xi::Stream::ByteVectorOutputStream::ByteVectorOutputStream(ByteVector &dest) : m_dest{dest} {
}

Xi::Result<size_t> Xi::Stream::ByteVectorOutputStream::write(ConstByteSpan buffer) {
  m_dest.insert(m_dest.end(), buffer.data(), buffer.data() + buffer.size());
  return makeSuccess(buffer.size());
}

Xi::Result<void> Xi::Stream::ByteVectorOutputStream::flush() {
  return makeSuccess();
}
