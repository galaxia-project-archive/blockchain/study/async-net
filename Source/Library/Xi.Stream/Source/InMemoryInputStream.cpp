// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/InMemoryInputStream.hpp"

#include <cstring>
#include <algorithm>

#include "Xi/Stream/StreamError.hpp"

namespace Xi {
namespace Stream {

InMemoryInputStream::InMemoryInputStream(ConstByteSpan source) : m_source{source}, m_pos{0} {
}

Result<size_t> InMemoryInputStream::read(ByteSpan buffer) {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  if (*isEos) {
    return makeSuccess<size_t>(0U);
  }

  size_t size = std::min(buffer.size(), m_source.size() - m_pos);
  std::memcpy(buffer.data(), m_source.data() + m_pos, static_cast<size_t>(size));
  m_pos += size;
  return makeSuccess(static_cast<size_t>(size));
}

Result<bool> InMemoryInputStream::isEndOfStream() const {
  const auto pos = tell();
  XI_ERROR_PROPAGATE(pos)
  return makeSuccess(*pos == m_source.size());
}

Result<size_t> InMemoryInputStream::tell() const {
  return makeSuccess(m_pos);
}

Result<Byte> InMemoryInputStream::peek() const {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  return makeSuccess(m_source[m_pos]);
}

Result<Byte> InMemoryInputStream::take() {
  const auto isEos = isEndOfStream();
  XI_ERROR_PROPAGATE(isEos)
  XI_FAIL_IF(*isEos, StreamError::EndOfStream)
  return makeSuccess(m_source[m_pos++]);
}

}  // namespace Stream
}  // namespace Xi
