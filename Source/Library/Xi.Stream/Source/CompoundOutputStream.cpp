// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Stream/CompoundOutputStream.hpp"

#include <utility>

#include <Xi/ErrorModel.hh>

#include "Xi/Stream/StreamError.hpp"

Xi::Stream::CompoundOutputStream::CompoundOutputStream(std::vector<Xi::Stream::UniqueOutputStream> dests) {
  for (auto& dest : dests) {
    XI_EXCEPTIONAL_IF_NOT(NullArgumentError, dest);
    m_dests.emplace_back(std::move(dest));
  }
}

Xi::Result<size_t> Xi::Stream::CompoundOutputStream::write(Xi::ConstByteSpan buffer) {
  XI_SUCCEED_IF(buffer.empty(), 0u);
  XI_SUCCEED_IF(m_dests.empty(), 0u);
  size_t nWritten = 0;
  do {
    const auto iWritten = m_dests.front()->write(buffer);
    XI_ERROR_PROPAGATE(iWritten)
    if (*iWritten == 0) {
      m_dests.pop_front();
      if (nWritten > 0) {
        break;
      } else {
        continue;
      }
    }
    buffer = buffer.slice(nWritten);
    if (buffer.empty()) {
      break;
    } else if (nWritten > 0) {
      break;
    }
  } while (!m_dests.empty());
  return makeSuccess(nWritten);
}

Xi::Result<void> Xi::Stream::CompoundOutputStream::flush() {
  XI_FAIL_IF(m_dests.empty(), StreamError::EndOfStream);
  return m_dests.front()->flush();
}
