// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Ownership.hpp>

#include <istream>

#include "Xi/Stream/InputStream.hpp"

namespace Xi {
namespace Stream {
class IStream final : public InputStream {
 public:
  explicit IStream(Ownership<std::istream> source);
  XI_DELETE_COPY(IStream)
  XI_DEFAULT_MOVE(IStream)
  ~IStream() override = default;

  [[nodiscard]] Result<size_t> read(ByteSpan buffer) override;
  [[nodiscard]] Result<bool> isEndOfStream() const override;
  [[nodiscard]] Result<size_t> tell() const override;
  [[nodiscard]] Result<Byte> peek() const override;
  [[nodiscard]] Result<Byte> take() override;

 private:
  mutable Ownership<std::istream> m_source;
};

XI_DECLARE_SMART_POINTER(IStream)
}  // namespace Stream
}  // namespace Xi
