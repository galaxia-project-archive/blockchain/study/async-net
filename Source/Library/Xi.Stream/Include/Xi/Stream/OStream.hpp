// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>
#include <Xi/Ownership.hpp>

#include <ostream>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {
class OStream final : public OutputStream {
 public:
  explicit OStream(Ownership<std::ostream> dest);
  XI_DELETE_COPY(OStream)
  XI_DEFAULT_MOVE(OStream)
  ~OStream() override;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

 private:
  Ownership<std::ostream> m_dest;
};

XI_DECLARE_SMART_POINTER(OStream)
}  // namespace Stream
}  // namespace Xi
