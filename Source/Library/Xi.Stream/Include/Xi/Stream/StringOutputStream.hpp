// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>

#include "Xi/Stream/OutputStream.hpp"

namespace Xi {
namespace Stream {
class StringOutputStream final : public OutputStream {
 public:
  explicit StringOutputStream(std::string &dest);
  XI_DELETE_COPY(StringOutputStream);
  XI_DEFAULT_MOVE(StringOutputStream);
  ~StringOutputStream() override = default;

  [[nodiscard]] Result<size_t> write(ConstByteSpan buffer) override;
  [[nodiscard]] Result<void> flush() override;

 private:
  std::string &m_dest;
};

XI_DECLARE_SMART_POINTER(StringOutputStream)
}  // namespace Stream
}  // namespace Xi
