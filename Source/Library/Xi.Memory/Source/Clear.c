// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Memory/Clear.hh"

#if defined(_WIN32)
#include <Windows.h>
#endif

void xi_memory_secure_clear(xi_byte_t *data, size_t length) {
#if defined(_WIN32)
  SecureZeroMemory(data, length);
#else
  volatile xi_byte_t *volatile pointer = (volatile xi_byte_t * volatile) data;
  size_t i = (size_t)0U;
  while (i < length) {
    pointer[i++] = 0U;
  }
#endif
}
