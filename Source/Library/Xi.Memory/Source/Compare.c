// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Memory/Compare.hh"

#include <Xi/Global.hh>

bool xi_memory_secure_compare(const xi_byte_t *lhs, const xi_byte_t *rhs, size_t length) {
  xi_byte_t acc = 0;
  for (size_t i = 0; i < length; ++i) {
    acc |= lhs[i] ^ rhs[i];
  }
  return acc == 0 ? true : false;
}
