// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Stream/InputStream.hpp>
#include <Xi/Stream/InMemoryInputStream.hpp>
#include <Xi/Serialization/Serializer.hpp>
#include <Xi/Serialization/InputSerializer.hpp>

namespace Xi {
namespace Serialization {
namespace Json {
class InputSerializer final : public Serialization::InputSerializer {
 public:
  explicit InputSerializer(Stream::InputStream& stream);
  ~InputSerializer() override;

  Format format() const override;

  Result<void> readInt8(std::int8_t& value, const Tag& nameTag) override;
  Result<void> readUInt8(std::uint8_t& value, const Tag& nameTag) override;
  Result<void> readInt16(std::int16_t& value, const Tag& nameTag) override;
  Result<void> readUInt16(std::uint16_t& value, const Tag& nameTag) override;
  Result<void> readInt32(std::int32_t& value, const Tag& nameTag) override;
  Result<void> readUInt32(std::uint32_t& value, const Tag& nameTag) override;
  Result<void> readInt64(std::int64_t& value, const Tag& nameTag) override;
  Result<void> readUInt64(std::uint64_t& value, const Tag& nameTag) override;

  Result<void> readBoolean(bool& value, const Tag& nameTag) override;

  Result<void> readFloat(float& value, const Tag& nameTag) override;
  Result<void> readDouble(double& value, const Tag& nameTag) override;

  Result<void> readTag(Tag& value, const Tag& nameTag) override;
  Result<void> readFlag(TagVector& value, const Tag& nameTag) override;

  Result<void> readString(std::string& value, const Tag& nameTag) override;
  Result<void> readBinary(ByteVector& out, const Tag& nameTag) override;

  Result<void> readBlob(ByteSpan out, const Tag& nameTag) override;

  Result<void> beginReadComplex(const Tag& nameTag) override;
  Result<void> endReadComplex() override;

  Result<void> beginReadVector(size_t& size, const Tag& nameTag) override;
  Result<void> endReadVector() override;

  Result<void> beginReadArray(size_t size, const Tag& nameTag) override;
  Result<void> endReadArray() override;

  Result<void> checkValue(bool& value, const Tag& nameTag) override;

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};
}  // namespace Json

template <typename _ValueT>
Result<_ValueT> fromJson(Stream::InputStream& stream) {
  Json::InputSerializer input{stream};
  Serializer ser{input};
  _ValueT reval{};
  XI_ERROR_PROPAGATE_CATCH(ser(reval, Tag::Null))
  XI_SUCCEED(std::move(reval))
}

template <typename _ValueT>
Result<_ValueT> fromJson(std::string& content) {
  Stream::InMemoryInputStream stream{asConstByteSpan(content)};
  return fromJson<_ValueT>(stream);
}

}  // namespace Serialization
}  // namespace Xi
