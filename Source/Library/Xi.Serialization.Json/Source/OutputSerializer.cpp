// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Serialization/Json/OutputSerializer.hpp"

#include <utility>
#include <numeric>

#include <Xi/Extern/Push.hh>
#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>
#include <Xi/Encoding/Base64.hh>
#include <Xi/Stream/InMemoryStreams.hpp>

#include "Xi/Serialization/Json/JsonError.hpp"

namespace Xi {
namespace Serialization {
namespace Json {

namespace {
struct JOutputStreamWrapper {
  using Ch = char;

  Stream::OutputStream& stream;

  JOutputStreamWrapper(Stream::OutputStream& _stream) : stream{_stream} {
  }
  void Put(Ch ch) {
    const auto ec = stream.writeStrict(ConstByteSpan{reinterpret_cast<Byte*>(&ch), sizeof(Ch)});
    if (ec.isError()) {
      throw RuntimeError{};
    }
  }
  void Flush() {
    const auto ec = stream.flush();
    if (ec.isError()) {
      throw RuntimeError{};
    }
  }

  [[noreturn]] Ch Peek() const {
    throw NotImplementedError{};
  }
  [[noreturn]] Ch Take() {
    throw NotImplementedError{};
  }
  [[noreturn]] size_t Tell() {
    throw NotImplementedError{};
  }
  [[noreturn]] Ch* PutBegin() {
    throw NotImplementedError{};
  }
  [[noreturn]] size_t PutEnd(Ch*) {
    throw NotImplementedError{};
  }
};
}  // namespace

struct Serialization::Json::OutputSerializer::_Impl {
  JOutputStreamWrapper stream;
  rapidjson::Writer<JOutputStreamWrapper> writer;

  [[nodiscard]] Result<void> emplaceKey(const Tag& nameTag) {
    XI_EXCEPTIONAL_IF(InvalidSizeError, nameTag.text().size() > std::numeric_limits<rapidjson::SizeType>::max());
    if (!nameTag.isNull()) {
      XI_FAIL_IF_NOT(writer.Key(nameTag.text().data(), static_cast<rapidjson::SizeType>(nameTag.text().size()), true),
                     JsonError::Internal);
      XI_SUCCEED()
    } else {
      XI_SUCCEED()
    }
  }

  _Impl(Stream::OutputStream& native) : stream{native}, writer{stream} {
    /* */
  }
};

using JValue = rapidjson::Value;

Serialization::Json::OutputSerializer::OutputSerializer(Stream::OutputStream& stream) : m_impl{new _Impl{stream}} {
  /* */
}

Serialization::Json::OutputSerializer::~OutputSerializer() {
  /* */
}

Format OutputSerializer::format() const {
  return Format::HumanReadable;
}

Result<void> OutputSerializer::writeInt8(int8_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Int(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeUInt8(uint8_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Uint(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeInt16(int16_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Int(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeUInt16(uint16_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Uint(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeInt32(int32_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Int(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeUInt32(uint32_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Uint(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeInt64(int64_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Int64(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeUInt64(uint64_t value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Uint64(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeBoolean(bool value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Bool(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeFloat(float value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Double(static_cast<double>(value)), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeDouble(double value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Double(value), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeTag(const Serialization::Tag& value, const Tag& nameTag) {
  XI_FAIL_IF(value.text() == Tag::NoTextTag, JsonError::NullTag);
  return writeString(value.text(), nameTag);
}

Result<void> OutputSerializer::writeFlag(const Serialization::TagVector& flag, const Tag& nameTag) {
  XI_FAIL_IF(flag.size() > Tag::maximumFlags(), JsonError::FlagOverflow);
  XI_ERROR_PROPAGATE_CATCH(beginWriteVector(flag.size(), nameTag))
  for (const auto& iFlag : flag) {
    XI_ERROR_PROPAGATE_CATCH(writeTag(iFlag, Tag::Null))
  }
  XI_ERROR_PROPAGATE_CATCH(endWriteVector())
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeString(const std::string_view value, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF(nameTag.text().size() > std::numeric_limits<rapidjson::SizeType>::max(), JsonError::Internal);
  XI_FAIL_IF(value.size() > std::numeric_limits<rapidjson::SizeType>::max(), JsonError::Internal);
  XI_FAIL_IF_NOT(m_impl->writer.String(value.data(), static_cast<rapidjson::SizeType>(value.size())),
                 JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeBinary(ConstByteSpan value, const Tag& nameTag) {
  return writeString(Encoding::Base64::encode(value), nameTag);
}

Result<void> OutputSerializer::writeBlob(ConstByteSpan value, const Tag& nameTag) {
  return writeBinary(value, nameTag);
}

Result<void> OutputSerializer::beginWriteComplex(const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.StartObject(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::endWriteComplex() {
  XI_FAIL_IF_NOT(m_impl->writer.EndObject(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::beginWriteVector(size_t, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.StartArray(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::endWriteVector() {
  XI_FAIL_IF_NOT(m_impl->writer.EndArray(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::beginWriteArray(size_t, const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.StartArray(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::endWriteArray() {
  XI_FAIL_IF_NOT(m_impl->writer.EndArray(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeNull(const Tag& nameTag) {
  XI_ERROR_PROPAGATE_CATCH(m_impl->emplaceKey(nameTag));
  XI_FAIL_IF_NOT(m_impl->writer.Null(), JsonError::Internal);
  XI_SUCCEED()
}

Result<void> OutputSerializer::writeNotNull(const Tag&) {
  XI_SUCCEED()
}

}  // namespace Json
}  // namespace Serialization
}  // namespace Xi
