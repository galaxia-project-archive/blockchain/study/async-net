﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Endianess/Identity.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>
#include <stdlib.h>
#include <assert.h>

/*!
 * \brief Swaps the endianess of a given value inplace, mutating the provided value.
 * \param value The value to convert.
 */
static inline void xi_endianess_swap_u16_inplace(uint16_t* value) {
  assert(value);
  *value = ((uint16_t)((*value & 0x00FFU) << 8) | (uint16_t)((*value & 0xFF00U) >> 8));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline uint16_t xi_endianess_swap_u16(const uint16_t value) {
  return ((uint16_t)((value & 0x00FFU) << 8) | (uint16_t)((value & 0xFF00U) >> 8));
}

/*!
 * \brief Swaps the endianess of multiple value in a vector and inplace.
 * \param value A pointer to the array to convert.
 * \param count Number of values to convert.
 */
static inline void xi_endianess_swap_u16_vector(uint16_t* value, const size_t count) {
  assert(value);
  for (size_t i = 0; i < count; ++i) {
    xi_endianess_swap_u16_inplace(value + i);
  }
}

static inline void xi_endianess_swap_u32_inplace(uint32_t* value) {
  *value = ((uint32_t)((*value & 0x000000FFU) << 24) | (uint32_t)((*value & 0x0000FF00U) << 8) |
            (uint32_t)((*value & 0x00FF0000U) >> 8) | (uint32_t)((*value & 0xFF000000U) >> 24));
}

/*!
 * \brief Swaps the endianess of multiple value in a vector and inplace.
 * \param value A pointer to the array to convert.
 * \param count Number of values to convert.
 */
static inline void xi_endianess_swap_u32_vector(uint32_t* value, const size_t count) {
  assert(value);
  for (size_t i = 0; i < count; ++i) {
    xi_endianess_swap_u32_inplace(value + i);
  }
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline uint32_t xi_endianess_swap_u32(const uint32_t value) {
  return ((uint32_t)((value & 0x000000FFU) << 24) | (uint32_t)((value & 0x0000FF00U) << 8) |
          (uint32_t)((value & 0x00FF0000U) >> 8) | (uint32_t)((value & 0xFF000000U) >> 24));
}

static inline void xi_endianess_swap_u64_inplace(uint64_t* value) {
  *value = ((uint64_t)((*value & 0x000000FFU) << 24) | (uint64_t)((*value & 0x0000FF00U) << 8) |
            (uint64_t)((*value & 0x00FF0000U) >> 8) | (uint64_t)((*value & 0xFF000000U) >> 24));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline uint64_t xi_endianess_swap_u64(const uint64_t value) {
  return ((uint64_t)((value & 0x00000000000000FFULL) << 56) | (uint64_t)((value & 0x000000000000FF00ULL) << 40) |
          (uint64_t)((value & 0x0000000000FF0000ULL) << 24) | (uint64_t)((value & 0x00000000FF000000ULL) << 8) |
          (uint64_t)((value & 0x000000FF00000000ULL) >> 8) | (uint64_t)((value & 0x0000FF0000000000ULL) >> 24) |
          (uint64_t)((value & 0x00FF000000000000ULL) >> 40) | (uint64_t)((value & 0xFF00000000000000ULL) >> 56));
}

/*!
 * \brief Swaps the endianess of multiple value in a vector and inplace.
 * \param value A pointer to the array to convert.
 * \param count Number of values to convert.
 */
static inline void xi_endianess_swap_u64_vector(uint64_t* value, const size_t count) {
  assert(value);
  for (size_t i = 0; i < count; ++i) {
    xi_endianess_swap_u64_inplace(value + i);
  }
}

static inline void xi_endianess_swap_16_inplace(int16_t* value) {
  xi_endianess_swap_u16_inplace((uint16_t*)value);
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline int16_t xi_endianess_swap_16(const int16_t value) {
  return (int16_t)xi_endianess_swap_u16((uint16_t)value);
}

static inline void xi_endianess_swap_32_inplace(int32_t* value) {
  xi_endianess_swap_u32_inplace((uint32_t*)value);
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline int32_t xi_endianess_swap_32(const int32_t value) {
  return (int32_t)xi_endianess_swap_u32((uint32_t)value);
}

static inline void xi_endianess_swap_64_inplace(int64_t* value) {
  xi_endianess_swap_u64_inplace((uint64_t*)value);
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
static inline int64_t xi_endianess_swap_64(const int64_t value) {
  return (int64_t)xi_endianess_swap_u64((uint64_t)value);
}

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Endianess {

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr uint16_t swap(uint16_t value) {
  return ((uint16_t)((value & 0x00FFU) << 8) | (uint16_t)((value & 0xFF00U) >> 8));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr uint32_t swap(uint32_t value) {
  return ((uint32_t)((value & 0x000000FFU) << 24) | (uint32_t)((value & 0x0000FF00U) << 8) |
          (uint32_t)((value & 0x00FF0000U) >> 8) | (uint32_t)((value & 0xFF000000U) >> 24));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr uint64_t swap(uint64_t value) {
  return ((uint64_t)((value & 0x00000000000000FFULL) << 56) | (uint64_t)((value & 0x000000000000FF00ULL) << 40) |
          (uint64_t)((value & 0x0000000000FF0000ULL) << 24) | (uint64_t)((value & 0x00000000FF000000ULL) << 8) |
          (uint64_t)((value & 0x000000FF00000000ULL) >> 8) | (uint64_t)((value & 0x0000FF0000000000ULL) >> 24) |
          (uint64_t)((value & 0x00FF000000000000ULL) >> 40) | (uint64_t)((value & 0xFF00000000000000ULL) >> 56));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr int16_t swap(int16_t value) {
  return static_cast<int16_t>(static_cast<uint16_t>(value));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr int32_t swap(int32_t value) {
  return static_cast<int32_t>(static_cast<uint32_t>(value));
}

/*!
 * \brief Swaps the endianess of the given value.
 * \param value The value to convert.
 * \return A value in converted endianess representation.
 */
[[nodiscard]] static inline constexpr int64_t swap(int64_t value) {
  return static_cast<int64_t>(static_cast<uint64_t>(value));
}

}  // namespace Endianess
}  // namespace Xi

#endif
