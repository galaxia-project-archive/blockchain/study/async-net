﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Endianess/Identity.hh"
#include "Xi/Endianess/Swap.hh"

#include "Xi/Endianess/Config/Config.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline uint16_t xi_endianess_big_u16(const uint16_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_u16(value);
#else
  return xi_endianess_swap_u16(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline uint32_t xi_endianess_big_u32(const uint32_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_u32(value);
#else
  return xi_endianess_swap_u32(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline uint64_t xi_endianess_big_u64(const uint64_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_u64(value);
#else
  return xi_endianess_swap_u64(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline int16_t xi_endianess_big_16(const int16_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_16(value);
#else
  return xi_endianess_swap_16(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline int32_t xi_endianess_big_32(const int32_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_32(value);
#else
  return xi_endianess_swap_32(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
static inline int64_t xi_endianess_big_64(const int64_t value) {
#if defined(XI_ENDIANESS_BIG)
  return xi_endianess_identity_64(value);
#else
  return xi_endianess_swap_64(value);
#endif
}

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Endianess {

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr uint16_t big(uint16_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr uint32_t big(uint32_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr uint64_t big(uint64_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr int16_t big(int16_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr int32_t big(int32_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

/*!
 * \brief Converts a native value into big endian representation.
 * \return Value in big endian representation.
 */
[[nodiscard]] static inline constexpr int64_t big(int64_t value) {
#if defined(XI_ENDIANESS_LITTLE)
  return swap(value);
#else
  return identity(value);
#endif
}

}  // namespace Endianess
}  // namespace Xi

#endif
