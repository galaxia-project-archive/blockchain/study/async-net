﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Ssl/Config.hpp"

#include <utility>

#if defined(_WIN32) && _WIN32
#include <wincrypt.h>
#endif

namespace Xi {
namespace Ssl {

Protocol Config::protocol() const {
  return m_protocol;
}

void Config::setProtocol(const Protocol protocol_) {
  m_protocol = protocol_;
}

VerifyMode Config::verifyMode() const {
  return m_verifyMode;
}

void Config::setVerifyMode(const VerifyMode verifyMode_) {
  m_verifyMode = verifyMode_;
}

const std::set<FileSystem::File> &Config::verifyFiles() const {
  return m_verifyFiles;
}

std::set<FileSystem::File> &Config::verifyFiles() {
  return m_verifyFiles;
}

const std::set<FileSystem::Directory> &Config::verifyPaths() const {
  return m_verifyPaths;
}

std::set<FileSystem::Directory> &Config::verifyPaths() {
  return m_verifyPaths;
}

std::optional<int> Config::verifyDepth() const {
  return m_verifyDepth;
}

void Config::setVerifyDepth(const std::optional<int> &depth) {
  m_verifyDepth = depth;
}

const std::optional<std::string> &Config::privateKeyPassword() const {
  return m_privateKeyPassword;
}

void Config::setPrivateKeyPassword(const std::optional<std::string> &privateKeyPassword_) {
  m_privateKeyPassword = privateKeyPassword_;
}

const std::optional<FileSystem::File> &Config::privateKey() const {
  return m_privateKey;
}

void Config::setPrivateKey(const std::optional<FileSystem::File> &file) {
  m_privateKey = file;
}

const std::optional<FileSystem::File> &Config::certificate() const {
  return m_certificate;
}

void Config::setCertificate(const std::optional<FileSystem::File> &file) {
  m_certificate = file;
}

const std::optional<FileSystem::File> &Config::certificateChain() const {
  return m_certificateChain;
}

void Config::setCertificateChain(const std::optional<FileSystem::File> &file) {
  m_certificateChain = file;
}

const std::optional<FileSystem::File> &Config::dhParam() const {
  return m_dhParam;
}

void Config::setDhParam(const std::optional<FileSystem::File> &file) {
  m_dhParam = file;
}

bool Config::rootStore() const {
  return m_rootStore;
}

void Config::setRootStore(bool isEnabled) {
  m_rootStore = isEnabled;
}

Result<void> Config::configure(Context &ctx) const {
  XI_ERROR_PROPAGATE_CATCH(ctx.setProtocols(protocol()))
  XI_ERROR_PROPAGATE_CATCH(ctx.setVerifyMode(verifyMode()))
  for (const auto &vf : verifyFiles()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.loadVerifyFile(vf))
  }
  for (const auto &vp : verifyPaths()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.addVerifyPath(vp))
  }
  if (verifyDepth()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setVerifyDepth(*verifyDepth()))
  }
  if (privateKeyPassword()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setPrivateKeyPassword(*privateKeyPassword()))
  }
  if (privateKey()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setPrivateKey(*privateKey()))
  }
  if (certificate()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setCertificate(*certificate()))
  }
  if (certificateChain()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setCertificateChain(*certificateChain()))
  }
  if (dhParam()) {
    XI_ERROR_PROPAGATE_CATCH(ctx.setDhParam(*dhParam()))
  }

  if (rootStore()) {
#if defined(_WIN32) && _WIN32
    HCERTSTORE hStore = CertOpenSystemStore(0, "ROOT");
    if (hStore != nullptr) {
      X509_STORE *store = X509_STORE_new();
      PCCERT_CONTEXT pContext = nullptr;
      while ((pContext = CertEnumCertificatesInStore(hStore, pContext)) != nullptr) {
        X509 *x509 = d2i_X509(nullptr, const_cast<const unsigned char **>(std::addressof(pContext->pbCertEncoded)),
                              static_cast<long>(pContext->cbCertEncoded));
        if (x509 != nullptr) {
          X509_STORE_add_cert(store, x509);
          X509_free(x509);
        }
      }

      CertFreeCertificateContext(pContext);
      CertCloseStore(hStore, 0);

      SSL_CTX_set_cert_store(ctx.handle().native_handle(), store);
    }

#else
    boost::system::error_code ec{/* */};
    ctx.handle().set_default_verify_paths(ec);
    XI_ERROR_CODE_PROPAGATE(ec);
#endif
  }

  XI_SUCCEED()
}

Result<Context> Config::construct() const {
  auto reval = makeContext();
  XI_ERROR_PROPAGATE(reval);
  XI_ERROR_PROPAGATE_CATCH(configure(*reval));
  XI_SUCCEED(reval.take());
}

XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(Config)
XI_SERIALIZATION_MEMBER(m_protocol, 0x00001, "protocol")
XI_SERIALIZATION_MEMBER(m_verifyMode, 0x00002, "verify_mode")
XI_SERIALIZATION_MEMBER(m_verifyFiles, 0x00003, "verify_files")
XI_SERIALIZATION_MEMBER(m_verifyPaths, 0x00004, "verify_paths")
XI_SERIALIZATION_MEMBER(m_privateKeyPassword, 0x00005, "key_password")
XI_SERIALIZATION_MEMBER(m_privateKey, 0x00006, "key")
XI_SERIALIZATION_MEMBER(m_certificate, 0x00007, "cert")
XI_SERIALIZATION_MEMBER(m_certificateChain, 0x00008, "cert_chain")
XI_SERIALIZATION_MEMBER(m_dhParam, 0x00009, "dh_param")
XI_SERIALIZATION_MEMBER(m_verifyDepth, 0x0000A, "verify_depth")
XI_SERIALIZATION_MEMBER(m_rootStore, 0x0000B, "root_store")
XI_SERIALIZATION_COMPLEX_EXTERN_END

}  // namespace Ssl
}  // namespace Xi
