// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Ssl/Context.hpp"

#include <utility>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Ssl/Context")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Ssl, Context)
XI_ERROR_CODE_DESC(FileFormatUnknown, "could not deduce file format type")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Ssl {

Context::Context() {
  /* */
}

Protocol Context::protocols() const {
  return m_protocols;
}

Result<void> Context::enableProtocols(const Protocol protocols_) {
  boost::system::error_code ec{/* */};
  if (hasFlag(protocols_, Protocol::Ssl_v2)) {
    handle().clear_options(boost::asio::ssl::context::no_sslv2, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Ssl_v2;
  }
  if (hasFlag(protocols_, Protocol::Ssl_v3)) {
    handle().clear_options(boost::asio::ssl::context::no_sslv3, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Ssl_v3;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_0)) {
    handle().clear_options(boost::asio::ssl::context::no_tlsv1, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Tls_v1_0;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_1)) {
    handle().clear_options(boost::asio::ssl::context::no_tlsv1_1, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Tls_v1_1;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_2)) {
    handle().clear_options(boost::asio::ssl::context::no_tlsv1_2, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Tls_v1_2;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_3)) {
    handle().clear_options(boost::asio::ssl::context::no_tlsv1_3, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols |= Protocol::Tls_v1_3;
  }
  XI_SUCCEED()
}

Result<void> Context::disableProtocols(const Protocol protocols_) {
  boost::system::error_code ec{/* */};
  if (hasFlag(protocols_, Protocol::Ssl_v2)) {
    handle().set_options(boost::asio::ssl::context::no_sslv2, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Ssl_v2;
  }
  if (hasFlag(protocols_, Protocol::Ssl_v3)) {
    handle().set_options(boost::asio::ssl::context::no_sslv3, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Ssl_v3;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_0)) {
    handle().set_options(boost::asio::ssl::context::no_tlsv1, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Tls_v1_0;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_1)) {
    handle().set_options(boost::asio::ssl::context::no_tlsv1_1, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Tls_v1_1;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_2)) {
    handle().set_options(boost::asio::ssl::context::no_tlsv1_2, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Tls_v1_2;
  }
  if (hasFlag(protocols_, Protocol::Tls_v1_3)) {
    handle().set_options(boost::asio::ssl::context::no_tlsv1_3, ec);
    XI_ERROR_CODE_PROPAGATE(ec);
    m_protocols &= ~Protocol::Tls_v1_3;
  }
  XI_SUCCEED()
}

Result<void> Context::setProtocols(const Protocol protocols_) {
  XI_ERROR_PROPAGATE_CATCH(disableProtocols(~protocols_));
  XI_ERROR_PROPAGATE_CATCH(enableProtocols(protocols_));
  XI_SUCCEED()
}

const Context::boost_handle &Context::handle() const {
  return m_handle;
}

Context::boost_handle &Context::handle() {
  return m_handle;
}

Context::ssl_handle Context::nativeHandle() {
  return handle().native_handle();
}

Result<void> Context::setVerifyMode(const VerifyMode vmode) {
  XI_LOG_TRACE("Set verify mode to {}", vmode);
  boost::system::error_code ec{/* */};
  handle().set_verify_mode(toBoost(vmode), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Error setting verify mode to '{}': {}", vmode, makeFailure(ec));
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::loadVerifyFile(const FileSystem::File &file) {
  XI_LOG_TRACE("Loading verify file: {}", file)
  boost::system::error_code ec{/* */};
  handle().load_verify_file(toString(file), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to load verification file '{}': {}", file, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::addVerifyPath(const FileSystem::Directory &dir) {
  XI_LOG_TRACE("Verification path: '{}'", dir)
  boost::system::error_code ec{/* */};
  handle().add_verify_path(toString(dir), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to add verification path '{}': {}", dir, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setVerifyDepth(const int depth) {
  boost::system::error_code ec{/* */};
  handle().set_verify_depth(depth, ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set verification depth '{}': {}", depth, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED()
}

Result<void> Context::setPrivateKeyPassword(const std::string &str) {
  boost::system::error_code ec{/* */};
  handle().set_password_callback(
      [pkp = str](std::size_t, boost::asio::ssl::context_base::password_purpose) -> std::string { return pkp; }, ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Error setting private key password: {}", makeFailure(ec));
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setPrivateKey(ConstByteSpan buffer, const FileFormat format) {
  boost::system::error_code ec{/* */};
  handle().use_private_key(boost::asio::const_buffer{buffer.data(), buffer.size()}, toBoost(format), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set private key buffer: {}", makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setPrivateKey(const FileSystem::File &file, const FileFormat format) {
  XI_LOG_TRACE("Private key file: '{}'", file)
  boost::system::error_code ec{/* */};
  handle().use_private_key_file(toString(file), toBoost(format), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set private key file '{}': {}", file, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setPrivateKey(const FileSystem::File &file) {
  if (file.isFormat(Asn1FileFormat)) {
    return setPrivateKey(file, FileFormat::Asn1);
  } else if (file.isFormat(PemFileFormat)) {
    return setPrivateKey(file, FileFormat::Pem);
  } else {
    XI_FAIL(ContextError::FileFormatUnknown)
  }
}

Result<void> Context::setCertificate(ConstByteSpan buffer, const FileFormat format) {
  boost::system::error_code ec{/* */};
  handle().use_certificate(boost::asio::const_buffer{buffer.data(), buffer.size()}, toBoost(format), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set certificate buffer: {}", makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setCertificate(const FileSystem::File &file, const FileFormat format) {
  XI_LOG_TRACE("Certificate chain file: '{}'", file)
  boost::system::error_code ec{/* */};
  handle().use_certificate_file(toString(file), toBoost(format), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set certificate file '{}': {}", file, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setCertificate(const FileSystem::File &file) {
  if (file.isFormat(Asn1FileFormat)) {
    return setCertificate(file, FileFormat::Asn1);
  } else if (file.isFormat(PemFileFormat)) {
    return setCertificate(file, FileFormat::Pem);
  } else {
    XI_FAIL(ContextError::FileFormatUnknown)
  }
}

Result<void> Context::setCertificateChain(ConstByteSpan buffer) {
  boost::system::error_code ec{/* */};
  handle().use_certificate_chain(boost::asio::const_buffer{buffer.data(), buffer.size()}, ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set certificate chain buffer: {}", makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setCertificateChain(const FileSystem::File &file) {
  XI_LOG_TRACE("Certificate chain file: '{}'", file)
  boost::system::error_code ec{/* */};
  handle().use_certificate_chain_file(toString(file), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to set certificate chain file '{}': {}", file, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setDhParam(ConstByteSpan buffer) {
  XI_LOG_TRACE("Loading temp diffie hellman buffer");
  boost::system::error_code ec{/* */};
  handle().use_tmp_dh(boost::asio::const_buffer{buffer.data(), buffer.size()}, ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed loading temp diffie hellman buffer: {}", makeFailure(ec));
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<void> Context::setDhParam(const FileSystem::File &file) {
  XI_LOG_TRACE("Loading temp diffie hellman file: '{}'", file)
  boost::system::error_code ec{/* */};
  handle().use_tmp_dh_file(toString(file), ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed to load temp diffie hellman file '{}': {}", file, makeFailure(ec))
    return makeFailure(ec);
  }
  XI_SUCCEED();
}

Result<Context> makeContext() {
  Context context{/* */};
  boost::system::error_code ec{/* */};
  context.handle().set_options(boost::asio::ssl::context::default_workarounds, ec);
  if (ec.failed()) {
    XI_LOG_ERROR("Failed setting default workarounds on ssl context: {}", makeFailure(ec));
    return makeFailure(ec);
  }
  XI_ERROR_PROPAGATE_CATCH(context.setProtocols(Protocol::Default));
  return makeSuccess(std::move(context));
}

}  // namespace Ssl
}  // namespace Xi
