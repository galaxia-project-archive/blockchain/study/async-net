// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Ssl/VerifyMode.hpp"

#include <vector>
#include <string>

#include <Xi/String.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Ssl/VerifyMode")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Ssl, VerifyMode)
XI_ERROR_CODE_DESC(IllFormed, "ssl verify mode string representation is ill formed")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Ssl {

std::string stringify(const VerifyMode vm) {
  if (vm == VerifyMode::None) {
    return "none";
  }

  std::vector<std::string> flags{};
  flags.reserve(4);

  if (hasFlag(vm, VerifyMode::VerifyPeer)) {
    flags.emplace_back("verify_peer");
  }
  if (hasFlag(vm, VerifyMode::RequirePeerCert)) {
    flags.emplace_back("require_peer_cert");
  }
  if (hasFlag(vm, VerifyMode::ClientOnlyOnce)) {
    flags.emplace_back("client_only_once");
  }
  if (hasFlag(vm, VerifyMode::PostHandshake)) {
    flags.emplace_back("post_handshake");
  }

  return join(flags, ",");
}

Result<void> parse(const std::string &str, VerifyMode &out) {
  const auto relaxed = relax(str);
  if (relaxed == "none") {
    out = VerifyMode::None;
    XI_SUCCEED();
  }

  VerifyMode result = VerifyMode::None;
  const auto flags = split(relaxed, ",");
  for (const auto &flag : flags) {
    if (flag == toString(VerifyMode::VerifyPeer)) {
      result |= VerifyMode::VerifyPeer;
    } else if (flag == toString(VerifyMode::RequirePeerCert)) {
      result |= VerifyMode::RequirePeerCert;
    } else if (flag == toString(VerifyMode::ClientOnlyOnce)) {
      result |= VerifyMode::ClientOnlyOnce;
    } else if (flag == toString(VerifyMode::PostHandshake)) {
      result |= VerifyMode::PostHandshake;
    } else {
      XI_FAIL(VerifyModeError::IllFormed)
    }
  }
  out = result;

  XI_SUCCEED();
}

boost::asio::ssl::verify_mode toBoost(const VerifyMode vm) {
  namespace boost_ssl = boost::asio::ssl;
  static_assert(static_cast<VerifyMode>(boost_ssl::verify_none) == VerifyMode::None, "");
  static_assert(static_cast<VerifyMode>(boost_ssl::verify_peer) == VerifyMode::VerifyPeer, "");
  static_assert(static_cast<VerifyMode>(boost_ssl::verify_fail_if_no_peer_cert) == VerifyMode::RequirePeerCert, "");
  static_assert(static_cast<VerifyMode>(boost_ssl::verify_client_once) == VerifyMode::ClientOnlyOnce, "");
  static_assert(static_cast<VerifyMode>(SSL_VERIFY_POST_HANDSHAKE) == VerifyMode::PostHandshake, "");
  return static_cast<boost::asio::ssl::verify_mode>(vm);
}

}  // namespace Ssl
}  // namespace Xi
