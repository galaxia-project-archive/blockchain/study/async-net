// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include <Xi/Extern/Push.hh>
#include <boost/asio/ssl.hpp>
#include <Xi/Extern/Pop.hh>

#include <Xi/Byte.hh>
#include <Xi/Async/Mutex.hpp>
#include <Xi/FileSystem/FileSystem.hpp>

#include "Xi/Ssl/Protocol.hpp"
#include "Xi/Ssl/FileFormat.hpp"
#include "Xi/Ssl/VerifyMode.hpp"

namespace Xi {
namespace Ssl {

XI_ERROR_CODE_BEGIN(Context)
XI_ERROR_CODE_VALUE(FileFormatUnknown, 0x0101)
XI_ERROR_CODE_END(Context, "Ssl::ContextError")

class Context {
 public:
  using boost_handle = boost::asio::ssl::context;
  using ssl_handle = boost_handle::native_handle_type;

 public:
  XI_DELETE_COPY(Context)
  XI_DEFAULT_MOVE(Context)
  ~Context() = default;

  const boost_handle& handle() const;
  boost_handle& handle();
  ssl_handle nativeHandle();

  Protocol protocols() const;
  Result<void> enableProtocols(const Protocol protocols);
  Result<void> disableProtocols(const Protocol protocols);
  Result<void> setProtocols(const Protocol protocols);

  Result<void> setVerifyMode(const VerifyMode vmode);
  Result<void> loadVerifyFile(const FileSystem::File& file);
  Result<void> addVerifyPath(const FileSystem::Directory& dir);

  Result<void> setVerifyDepth(const int depth);

  Result<void> setPrivateKeyPassword(const std::string& str);
  Result<void> setPrivateKey(ConstByteSpan buffer, const FileFormat format);
  Result<void> setPrivateKey(const FileSystem::File& file, const FileFormat format);
  Result<void> setPrivateKey(const FileSystem::File& file);

  Result<void> setCertificate(ConstByteSpan buffer, const FileFormat format);
  Result<void> setCertificate(const FileSystem::File& file, const FileFormat format);
  Result<void> setCertificate(const FileSystem::File& file);

  Result<void> setCertificateChain(ConstByteSpan buffer);
  Result<void> setCertificateChain(const FileSystem::File& file);

  Result<void> setDhParam(ConstByteSpan buffer);
  Result<void> setDhParam(const FileSystem::File& file);

 private:
  Context();

  friend Result<Context> makeContext();

 private:
  Protocol m_protocols{Protocol::Default};
  boost_handle m_handle{boost_handle::method::tls};
};

Result<Context> makeContext();

}  // namespace Ssl
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Ssl, Context)
