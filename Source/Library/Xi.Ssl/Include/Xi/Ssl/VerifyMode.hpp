// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Flag.hpp>

#include <Xi/Extern/Push.hh>
#include <boost/asio/ssl/verify_mode.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Ssl {

XI_ERROR_CODE_BEGIN(VerifyMode)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(VerifyMode, "Ssl::VerifyModeError")

enum struct VerifyMode {
  None = 0,
  VerifyPeer = 1 << 0,
  RequirePeerCert = 1 << 1,
  ClientOnlyOnce = 1 << 2,
  PostHandshake = 1 << 3,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(VerifyMode)
XI_SERIALIZATION_FLAG(VerifyMode)

std::string stringify(const VerifyMode vm);
Result<void> parse(const std::string& str, VerifyMode& out);

boost::asio::ssl::verify_mode toBoost(const VerifyMode vm);

}  // namespace Ssl
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Ssl::VerifyMode, VerifyPeer, PostHandshake)
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::VerifyMode, VerifyPeer, "verify_peer")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::VerifyMode, RequirePeerCert, "require_peer_cert")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::VerifyMode, ClientOnlyOnce, "client_only_once")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::VerifyMode, PostHandshake, "post_handshake")

XI_ERROR_CODE_OVERLOADS(Xi::Ssl, VerifyMode)
