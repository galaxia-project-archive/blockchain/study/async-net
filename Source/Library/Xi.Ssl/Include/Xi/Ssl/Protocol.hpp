// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Flag.hpp>

namespace Xi {
namespace Ssl {

XI_ERROR_CODE_BEGIN(Protocol)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(Protocol, "Ssl::ProtocolError")

enum struct Protocol {
  None = 0,

  Ssl_v2 = 1 << 0,
  Ssl_v3 = 1 << 1,
  Tls_v1_0 = 1 << 2,
  Tls_v1_1 = 1 << 3,
  Tls_v1_2 = 1 << 4,
  Tls_v1_3 = 1 << 5,

  Ssl = Ssl_v2 | Ssl_v3,
  Tls = Tls_v1_0 | Tls_v1_1 | Tls_v1_2 | Tls_v1_3,

  Default = Tls_v1_2 | Tls_v1_3,
  Deprecated = Ssl | Tls_v1_0 | Tls_v1_1,
  Insecure = Ssl | Tls_v1_0,
};

[[nodiscard]] bool hasSslSupport(const Protocol protocol);
[[nodiscard]] bool hasTlsSupport(const Protocol protocol);
[[nodiscard]] bool hasDeprectaedSupport(const Protocol protocol);
[[nodiscard]] bool hasInsecureSupport(const Protocol protocol);

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(Protocol)
XI_SERIALIZATION_FLAG(Protocol)

std::string stringify(const Protocol protocol);
Result<void> parse(const std::string& str, Protocol& out);

}  // namespace Ssl
}  // namespace Xi

XI_SERIALIZATION_FLAG_RANGE(Xi::Ssl::Protocol, Ssl_v2, Tls_v1_3)
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Ssl_v2, "ssl_v2")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Ssl_v3, "ssl_v2")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Tls_v1_0, "tls_v1_0")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Tls_v1_1, "tls_v1_1")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Tls_v1_2, "tls_v1_2")
XI_SERIALIZATION_FLAG_TAG(Xi::Ssl::Protocol, Tls_v1_3, "tls_v1_3")

XI_ERROR_CODE_OVERLOADS(Xi::Ssl, Protocol)
