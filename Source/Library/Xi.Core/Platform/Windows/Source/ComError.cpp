// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <comdef.h>

#include "Xi/Windows/ComError.hpp"

namespace Xi::Windows {
ComErrorCategory ComErrorCategory::Instance{/* */};

const char *ComErrorCategory::name() const noexcept {
  return "ComError";
}

std::string ComErrorCategory::message(int hresult) const {
  return _com_error{hresult}.ErrorMessage();
}

std::error_condition ComErrorCategory::default_error_condition(int hresult) const noexcept {
  if (HRESULT_CODE(hresult) || hresult == 0) {
    return std::system_category().default_error_condition(HRESULT_CODE(hresult));
  }
  { return std::error_condition{hresult, ComErrorCategory::Instance}; }
}

ComError toComError(const HRESULT hresult) noexcept {
  return static_cast<ComError>(hresult);
}

}  // namespace Xi::Windows
