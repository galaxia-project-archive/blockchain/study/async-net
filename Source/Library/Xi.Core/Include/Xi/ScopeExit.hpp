// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <utility>

#include <Xi/Global.hh>

namespace Xi {

template <typename _FunctorT>
class ScopeExit {
 private:
  _FunctorT m_functor;

 public:
  explicit ScopeExit(_FunctorT&& functor) : m_functor(std::forward<_FunctorT>(functor)) {
    /* */
  }

  ~ScopeExit() {
    try {
      m_functor();
    } catch (...) {
      XI_PRINT_EC("scope desctructor threw.");
    }
  }
};

template <typename _FunctorT>
ScopeExit<_FunctorT> makeScopeExit(_FunctorT&& functor) {
  return ScopeExit<_FunctorT>(std::forward<_FunctorT>(functor));
}

}  // namespace Xi

#define XI_SCOPE_EXIT(STMT) \
  [[maybe_unused]] const auto XI_CONCATENATE(__ScopeExit, __LINE__) = ::Xi::makeScopeExit(STMT);
