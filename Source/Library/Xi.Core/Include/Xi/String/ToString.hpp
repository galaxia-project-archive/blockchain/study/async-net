// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <sstream>
#include <utility>
#include <ostream>
#include <type_traits>

#include "Xi/Extern/Push.hh"
#include <fmt/ostream.h>
#include "Xi/Extern/Pop.hh"

#include "Xi/TypeTrait/IsDetected.hpp"
#include "Xi/TypeTrait/IsSmartPointer.hpp"

#include "Xi/String/Stringify/Stringify.hpp"

namespace Xi {

template <typename _T, typename... _Ts>
inline std::string toString(const std::string& str, _T&& arg, _Ts&&... args) {
  return fmt::format(str, std::forward<_T>(arg), std::forward<_Ts>(args)...);
}

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_stringify_expression_v<_ValueT, _Ts...>, int> = 0>
inline std::string toString(const _ValueT& value, _Ts&&... args) {
  return stringify(value, std::forward<_Ts>(args)...);
}

template <typename _ValueT, typename... _Ts, std::enable_if_t<has_stringify_member_expression_v<_ValueT>, int> = 0>
inline std::string toString(const _ValueT& value, _Ts&&... args) {
  return value.stringify(std::forward<_Ts>(args)...);
}

template <typename _ValueT, std::enable_if_t<has_std_to_string_expression_v<_ValueT>, int> = 0>
std::string toString(const _ValueT& value) {
  return std::to_string(value);
}

template <typename _ValueT, std::enable_if_t<is_smart_pointer_v<_ValueT>, int> = 0>
inline std::string toString(const _ValueT& value) {
  if constexpr (is_weak_pointer_v<_ValueT>) {
    return toString(value.lock());
  } else {
    if (value.get() == nullptr) {
      return "null";
    } else {
      return toString(*value);
    }
  }
}

inline std::string toString(const std::string& value) {
  return value;
}

}  // namespace Xi
