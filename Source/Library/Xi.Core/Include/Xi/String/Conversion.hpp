﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

namespace Xi {

[[nodiscard]] std::string toMultiByteString(const wchar_t *wstring, const size_t size);
[[nodiscard]] std::string toMultiByteString(std::wstring_view wstring);
[[nodiscard]] std::string toMultiByteString(const wchar_t *wstring);
[[nodiscard]] std::string toMultiByteString(const std::wstring &wstring);

[[nodiscard]] std::wstring toWideCharacterString(const char *mbstring, const size_t size);
[[nodiscard]] std::wstring toWideCharacterString(std::string_view mbstring);
[[nodiscard]] std::wstring toWideCharacterString(const char *mbstring);
[[nodiscard]] std::wstring toWideCharacterString(const std::string &mbstring);

}  // namespace Xi
