// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string_view>

namespace Xi {

[[nodiscard]] bool startsWith(std::string_view str, std::string_view prefix);

}
