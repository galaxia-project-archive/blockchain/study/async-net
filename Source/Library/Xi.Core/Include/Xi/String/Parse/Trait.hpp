// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <utility>

#include "Xi/TypeTrait/IsDetected.hpp"

namespace Xi {

template <typename _ValueT, typename... _Ts>
using parse_member_expression_t = decltype(_ValueT::parse(std::declval<const std::string&>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_parse_member_expression_v = is_detected_v<parse_member_expression_t, _ValueT, _Ts...>;

template <typename _ValueT, typename... _Ts>
using parse_expression_t =
    decltype(parse(std::declval<const std::string&>(), std::declval<_ValueT&>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_parse_expression_v = is_detected_v<parse_expression_t, _ValueT, _Ts...>;

}  // namespace Xi
