// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <inttypes.h>
#include <stdio.h>

#define XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE 0x0001
#define XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW 0x0002
#define XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW 0x0003
#define XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE 0x0004
#define XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS 0x0005
#define XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER 0x0006
#define XI_STRING_PARSE_NUMBER_ERROR_INVALID_LOCALE 0x0007

int xi_string_parse_number_int8(int8_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_int16(int16_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_int32(int32_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_int64(int64_t *out, const char *string, const char *stringEnd, uint8_t base);

int xi_string_parse_number_uint8(uint8_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_uint16(uint16_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_uint32(uint32_t *out, const char *string, const char *stringEnd, uint8_t base);
int xi_string_parse_number_uint64(uint64_t *out, const char *string, const char *stringEnd, uint8_t base);

int xi_string_parse_number_float(float *out, const char *string, const char *stringEnd);
int xi_string_parse_number_double(double *out, const char *string, const char *stringEnd);
int xi_string_parse_number_long_double(long double *out, const char *string, const char *stringEnd);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {

XI_ERROR_CODE_BEGIN(ParseNumber)
XI_ERROR_CODE_VALUE(IllFormed, XI_STRING_PARSE_NUMBER_ERROR_INCONVERTIBLE)
XI_ERROR_CODE_VALUE(Overflow, XI_STRING_PARSE_NUMBER_ERROR_OVERFLOW)
XI_ERROR_CODE_VALUE(Underflow, XI_STRING_PARSE_NUMBER_ERROR_UNDERFLOW)
XI_ERROR_CODE_VALUE(LeadingSpace, XI_STRING_PARSE_NUMBER_ERROR_LEADING_SPACE)
XI_ERROR_CODE_VALUE(TrailingCharacter, XI_STRING_PARSE_NUMBER_ERROR_TRAILING_CHARACTERS)
XI_ERROR_CODE_VALUE(InvalidCharacter, XI_STRING_PARSE_NUMBER_ERROR_INVALID_CHARACTER)
XI_ERROR_CODE_VALUE(InvalidLocale, XI_STRING_PARSE_NUMBER_ERROR_INVALID_LOCALE)
XI_ERROR_CODE_END(ParseNumber, "Parse::NumberError")

Result<void> parse(const std::string &str, uint8_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, uint16_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, uint32_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, uint64_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, int8_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, int16_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, int32_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, int64_t &out, uint8_t base = 0);
Result<void> parse(const std::string &str, float &out);
Result<void> parse(const std::string &str, double &out);
Result<void> parse(const std::string &str, long double &out);

}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi, ParseNumber)

#endif
