// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <ostream>

#include "Xi/TypeTrait/IsDetected.hpp"

namespace Xi {

template <typename _ValueT, typename... _Ts>
using stringify_member_expression_t = decltype(std::declval<const _ValueT>().stringify(std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_stringify_member_expression_v =
    is_detected_v<stringify_member_expression_t, _ValueT, _Ts...>;

template <typename _ValueT, typename... _Ts>
using stringify_expression_t =
    decltype(stringify(std::declval<const std::decay_t<_ValueT>&>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_stringify_expression_v = is_detected_v<stringify_expression_t, _ValueT, _Ts...>;

}  // namespace Xi
