// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

namespace Xi {

/// Converts the string into a relaxed form for case insensitive comparision.
[[nodiscard]] std::string relax(std::string_view str);

}  // namespace Xi
