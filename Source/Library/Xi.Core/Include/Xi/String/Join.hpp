// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>
#include <string_view>

#include <Xi/Span.hpp>

namespace Xi {

[[nodiscard]] std::string join(Span<const std::string> strings, std::string_view token);

}  // namespace Xi
