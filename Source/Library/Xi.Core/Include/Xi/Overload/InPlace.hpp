// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi {

struct in_place_t {
  /* */
};

static inline constexpr in_place_t in_place{/* */};

}  // namespace Xi
