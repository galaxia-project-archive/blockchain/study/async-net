// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/String/Parse/Parse.hpp"
#include "Xi/String/Stringify/Stringify.hpp"
#include "Xi/String/EndsWith.hpp"
#include "Xi/String/FromString.hpp"
#include "Xi/String/Join.hpp"
#include "Xi/String/Replace.hpp"
#include "Xi/String/Relax.hpp"
#include "Xi/String/Split.hpp"
#include "Xi/String/StartsWith.hpp"
#include "Xi/String/ToLower.hpp"
#include "Xi/String/ToString.hpp"
#include "Xi/String/ToUpper.hpp"
#include "Xi/String/Trim.hpp"
