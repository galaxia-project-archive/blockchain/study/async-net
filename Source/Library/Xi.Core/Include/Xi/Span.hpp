﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <vector>
#include <array>
#include <memory>

#include "Xi/ErrorModel.hh"

namespace Xi {

template <typename _ValueT>
class Span {
 public:
  using element_type = _ValueT;
  using value_type = std::remove_cv_t<element_type>;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using pointer = value_type *;
  using const_pointer = const value_type *;
  using reference = _ValueT &;
  using const_reference = const value_type &;
  using iterator = pointer;
  using const_iterator = const_pointer;

 private:
  static constexpr const bool is_const_span_v = std::is_const_v<element_type>;
  using data_storage_t = std::conditional_t<is_const_span_v, const_pointer, pointer>;

 private:
  data_storage_t m_data;
  size_type m_size;

 public:
  constexpr Span() noexcept : m_data{nullptr}, m_size{0} {
    /* */
  }

  constexpr Span(std::conditional_t<is_const_span_v, const_pointer, pointer> data, size_type size)
      : m_data{data}, m_size{size} {
    /* */
  }

  template <std::size_t _N>
  constexpr Span(element_type (&arr)[_N]) noexcept : m_data{std::addressof(arr[0])}, m_size{_N} {
    /* */
  }

  template <std::size_t _N>
  constexpr Span(std::array<value_type, _N> &array) : m_data{array.data()}, m_size{_N} {
    /* */
  }
  template <std::size_t _N>
  constexpr Span(const std::array<value_type, _N> &array) : m_data{array.data()}, m_size{_N} {
    /* */
  }

  constexpr Span(std::vector<value_type> &vector) : m_data{vector.data()}, m_size{vector.size()} {
    /* */
  }
  constexpr Span(const std::vector<value_type> &vector) : m_data{vector.data()}, m_size{vector.size()} {
    /* */
  }

  size_type size() const noexcept {
    return this->m_size;
  }
  size_type size_bytes() const noexcept {
    return this->m_size * sizeof(value_type);
  }

  const_pointer data() const {
    return this->m_data;
  }
  std::conditional_t<is_const_span_v, const_pointer, pointer> data() {
    return this->m_data;
  }

  const_reference operator[](size_type idx) const {
    return this->m_data[idx];
  }
  std::conditional_t<is_const_span_v, const_reference, reference> operator[](size_type idx) {
    return this->m_data[idx];
  }

  operator Span<const value_type>() {
    return Span<const value_type>{this->m_data, this->m_size};
  }

  bool empty() const {
    return this->m_size == 0;
  }

  const_reference front() const {
    XI_EXCEPTIONAL_IF(OutOfRangeError, empty())
    return this->operator[](0);
  }

  reference front() {
    XI_EXCEPTIONAL_IF(OutOfRangeError, empty())
    return this->operator[](0);
  }

  const_reference back() const {
    XI_EXCEPTIONAL_IF(OutOfRangeError, empty())
    return this->operator[](this->size() - 1);
  }

  reference back() {
    XI_EXCEPTIONAL_IF(OutOfRangeError, empty())
    return this->operator[](this->size() - 1);
  }

  const_iterator begin() const {
    return m_data;
  }
  const_iterator cbegin() const {
    return m_data;
  }
  std::conditional_t<is_const_span_v, const_iterator, iterator> begin() {
    return m_data;
  }

  const_iterator end() const {
    return m_data + m_size;
  }
  const_iterator cend() const {
    return m_data + m_size;
  }
  std::conditional_t<is_const_span_v, const_iterator, iterator> end() {
    return m_data + m_size;
  }

  Span range(size_type offset, size_type count) const {
    XI_EXCEPTIONAL_IF(OutOfRangeError, offset + count > this->m_size);
    return Span{m_data + offset, count};
  }

  Span range(size_type offset) const {
    XI_EXCEPTIONAL_IF(OutOfRangeError, offset > this->m_size);
    return this->range(offset, this->m_size - offset);
  }

  Span slice(size_type start, size_t end) {
    XI_EXCEPTIONAL_IF(OutOfRangeError, start > this->m_size);
    XI_EXCEPTIONAL_IF(OutOfRangeError, end > this->m_size);
    XI_EXCEPTIONAL_IF(OutOfRangeError, start > end);
    return Span{m_data + start, end - start};
  }

  Span slice(size_type start) {
    XI_EXCEPTIONAL_IF(OutOfRangeError, start > this->m_size);
    return this->slice(start, this->size());
  }
};

template <typename _ValueT>
using ConstSpan = Span<const _ValueT>;

template <typename _ValueT>
inline Span<_ValueT> makeSpan(_ValueT &value) {
  return Span<_ValueT>{std::addressof(value), 1};
}

template <typename _ValueT>
inline Span<_ValueT> makeSpan(_ValueT *value, size_t count) {
  return Span<_ValueT>{value, count};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeSpan(const _ValueT &value) {
  return ConstSpan<_ValueT>{std::addressof(value), 1};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeSpan(const _ValueT *value, size_t count) {
  return ConstSpan<_ValueT>{value, count};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeConstSpan(_ValueT &value) {
  return ConstSpan<_ValueT>{std::addressof(value), 1};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeConstSpan(_ValueT *value, size_t count) {
  return ConstSpan<_ValueT>{value, count};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeConstSpan(const _ValueT &value) {
  return ConstSpan<_ValueT>{std::addressof(value), 1};
}

template <typename _ValueT>
inline ConstSpan<_ValueT> makeConstSpan(const _ValueT *value, size_t count) {
  return ConstSpan<_ValueT>{value, count};
}

}  // namespace Xi

#define XI_DECLARE_SPANS(TYPE)         \
  using TYPE##Span = ::Xi::Span<TYPE>; \
  using Const##TYPE##Span = ::Xi::Span<const TYPE>;

#define XI_DECLARE_SPANS_STRUCT(TYPE) \
  struct TYPE;                        \
  XI_DECLARE_SPANS(TYPE)

#define XI_DECLARE_SPANS_CLASS(TYPE) \
  class TYPE;                        \
  XI_DECLARE_SPANS(TYPE)
