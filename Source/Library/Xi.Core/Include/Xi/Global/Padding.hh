// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Global/Concatenate.hh"

#define XI_PADDING(BYTES) unsigned char XI_CONCATENATE(_XI_PADDING, __LINE__)[BYTES];
