// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#define XI_NARGS(...) ((int)(sizeof((int[]){__VA_ARGS__}) / sizeof(int)))
