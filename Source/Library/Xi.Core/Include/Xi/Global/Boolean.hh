// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdbool.h>

#if defined(__cplusplus)
}
#endif
