// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#if !defined(NDEBUG)
#include <stdio.h>
#define XI_PRINT_EC(...) \
  printf(__VA_ARGS__);   \
  fflush(stdout);
#else
#define XI_PRINT_EC(...)
#endif

#if defined(__cplusplus)
}
#endif
