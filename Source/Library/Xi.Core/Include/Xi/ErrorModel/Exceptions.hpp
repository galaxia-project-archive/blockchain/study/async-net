// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/ErrorModel/Exceptional.hpp"

namespace Xi {
XI_DECLARE_EXCEPTIONAL_CATEGORY(Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(Runtime, "generic runtime error", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(IndexOutOfRange, "array/vector index is out of bounds", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(OutOfRange, "number is out of range, usually required for a cast", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidIndex, "array/vector index is invalid in called context", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidSize, "array/vector size is invalid", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidVariantType, "unexpected type in variant", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidEnumValue, "enum does not match any decleration", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(Format, "invalid format for conversion", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(NotImplemented, "feature required is not implemented yet", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(NotFound, "an object was requested but not found", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(InvalidArgument, "argument provided is invalid", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(NotInitialized, "object is not initialized and cannot perform yet", Runtime)
XI_DECLARE_EXCEPTIONAL_INSTANCE(NullArgument, "argument passed is considered as null (ie. nullptr)", Runtime)
}  // namespace Xi
