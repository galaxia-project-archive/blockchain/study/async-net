// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

#include "Xi/Global/Boolean.hh"
#include "Xi/ErrorModel/Print.hh"

#define XI_RETURN_CODE_NO_SUCCESS 1
#define XI_RETURN_CODE_SUCCESS 0

#define XI_RETURN_EC(EC)                                                 \
  do {                                                                   \
    XI_PRINT_EC("[%s:%i] returns error: %s\n", __FILE__, __LINE__, #EC); \
    return EC;                                                           \
  } while (false)

#define XI_RETURN_EC_IF(COND, EC)                                                                         \
  do {                                                                                                    \
    if (COND) {                                                                                           \
      XI_PRINT_EC("[%s:%i] condition (%s) failed returning error: %s\n", __FILE__, __LINE__, #COND, #EC); \
      return EC;                                                                                          \
    }                                                                                                     \
  } while (false)

#define XI_RETURN_EC_IF_NOT(COND, EC)                                                                        \
  do {                                                                                                       \
    if (!(COND)) {                                                                                           \
      XI_PRINT_EC("[%s:%i] condition (!(%s)) failed returning error: %s\n", __FILE__, __LINE__, #COND, #EC); \
      return EC;                                                                                             \
    }                                                                                                        \
  } while (false)

#define XI_RETURN_SC(SC) \
  do {                   \
    return SC;           \
  } while (false)

#define XI_RETURN_SC_IF(COND, SC) \
  do {                            \
    if (COND) {                   \
      return SC;                  \
    }                             \
  } while (false)

#define XI_RETURN_SC_IF_NOT(COND, SC) \
  do {                                \
    if (!(COND)) {                    \
      return SC;                      \
    }                                 \
  } while (false)

#if defined(__cplusplus)
}
#endif
