// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Relax.hpp"

#include "Xi/String/ToLower.hpp"
#include "Xi/String/Trim.hpp"

namespace Xi {

std::string relax(std::string_view str) {
  return toLower(trim(str));
}

}  // namespace Xi
