﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Conversion.hpp"

#define _SILENCE_CXX17_CODECVT_HEADER_DEPRECATION_WARNING
#include <codecvt>
#include <locale>

namespace {
template <typename _FacetT>
struct deletable_facet : _FacetT {
  template <typename... _ArgTs>
  deletable_facet(_ArgTs &&... args) : _FacetT(std::forward<_ArgTs>(args)...) {
    /* */
  }
  ~deletable_facet() {
    /* */
  }
};
}  // namespace

namespace Xi {
std::string toMultiByteString(const wchar_t *wstring, const size_t size) {
  std::wstring_convert<deletable_facet<std::codecvt<wchar_t, char, std::mbstate_t>>> converter{};
  return converter.to_bytes(wstring, wstring + size);
}

std::string toMultiByteString(std::wstring_view wstring) {
  return toMultiByteString(wstring.data(), wstring.size());
}

std::string toMultiByteString(const wchar_t *wstring) {
  return toMultiByteString(std::wstring_view{wstring});
}

std::string toMultiByteString(const std::wstring &wstring) {
  return toMultiByteString(wstring.data(), wstring.size());
}

std::wstring toWideCharacterString(const char *mbstring, const size_t size) {
  std::wstring_convert<deletable_facet<std::codecvt<wchar_t, char, std::mbstate_t>>> converter{};
  return converter.from_bytes(mbstring, mbstring + size);
}

std::wstring toWideCharacterString(std::string_view mbstring) {
  return toWideCharacterString(mbstring.data(), mbstring.size());
}

std::wstring toWideCharacterString(const char *mbstring) {
  return toWideCharacterString(std::string_view{mbstring});
}

std::wstring toWideCharacterString(const std::string &mbstring) {
  return toWideCharacterString(mbstring.data(), mbstring.size());
}
}  // namespace Xi
