// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/ToLower.hpp"

#include <memory>
#include <locale>

namespace Xi {

std::string toLower(std::string_view str) {
  const std::locale locale{/* */};
  std::string reval{};
  reval.resize(str.size());
  for (size_t i = 0; i < str.size(); ++i) {
    reval[i] = static_cast<std::string::value_type>(std::tolower(str[i], locale));
  }
  return reval;
}

std::string::value_type toLower(const std::string::value_type ch) {
  const std::locale locale{/* */};
  return static_cast<std::string::value_type>(std::tolower(ch, locale));
}

void toLower(std::string &str, in_place_t) {
  const std::locale locale{/* */};
  for (char &i : str) {
    i = static_cast<std::string::value_type>(std::tolower(i, locale));
  }
}

}  // namespace Xi
