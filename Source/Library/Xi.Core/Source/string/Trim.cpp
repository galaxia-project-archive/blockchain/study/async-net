// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/String/Trim.hpp"

#include <algorithm>
#include <iterator>

namespace Xi {

std::string trimLeft(std::string_view str) {
  std::string reval{};
  auto begin = std::find_if_not(str.begin(), str.end(), [](auto c) { return std::isspace(c); });
  reval.reserve(static_cast<std::string::size_type>(std::distance(begin, str.end())));
  std::copy(begin, str.end(), std::back_inserter(reval));
  return reval;
}

void trimLeft(std::string &str, in_place_t) {
  auto end = std::find_if_not(str.begin(), str.end(), [](auto c) { return std::isspace(c); });
  str.erase(str.begin(), end);
}

std::string trimRight(std::string_view str) {
  std::string reval{};
  size_t i = str.size();
  while (i > 0 && std::isspace(str[i - 1])) {
    i -= 1;
  }
  return std::string{str.data(), i};
}

void trimRight(std::string &str, in_place_t) {
  size_t i = str.size();
  while (i > 0 && std::isspace(str[i - 1])) {
    i -= 1;
  }
  str.resize(i);
}

std::string trim(std::string_view str) {
  auto reval = trimLeft(str);
  trimRight(reval, in_place);
  return reval;
}

void trim(std::string &str, in_place_t) {
  trimLeft(str, in_place);
  trimRight(str, in_place);
}

}  // namespace Xi
