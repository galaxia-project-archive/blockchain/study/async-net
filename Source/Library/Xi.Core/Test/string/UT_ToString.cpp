// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <sstream>
#include <cinttypes>
#include <memory>

#include <Xi/Extern/Push.hh>
#include <fmt/format.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>
#include <Xi/String/ToString.hpp>

#define XI_TEST_SUITE Xi_Algorithm_String_ToString

namespace XI_TEST_SUITE {

class ToStringMember {
 public:
  std::string stringify() const {
    return "CUSTOM_MEMBER_FN";
  }
};

class ToStringExtern {
  /* */
};

inline std::string stringify(const ToStringExtern&) {
  return "CUSTOM_EXTERN";
}

}  // namespace XI_TEST_SUITE

static_assert(Xi::has_stringify_member_expression_v<XI_TEST_SUITE::ToStringMember>, "");
static_assert(Xi::has_stringify_expression_v<XI_TEST_SUITE::ToStringExtern>, "");
static_assert(!Xi::has_stringify_expression_v<char[3]>, "");

TEST(XI_TEST_SUITE, MemberFunction) {
  using namespace ::testing;
  using namespace ::Xi;

  XI_TEST_SUITE::ToStringMember value{};
  EXPECT_THAT(toString(value), Eq("CUSTOM_MEMBER_FN"));
}

TEST(XI_TEST_SUITE, ExternFunction) {
  using namespace ::testing;
  using namespace ::Xi;

  XI_TEST_SUITE::ToStringExtern value{};
  EXPECT_THAT(toString(value), Eq("CUSTOM_EXTERN"));
}

TEST(XI_TEST_SUITE, Number) {
  using namespace ::testing;
  using namespace ::Xi;

  EXPECT_THAT(toString(1), Eq("1"));
}

TEST(XI_TEST_SUITE, Fmt) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    XI_TEST_SUITE::ToStringMember value{};
    fmt::format("{}", value);
  }

  {
    XI_TEST_SUITE::ToStringExtern value{};
    fmt::format("{}", value);
  }

  {
    Error value;
    fmt::format("{}", value);
  }
}

TEST(XI_TEST_SUITE, OStream) {
  using namespace ::testing;
  using namespace ::Xi;

  {
    XI_TEST_SUITE::ToStringMember value{};
    std::cout << value << std::endl;
  }

  {
    XI_TEST_SUITE::ToStringExtern value{};
    std::cout << value << std::endl;
  }

  {
    Error value;
    std::cout << value << std::endl;
  }
}
