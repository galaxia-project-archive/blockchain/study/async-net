// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/Async/Async.hpp>
#include <Xi/Network/Tcp/Listener.hpp>
#include <Xi/Network/Tcp/Acceptor.hpp>
#include <Xi/Network/Tcp/AcceptorBuilder.hpp>

#define XI_TEST_SUITE Xi_Network_Tcp_Acceptor

TEST(XI_TEST_SUITE, v4_v6_Endpoints) {
  using namespace Xi;
  using namespace Xi::Async;
  using namespace Xi::Network;
  using namespace Xi::Network::Tcp;
  using namespace Xi::Testing;

  auto listener = makeListener();
  auto firstAcceptor = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v4Loopback, Port::Any}).build();
  ASSERT_TRUE(isSuccess(firstAcceptor));
  ASSERT_THAT(listener->bind(*firstAcceptor), IsSuccess());
  ASSERT_THAT(listener->bind(*firstAcceptor), IsFailure());

  auto listenerRun = Async::invoke(&Listener::run, listener);

  auto secondAcceptor = AcceptorBuilder{/* */}.withEndpoint(Endpoint{IpAddress::v6Loopback, Port::Any}).build();
  ASSERT_TRUE(isSuccess(secondAcceptor));
  ASSERT_THAT(listener->bind(*secondAcceptor), IsSuccess());
  ASSERT_THAT(listener->bind(*secondAcceptor), IsFailure());

  Async::invoke([=]() {
    sleepFor(std::chrono::milliseconds{200});
    listener->shutdown();
  });

  listenerRun.wait();
}
