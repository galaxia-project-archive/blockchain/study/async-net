﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "SslSession.hpp"

#include <utility>

#include <Xi/Async/Async.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/BasicServerSession")

namespace Xi {
namespace Network {
namespace Tcp {

SslSession::SslSession(Ssl::Context &ssl)
    : m_stream{boost::asio::make_strand(Async::this_executor::sharedIo()), ssl.handle()} {
  /* */
}

SslSession::~SslSession() {
  if (socket().is_open()) {
    XI_LOG_WARN("Session is still open on deconstruction, should be closed before releasing memory.")
    const auto ec = close();
    XI_LOG_ERROR_IF(isFailure(ec), "Closing session in deconstructor failed: {}", ec)
  }
}

SessionEncryption SslSession::encryption() const {
  return SessionEncryption::Ssl;
}

Result<Endpoint> SslSession::localEndpoint() const {
  Boost::ErrorCode ec{/* */};
  auto reval = socket().local_endpoint(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  return makeSuccess<Endpoint>(reval);
}

Result<Endpoint> SslSession::remoteEndpoint() const {
  Boost::ErrorCode ec{/* */};
  auto reval = socket().remote_endpoint(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  return makeSuccess<Endpoint>(reval);
}

Result<void> SslSession::setupIncoming() {
  Boost::ErrorCode ec{/* */};
  m_stream.async_handshake(boost::asio::ssl::stream_base::server, Async::yield(ec));
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED();
}

Result<void> SslSession::setupOutgoing() {
  Boost::ErrorCode ec{/* */};
  m_stream.async_handshake(boost::asio::ssl::stream_base::client, Async::yield(ec));
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED();
}

Result<void> SslSession::teardownIncoming() {
  Boost::ErrorCode ec{/* */};
  m_stream.async_shutdown(Async::yield(ec));
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED();
}

Result<void> SslSession::teardownOutgoing() {
  Boost::ErrorCode ec{/* */};
  m_stream.async_shutdown(Async::yield(ec));
  XI_ERROR_CODE_PROPAGATE(ec);
  XI_SUCCEED();
}

Result<void> SslSession::close() {
  Boost::ErrorCode ec{/* */};
  socket().close(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}

bool SslSession::isOpen() const {
  return socket().is_open();
}

Boost::Socket &SslSession::socket() {
  return Boost::get_lowest_layer(m_stream).socket();
}

const Boost::Socket &SslSession::socket() const {
  return Boost::get_lowest_layer(m_stream).socket();
}

Boost::Stream &SslSession::plainStream() {
  XI_EXCEPTIONAL(InvalidVariantTypeError);
}

const Boost::Stream &SslSession::plainStream() const {
  XI_EXCEPTIONAL(InvalidVariantTypeError);
}

Boost::SslStream &SslSession::sslStream() {
  return m_stream;
}

const Boost::SslStream &SslSession::sslStream() const {
  return m_stream;
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
