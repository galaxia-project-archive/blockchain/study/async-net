﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Stream/Stream.hpp>
#include <Xi/Ssl/Context.hpp>

#include "Xi/Network/Tcp/Boost/Boost.hpp"
#include "Xi/Network/Tcp/SessionConcept.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

class SslSession final : public SessionConcept {
 public:
  explicit SslSession(Ssl::Context& ssl);
  ~SslSession() override;

  SessionEncryption encryption() const override;
  Result<Endpoint> localEndpoint() const override;
  Result<Endpoint> remoteEndpoint() const override;

  Result<void> setupIncoming() override;
  Result<void> setupOutgoing() override;
  Result<void> teardownIncoming() override;
  Result<void> teardownOutgoing() override;
  Result<void> close() override;

  [[nodiscard]] bool isOpen() const;

  Boost::Socket& socket() override;
  const Boost::Socket& socket() const override;
  Boost::Stream& plainStream() override;
  const Boost::Stream& plainStream() const override;
  Boost::SslStream& sslStream() override;
  const Boost::SslStream& sslStream() const override;

 private:
  Boost::SslStream m_stream;
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
