﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "PlainSession.hpp"

#include <utility>

#include <Xi/Async/Async.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Tcp/BasicServerSession")

namespace Xi {
namespace Network {
namespace Tcp {

PlainSession::PlainSession() : m_stream{boost::asio::make_strand(Async::this_executor::sharedIo())} {
  /* */
}

PlainSession::~PlainSession() {
  if (socket().is_open()) {
    XI_LOG_WARN("Session is still open on deconstruction, should be closed before releasing memory.")
    const auto ec = close();
    XI_LOG_ERROR_IF(isFailure(ec), "Closing session in deconstructor failed: {}", ec)
  }
}

SessionEncryption PlainSession::encryption() const {
  return SessionEncryption::None;
}

Result<Endpoint> PlainSession::localEndpoint() const {
  Boost::ErrorCode ec{/* */};
  auto reval = socket().local_endpoint(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  return makeSuccess<Endpoint>(reval);
}

Result<Endpoint> PlainSession::remoteEndpoint() const {
  Boost::ErrorCode ec{/* */};
  auto reval = socket().remote_endpoint(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  return makeSuccess<Endpoint>(reval);
}

Result<void> PlainSession::setupIncoming() {
  XI_SUCCEED();
}

Result<void> PlainSession::setupOutgoing() {
  XI_SUCCEED();
}

Result<void> PlainSession::teardownIncoming() {
  XI_SUCCEED();
}

Result<void> PlainSession::teardownOutgoing() {
  XI_SUCCEED();
}

Result<void> PlainSession::close() {
  Boost::ErrorCode ec{/* */};
  socket().close(ec);
  XI_ERROR_CODE_PROPAGATE(ec)
  XI_SUCCEED()
}

bool PlainSession::isOpen() const {
  return socket().is_open();
}

Boost::Socket &PlainSession::socket() {
  return m_stream.socket();
}

const Boost::Socket &PlainSession::socket() const {
  return m_stream.socket();
}

Boost::Stream &PlainSession::plainStream() {
  return m_stream;
}

const Boost::Stream &PlainSession::plainStream() const {
  return m_stream;
}

Boost::SslStream &PlainSession::sslStream() {
  XI_EXCEPTIONAL(InvalidVariantTypeError);
}

const Boost::SslStream &PlainSession::sslStream() const {
  XI_EXCEPTIONAL(InvalidVariantTypeError);
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
