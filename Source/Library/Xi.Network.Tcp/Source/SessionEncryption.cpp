// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Tcp/SessionEncryption.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

std::string stringify(const SessionEncryption enc) {
  switch (enc) {
    case SessionEncryption::None:
      return "none";
    case SessionEncryption::Ssl:
      return "ssl";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
