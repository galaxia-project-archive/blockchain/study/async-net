﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>

#include <Xi/Extern/Push.hh>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/beast/core/basic_stream.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/core/rate_policy.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Network {
namespace Tcp {
namespace Boost {

using ErrorCode = boost::system::error_code;
using Executor = boost::beast::net::executor;
using Strand = boost::asio::strand<Executor>;
using Tcp = boost::beast::net::ip::tcp;
using Acceptor = boost::asio::ip::tcp::acceptor;
using Endpoint = boost::asio::ip::tcp::endpoint;

using boost::beast::get_lowest_layer;

// clang-format off
using Stream = boost::beast::basic_stream<
    Tcp,
    Executor,
    boost::beast::simple_rate_policy
>;

using Socket = Stream::socket_type;

using SslStream = boost::beast::ssl_stream<
    Stream
>;
// clang-format on

}  // namespace Boost
}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
