﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <optional>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Ssl/Config.hpp>

#include "Xi/Network/Tcp/Endpoint.hpp"
#include "Xi/Network/Tcp/Acceptor.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

class AcceptorBuilder {
 public:
  AcceptorBuilder();
  XI_DELETE_COPY(AcceptorBuilder)
  XI_DELETE_MOVE(AcceptorBuilder)
  ~AcceptorBuilder() = default;

  AcceptorBuilder& withEndpoint(const Endpoint& endpoint);
  AcceptorBuilder& withIpAddress(const IpAddress ip);
  AcceptorBuilder& withPort(const Port port);
  AcceptorBuilder& withAddressReuse(bool reuseAddress);
  AcceptorBuilder& withSsl(const std::optional<Ssl::Config>& ssl);
  AcceptorBuilder& withoutSsl();

  Result<SharedAcceptor> build();

 private:
  Endpoint m_endpoint;
  std::optional<bool> m_addressReuse;
  std::optional<Ssl::Config> m_ssl;
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
