﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Ssl/Context.hpp>

#include "Xi/Network/Tcp/Boost/Boost.hpp"
#include "Xi/Network/Tcp/Endpoint.hpp"
#include "Xi/Network/Tcp/Session.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

XI_ERROR_CODE_BEGIN(Acceptor)
XI_ERROR_CODE_VALUE(AlreadyOpen, 0x0001)
XI_ERROR_CODE_END(Acceptor, "Network::Tcp::AcceptorError")

XI_DECLARE_SMART_POINTER_CLASS(Listener)

/*!
 *
 * Raw connection entry point. The acceptor listens for incoming connections on a sepcific network interface
 * configuration and may abstract a required ssl wrapper. The generated session is created and filled with some info
 * about its origin but not handled at all. New sessions created are forwarded to the listener for handling.
 *
 */
class Acceptor final : public std::enable_shared_from_this<Acceptor> {
 public:
  using native_handle = Boost::Acceptor;

 public:
  ~Acceptor();

  Result<void> run();
  Result<void> shutdown();

  [[nodiscard]] bool isOpen() const;

 private:
  UniqueSessionConcept makeConcept();
  void onAccept(UniqueSessionConcept socket, Boost::ErrorCode ec);

  Result<void> doStartUp();
  Result<void> doTearDown();

 private:
  Acceptor();
  XI_DELETE_COPY(Acceptor)
  XI_DELETE_MOVE(Acceptor)

  friend class AcceptorBuilder;
  friend class Listener;

 private:
  bool m_reuseAddress;
  Endpoint m_endpoint;
  Boost::Acceptor m_acceptor;
  WeakListener m_handler;
  std::optional<Ssl::Context> m_ssl;
};

XI_DECLARE_SMART_POINTER(Acceptor)

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
