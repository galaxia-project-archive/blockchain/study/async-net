// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <limits>

#include <Xi/Global.hh>
#include <Xi/Memory/Size.hpp>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace Network {
namespace Tcp {

class RateLimit {
 public:
  constexpr static const Memory::Bytes NoLimit{std::numeric_limits<Memory::Bytes::value_type>::max()};

 public:
  RateLimit() = default;
  XI_DEFAULT_COPY(RateLimit)
  XI_DEFAULT_MOVE(RateLimit)
  ~RateLimit() = default;

  Memory::Bytes read() const;
  void setRead(const Memory::Bytes limit);

  Memory::Bytes write() const;
  void setWrite(const Memory::Bytes limit);

  XI_SERIALIZATION_COMPLEX_EXTERN()

 private:
  Memory::Bytes m_read{NoLimit};
  Memory::Bytes m_write{NoLimit};
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
