﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

namespace Xi::Network::Tcp {

XI_DECLARE_SMART_POINTER_CLASS(Connector)

/*!
 *
 * A Connector is the counter part of an acceptor. It is responsible to configure a session concept to connect to
 * another server. The configuration may vary, ie. different certificates being used or different limitations.
 *
 * Multiple connectors are managed by a speaker.
 *
 */
class Connector final {
 public:
};

}  // namespace Xi::Network::Tcp
