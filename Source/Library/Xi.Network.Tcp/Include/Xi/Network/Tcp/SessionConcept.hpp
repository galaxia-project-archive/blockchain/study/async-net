﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Network/Tcp/Boost/Boost.hpp"
#include "Xi/Network/Tcp/Endpoint.hpp"
#include "Xi/Network/Tcp/SessionDirection.hpp"
#include "Xi/Network/Tcp/SessionEncryption.hpp"

namespace Xi {
namespace Network {
namespace Tcp {

XI_DECLARE_SMART_POINTER_CLASS(SessionConcept)

class SessionConcept {
 public:
  virtual ~SessionConcept() = default;

  virtual SessionEncryption encryption() const = 0;
  virtual Result<Endpoint> localEndpoint() const = 0;
  virtual Result<Endpoint> remoteEndpoint() const = 0;

  virtual Result<void> setupIncoming() = 0;
  virtual Result<void> setupOutgoing() = 0;
  virtual Result<void> teardownIncoming() = 0;
  virtual Result<void> teardownOutgoing() = 0;
  virtual Result<void> close() = 0;

  /*
   *
   * The following Methods are convenient methods to expose internal handles of the api.
   *
   * In most cases you may not want to call any of those! In case you want to, here are some basics to consider.
   *  - plainServerStream call is only valid if the direction is incoming and the encryption type plain
   *  - sslServerStream call is only valid if the direction is incoming and the encryption type ssl
   *  - plainClientStream call is only valid if the direction is outgoing and the encryption type plain
   *  - sslClientStream call is only valid if the direction is outgoing and the encryption type ssl
   * This encodes a variant into the concept. It may would be possible to introduce different types but that would
   * yield to more server/client dependent code and maybe code duplication. As mentioned earlier in most cases
   * you only want to use the Session class which abstracts most of this here.
   *
   */
  virtual Boost::Socket& socket() = 0;
  virtual const Boost::Socket& socket() const = 0;
  virtual Boost::Stream& plainStream() = 0;
  virtual const Boost::Stream& plainStream() const = 0;
  virtual Boost::SslStream& sslStream() = 0;
  virtual const Boost::SslStream& sslStream() const = 0;
};

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi
