// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Network/IpAddress.hpp>
#include <Xi/Network/Port.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include <Xi/Extern/Push.hh>
#include <boost/asio/ip/tcp.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Network {
namespace Tcp {

XI_ERROR_CODE_BEGIN(Endpoint)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(Endpoint, "Network::Tcp::EndpointError")

class Endpoint {
 public:
  using native_type = boost::asio::ip::tcp::endpoint;

 public:
  static Result<Endpoint> parse(const std::string& str);

 public:
  explicit Endpoint() = default;
  explicit Endpoint(const IpAddress& address_, Port port_);
  XI_DEFAULT_COPY(Endpoint)
  XI_DEFAULT_MOVE(Endpoint)
  ~Endpoint() = default;

  /* implict */ Endpoint(const native_type& native);
  operator native_type() const;

  const IpAddress& address() const;
  void setAddress(const IpAddress& address_);

  Port port() const;
  void setPort(Port port_);

  std::string stringify() const;

 private:
  IpAddress m_address{/* */};
  Port m_port{/* */};

  XI_SERIALIZATION_FRIEND(Endpoint)
};

Result<void> serialize(Endpoint& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network::Tcp, Endpoint)
