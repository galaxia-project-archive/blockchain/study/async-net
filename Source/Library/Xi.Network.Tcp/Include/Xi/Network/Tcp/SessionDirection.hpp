// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace Network {
namespace Tcp {

XI_ERROR_CODE_BEGIN(SessionDirection)
XI_ERROR_CODE_VALUE(IllFormed, 0x0001)
XI_ERROR_CODE_END(SessionDirection, "Network::Tcp::SessionDirectionError")

enum struct SessionDirection {
  Incoming = 1,
  Outgoing = 2,
};

XI_SERIALIZATION_ENUM(SessionDirection)

std::string stringify(const SessionDirection direction);
Result<void> parse(const std::string& str, SessionDirection& out);

}  // namespace Tcp
}  // namespace Network
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Network::Tcp, SessionDirection)

XI_SERIALIZATION_ENUM_RANGE(Xi::Network::Tcp::SessionDirection, Incoming, Outgoing)
XI_SERIALIZATION_ENUM_TAG(Xi::Network::Tcp::SessionDirection, Incoming, "incoming")
XI_SERIALIZATION_ENUM_TAG(Xi::Network::Tcp::SessionDirection, Outgoing, "outgoing")
