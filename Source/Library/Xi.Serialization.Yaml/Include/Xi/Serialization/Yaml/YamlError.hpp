// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Serialization {
namespace Yaml {

XI_ERROR_CODE_BEGIN(Yaml)
XI_ERROR_CODE_VALUE(NoValue, 0x0001)
XI_ERROR_CODE_VALUE(Internal, 0x0002)

XI_ERROR_CODE_VALUE(TypeMissmatchScalar, 0x0010)
XI_ERROR_CODE_VALUE(TypeMissmatchBoolean, 0x0011)
XI_ERROR_CODE_VALUE(TypeMissmatchFloating, 0x0012)
XI_ERROR_CODE_VALUE(TypeMissmatchContainer, 0x0013)
XI_ERROR_CODE_VALUE(TypeMissmatchObject, 0x0014)
XI_ERROR_CODE_VALUE(TypeMissmatchArray, 0x0015)

XI_ERROR_CODE_VALUE(ScalarOutOfBoundary, 0x0020)
XI_ERROR_CODE_VALUE(FlagOverflow, 0x0021)
XI_ERROR_CODE_VALUE(SizeMissmatch, 0x0022)

XI_ERROR_CODE_VALUE(NullTag, 0x0030)
XI_ERROR_CODE_VALUE(DuplicateTag, 0x0031)
XI_ERROR_CODE_END(Yaml, "Serialization::YamlError")

}  // namespace Yaml
}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Yaml, Yaml)
