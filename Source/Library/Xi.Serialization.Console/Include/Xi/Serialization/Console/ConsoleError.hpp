// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Serialization {
namespace Console {

XI_ERROR_CODE_BEGIN(Console)
XI_ERROR_CODE_VALUE(NoValue, 0x0001)
XI_ERROR_CODE_VALUE(Internal, 0x0002)
XI_ERROR_CODE_VALUE(NullTag, 0x0003)

XI_ERROR_CODE_VALUE(TypeMissmatchScalar, 0x0010)
XI_ERROR_CODE_VALUE(TypeMissmatchBoolean, 0x0011)
XI_ERROR_CODE_VALUE(TypeMissmatchFloating, 0x0012)
XI_ERROR_CODE_VALUE(TypeMissmatchContainer, 0x0013)
XI_ERROR_CODE_VALUE(TypeMissmatchObject, 0x0014)
XI_ERROR_CODE_VALUE(TypeMissmatchArray, 0x0015)
XI_ERROR_CODE_END(Console, "Serialization::ConsoleError")

}  // namespace Console
}  // namespace Serialization
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Serialization::Console, Console)
