﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <functional>

#include "Xi/Async/Mutex.hpp"
#include "Xi/Async/Future.hpp"
#include "Xi/Async/WaitResult.hpp"

namespace Xi {
namespace Async {

/*!
 * \brief The Handler class wraps a callback that may be invoked due to a specific event.
 *
 * Some callbacks may only be viable in a certain scope. For this reason callback methods may return a handler that
 * is supposed to be hold in hold as long as the underlying callback is valid. Iff scoping is not cruicial for the
 * callback you may detach the handler, but in most cases that is undesirable.
 */
class [[nodiscard]] Handler final {
 public:
  using Callback = std::function<void()>;

 public:
  ~Handler();

  Handler(const Handler&) = delete;
  Handler& operator=(const Handler&) = delete;

  Handler(Handler && other);
  Handler& operator=(Handler&& other);

  /*!
   * \brief cancel Cancels any callback invoke that may occur in future.
   */
  void cancel();

  /*!
   * \brief detach Moves this event handler into a detached scope, thus it will still be invoked even if this handler is
   * deconstructed.
   */
  void detach();

 private:
  explicit Handler();

  friend Handler makeHandler(Callback&&, SharedFuture<DecidingWaitResult>, const DecidingWaitResult);

 private:
  Mutex m_guard;

  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

Handler makeHandler(Handler::Callback&& cb, SharedFuture<DecidingWaitResult> notifier);
Handler makeHandler(Handler::Callback&& cb, SharedFuture<DecidingWaitResult> notifier,
                    const DecidingWaitResult desiredState);

}  // namespace Async
}  // namespace Xi
