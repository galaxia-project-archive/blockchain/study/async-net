// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <mutex>
#include <cinttypes>
#include <shared_mutex>
#include <chrono>
#include <cassert>

#include <Xi/Extern/Push.hh>
#include <boost/fiber/timed_mutex.hpp>
#include <boost/fiber/condition_variable.hpp>
#include <Xi/Extern/Pop.hh>

namespace Xi {
namespace Async {

class SharedUpgradeMutex {
 private:
  using condition_variable = boost::fibers::condition_variable_any;
  using timed_mutex = boost::fibers::timed_mutex;
  using unique_lock = std::unique_lock<timed_mutex>;
  using readers_count = uint32_t;

  static inline constexpr readers_count write_entered_mask = 1U << (sizeof(readers_count) * 8 - 1);
  static inline constexpr readers_count upgrade_entered_mask = write_entered_mask >> 1;
  static inline constexpr readers_count state_mask = write_entered_mask | upgrade_entered_mask;
  static inline constexpr readers_count readers_count_mask = ~state_mask;

 private:
  boost::fibers::timed_mutex m_access{/* */};
  readers_count m_readers{0};
  condition_variable m_stateGate{/* */};
  condition_variable m_noReadersGate{/* */};

  [[nodiscard]] inline bool hasPendingWrite() const {
    return (m_readers & write_entered_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingWrite() const {
    return (m_readers & write_entered_mask) == 0;
  }

  [[nodiscard]] inline bool hasPendingUpgrade() const {
    return (m_readers & upgrade_entered_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingUpgrade() const {
    return (m_readers & upgrade_entered_mask) == 0;
  }

  [[nodiscard]] inline bool hasPendingWriteOrUpgrade() const {
    return (m_readers & state_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingWriteOrUpgrade() const {
    return (m_readers & state_mask) == 0;
  }

  [[nodiscard]] inline bool hasPendingRead() const {
    return (m_readers & readers_count_mask) > 0;
  }
  [[nodiscard]] inline bool hasNotPendingRead() const {
    return (m_readers & readers_count_mask) == 0;
  }

  [[nodiscard]] inline bool readersCountWillOverflow() const {
    return (m_readers & readers_count_mask) == readers_count_mask;
  }
  [[nodiscard]] inline bool readersCountWillNotOverflow() const {
    return (m_readers & readers_count_mask) < readers_count_mask;
  }

  inline void enterWriteState() {
    m_readers |= write_entered_mask;
  }
  inline void leaveWriteState() {
    m_readers &= ~write_entered_mask;
  }

  inline readers_count readersCount() const {
    return m_readers & readers_count_mask;
  }
  inline void setReadersCount(readers_count count) {
    m_readers &= state_mask;
    m_readers |= count;
  }

  inline void enterReadState() {
    setReadersCount((m_readers & readers_count_mask) + 1);
  }
  inline void leaveReadState() {
    setReadersCount((m_readers & readers_count_mask) - 1);
  }

  inline void enterUpgradeState() {
    const readers_count newCount = readersCount() + 1;
    m_readers &= state_mask;
    m_readers |= (upgrade_entered_mask | newCount);
  }
  inline void leaveUpgradeState() {
    const readers_count newCount = readersCount() - 1;
    m_readers &= write_entered_mask;
    m_readers |= newCount;
  }

 public:
  inline ~SharedUpgradeMutex() {
    [[maybe_unused]] unique_lock lock{m_access};
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] inline bool try_lock_until(const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    if (hasPendingWriteOrUpgrade()) {
      while (true) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (hasNotPendingWriteOrUpgrade()) {
          break;
        }
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
      }
    }

    enterWriteState();

    if (hasPendingRead()) {
      while (true) {
        const auto status = m_noReadersGate.wait_until(lock, limit);
        if (hasNotPendingRead()) {
          break;
        }
        if (status == boost::fibers::cv_status::timeout) {
          leaveWriteState();
          return false;
        }
      }
    }

    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] inline bool try_lock_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_lock_until(std::chrono::steady_clock::now() + duration);
  }

  inline void lock() {
    unique_lock lock{m_access};
    while (hasPendingWriteOrUpgrade()) {
      m_stateGate.wait(lock);
    }
    enterWriteState();
    while (hasPendingRead()) {
      m_noReadersGate.wait(lock);
    }
  }

  [[nodiscard]] inline bool try_lock() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingRead() || hasPendingWriteOrUpgrade()) {
      return false;
    }
    enterWriteState();
    return true;
  }

  inline void unlock() {
    [[maybe_unused]] unique_lock lock{m_access};
    m_readers = 0;
    m_stateGate.notify_all();
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] bool inline try_lock_shared_until(const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    while (true) {
      if (hasPendingWrite()) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
      }

      if (readersCountWillOverflow()) {
        const auto status = m_stateGate.wait_until(lock, limit);
        if (status == boost::fibers::cv_status::timeout) {
          return false;
        }
        if (!hasPendingWrite()) {
          break;
        }
      }
    }

    enterReadState();
    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] bool inline try_lock_shared_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_lock_shared_until(std::chrono::steady_clock::now() + duration);
  }

  inline void lock_shared() {
    unique_lock lock{m_access};
    while (true) {
      if (hasPendingWrite()) {
        m_stateGate.wait(lock);
      }

      if (readersCountWillOverflow()) {
        m_stateGate.wait(lock);
        if (hasNotPendingWrite()) {
          break;
        }
      }
    }

    enterReadState();
  }

  [[nodiscard]] inline bool try_lock_shared() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingWrite() || readersCountWillOverflow()) {
      return false;
    }
    enterReadState();
    return true;
  }

  inline void unlock_shared() {
    [[maybe_unused]] unique_lock lock{m_access};
    if (hasNotPendingWrite()) {
      if (readersCountWillOverflow()) {
        leaveReadState();
        m_stateGate.notify_one();
      }
    } else {
      leaveReadState();
      if (readersCount() == 0) {
        m_noReadersGate.notify_one();
      }
    }
  }

  inline void lock_upgrade() {
    unique_lock lock{m_access};
    while (hasPendingWriteOrUpgrade() || readersCountWillOverflow()) {
      m_stateGate.wait(lock);
    }

    enterUpgradeState();
  }

  [[nodiscard]] inline bool try_lock_upgrade() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingWriteOrUpgrade() || readersCountWillNotOverflow()) {
      return false;
    }
    enterUpgradeState();
    return true;
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] inline bool try_lock_upgrade_until(const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    while (hasPendingWriteOrUpgrade() || readersCountWillOverflow()) {
      const auto state = m_stateGate.wait_until(lock, limit);
      if (state == boost::fibers::cv_status::timeout) {
        return false;
      }
    }

    enterUpgradeState();
    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] inline bool try_lock_upgrade_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_lock_upgrade_until(std::chrono::steady_clock::now() + duration);
  }

  inline void unlock_upgrade() {
    [[maybe_unused]] unique_lock lock{m_access};
    leaveUpgradeState();
    m_stateGate.notify_all();
  }

  [[nodiscard]] inline bool try_unlock_shared_and_lock() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access, std::adopt_lock};
    if (hasPendingWriteOrUpgrade() || readersCount() > 1) {
      return false;
    }
    m_readers = write_entered_mask;
    return true;
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] inline bool try_unlock_shared_and_lock_until(
      const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    while (hasPendingWriteOrUpgrade()) {
      const auto state = m_stateGate.wait_until(lock, limit);
      if (state == boost::fibers::cv_status::timeout) {
        return false;
      }
    }

    leaveReadState();
    enterWriteState();

    while (hasPendingRead()) {
      const auto state = m_noReadersGate.wait_until(lock, limit);
      if (state == boost::fibers::cv_status::timeout) {
        leaveWriteState();
        enterReadState();
        return false;
      }
    }

    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] inline bool try_unlock_shared_and_lock_for(const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_unlock_shared_and_lock_until(std::chrono::steady_clock::now() + duration);
  }

  inline void unlock_and_lock_shared() {
    [[maybe_unused]] unique_lock lock{m_access};
    m_readers = 1;
    m_stateGate.notify_all();
  }

  [[nodiscard]] inline bool try_unlock_shared_and_lock_upgrade() {
    if (!m_access.try_lock()) {
      return false;
    }
    [[maybe_unused]] unique_lock lock{m_access};

    if (hasPendingWriteOrUpgrade()) {
      return false;
    }

    m_readers |= upgrade_entered_mask;
    return true;
  }

  template <typename _ClockT, typename _DurationT>
  [[nodiscard]] inline bool try_unlock_shared_and_lock_upgrade_until(
      const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    while (hasPendingWriteOrUpgrade()) {
      const auto state = m_stateGate.wait_until(lock, limit);
      if (state == boost::fibers::cv_status::timeout) {
        return false;
      }
    }

    m_readers |= upgrade_entered_mask;
    return true;
  }

  template <typename _RepT, typename _PeriodT>
  [[nodiscard]] inline bool try_unlock_shared_and_lock_upgrade_for(
      const std::chrono::duration<_RepT, _PeriodT>& duration) {
    return try_unlock_shared_and_lock_upgrade_until(std::chrono::steady_clock::now() + duration);
  }

  inline void unlock_upgrade_and_lock_shared() {
    [[maybe_unused]] unique_lock lock{m_access};
    m_readers &= ~upgrade_entered_mask;
    m_stateGate.notify_all();
  }

  inline void unlock_upgrade_and_lock() {
    unique_lock lock{m_access};

    leaveReadState();
    enterWriteState();

    while (hasPendingRead()) {
      m_noReadersGate.wait(lock);
    }
  }

  [[nodiscard]] inline bool try_unlock_upgrade_and_lock() {
    if (!m_access.try_lock()) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    if (readersCount() > 1) {
      return false;
    }

    m_readers = write_entered_mask;
    return true;
  }

  template <class _ClockT, class _DurationT>
  [[nodiscard]] inline bool try_unlock_upgrade_and_lock_until(
      const std::chrono::time_point<_ClockT, _DurationT>& limit) {
    if (!m_access.try_lock_until(limit)) {
      return false;
    }
    unique_lock lock{m_access, std::adopt_lock};

    leaveReadState();
    enterWriteState();
    m_readers &= ~upgrade_entered_mask;

    while (hasPendingRead()) {
      const auto status = m_noReadersGate.wait_until(lock, limit);
      if (status == boost::fibers::cv_status::timeout) {
        leaveWriteState();
        enterReadState();
        m_readers |= upgrade_entered_mask;
        return false;
      }
    }

    return true;
  }

  inline void unlock_and_lock_upgrade() {
    [[maybe_unused]] unique_lock lock{m_access};
    m_readers = upgrade_entered_mask | 1;
    m_stateGate.notify_all();
  }
};

}  // namespace Async
}  // namespace Xi
