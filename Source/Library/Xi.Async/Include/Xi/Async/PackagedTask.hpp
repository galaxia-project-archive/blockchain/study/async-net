// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {

template <typename _Signature>
using PackagedTask = Boost::PackagedTask<_Signature>;

}  // namespace Async
}  // namespace Xi
