﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

namespace Xi {
namespace Async {

/*!
 * \brief WaitResult encodes all possible states of a waitable result.
 *
 * This framework embedds a cancellation mechanism thus the wait result not only encodes ready/waiting but also
 * cancellation.
 */
enum struct [[nodiscard]] WaitResult{
    /// The time limited operation succeeded.
    Success = 1,
    /// Time limit exceeded and operation cancelled before being performed.
    Timeout = 2,
    /// Operation was cancelled by a fiber attached cancellation token.
    Cancelled = 3,
};

/*!
 * \brief DecidingWaitResult encodes all possible states of an infinite waiting result.
 *
 * If you wait forever for a result the operation may be cancelled or succeeds but you never will run into a timeout.
 * To encode this behaviour into the type system, we introduce a second enum.
 */
enum struct [[nodiscard]] DecidingWaitResult{
    /// The time limited operation succeeded.
    Success = 1,
    /// Operation was cancelled by a fiber attached cancellation token.
    Cancelled = 3,
};

/// Returns true iff the argument is a waiting result success.
[[nodiscard]] bool isSuccess(WaitResult res);
/// Returns true iff the argument is a waiting result success.
[[nodiscard]] bool isSuccess(DecidingWaitResult res);

/// Returns true iff the argument is a waiting result cancellation.
[[nodiscard]] bool isCancelled(WaitResult res);
/// Returns true iff the argument is a waiting result cancellation.
[[nodiscard]] bool isCancelled(DecidingWaitResult res);

/// Returns true iff the argument is a waiting result cancellation timeout.
[[nodiscard]] bool isTimeout(WaitResult res);

}  // namespace Async
}  // namespace Xi
