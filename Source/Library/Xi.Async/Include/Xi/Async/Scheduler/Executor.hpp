﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"

namespace Xi {
namespace Async {
namespace Scheduler {

/*!
 * \brief The Executor class wraps the main entry point for an event loop processing thread.
 *
 * Basically you want to spawn n threads with each thread calling the call operator on the Executor within an event
 * loop. The executor will poll the shared and local queue as well as schedule fibers of this thread until the shared
 * queue is stopped, see LoopGuard.
 */
class Executor final {
 public:
  using Id = std::uint32_t;

 public:
  static Executor& current();

 public:
  explicit Executor(Boost::IoContext& sharedIo_);
  Executor(const Executor&) = delete;
  Executor& operator=(const Executor&) = delete;
  Executor(Executor&&) = delete;
  Executor& operator=(Executor&&) = delete;
  ~Executor();

 public:
  Boost::IoContext& sharedIo();
  const Boost::IoContext& sharedIo() const;

  Boost::IoContext& localIo();
  const Boost::IoContext& localIo() const;

  std::thread::id thread() const;

  void operator()();

 private:
  Boost::IoContext& m_sharedIo;
  Boost::IoContext m_localIo;
  std::thread::id m_thread;
};

}  // namespace Scheduler

namespace this_executor {

Boost::IoContext& sharedIo();
Boost::IoContext& localIo();
std::thread::id thread();

}  // namespace this_executor

}  // namespace Async
}  // namespace Xi
