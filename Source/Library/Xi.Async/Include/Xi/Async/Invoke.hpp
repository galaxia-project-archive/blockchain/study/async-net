﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <type_traits>
#include <utility>

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Future.hpp"
#include "Xi/Async/PackagedTask.hpp"
#include "Xi/Async/Launch.hpp"
#include "Xi/Async/Event.hpp"
#include "Xi/Async/Scheduler/Properties.hpp"

namespace Xi {
namespace Async {

namespace Detail {

template <bool _EnabledV, typename _FunctorT, typename... _ArgsT>
using future_result_t = Future<std::invoke_result_t<std::enable_if_t<_EnabledV, _FunctorT>, std::decay_t<_ArgsT>...>>;

template <typename _FunctionT, typename... _ArgsT>
using invoke_result_t = std::invoke_result_t<_FunctionT, std::decay_t<_ArgsT>...>;

}  // namespace Detail

template <typename _LaunchT, typename _FunctionT, typename... _ArgsT>
Detail::future_result_t<is_launch_v<_LaunchT>, _FunctionT, _ArgsT...> invoke(const _LaunchT launch, _FunctionT&& fn,
                                                                             _ArgsT... args) {
  using result_type = Detail::invoke_result_t<_FunctionT, _ArgsT...>;

  PackagedTask<result_type(std::decay_t<_ArgsT>...)> task{std::forward<_FunctionT>(fn)};
  Future<result_type> future{task.get_future()};
  Boost::Fiber{static_cast<Boost::Policy>(launch), std::move(task), std::forward<_ArgsT>(args)...}.detach();

  return future;
}

template <typename _FunctionT, typename... _ArgsT>
Detail::future_result_t<!(is_launch_v<_FunctionT> || is_event_v<_FunctionT>), _FunctionT, _ArgsT...> invoke(
    _FunctionT&& fn, _ArgsT... args) {
  using result_type = Detail::invoke_result_t<_FunctionT, _ArgsT...>;

  PackagedTask<result_type(std::decay_t<_ArgsT>...)> task{std::forward<_FunctionT>(fn)};
  Future<result_type> future{task.get_future()};
  Boost::Fiber{std::move(task), std::forward<_ArgsT>(args)...}.detach();

  return future;
}

}  // namespace Async
}  // namespace Xi
