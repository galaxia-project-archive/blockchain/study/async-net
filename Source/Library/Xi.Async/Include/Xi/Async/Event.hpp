// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>
#include <type_traits>
#include <initializer_list>

#include <Xi/Global.hh>
#include <Xi/Chrono.hpp>

#include "Xi/Async/Clock.hpp"
#include "Xi/Async/Mutex.hpp"
#include "Xi/Async/Future.hpp"
#include "Xi/Async/Handler.hpp"
#include "Xi/Async/WaitResult.hpp"

namespace Xi {
namespace Async {

class Event {
 public:
  explicit Event();
  Event(const Event& other);
  Event& operator=(const Event& other);
  ~Event();

 public:
  void notify() noexcept;
  void notifyAt(const TimePoint& limit) noexcept;
  template <typename _ClockT, typename _DurationT,
            std::enable_if_t<!std::is_same_v<TimePoint, std::chrono::time_point<_ClockT, _DurationT>>, int> = 0>
  inline void notifyAt(const std::chrono::time_point<_ClockT, _DurationT>& limit) noexcept {
    notifyAt(Chrono::clock_cast<Clock>(limit));
  }
  void notifyAfter(const Duration& limit) noexcept;

  void cancel() noexcept;
  void cancelAt(const TimePoint& limit) noexcept;
  template <typename _ClockT, typename _DurationT,
            std::enable_if_t<!std::is_same_v<TimePoint, std::chrono::time_point<_ClockT, _DurationT>>, int> = 0>
  inline void cancelAt(const std::chrono::time_point<_ClockT, _DurationT>& limit) noexcept {
    notifyAt(Chrono::clock_cast<Clock>(limit));
  }
  void cancelAfter(const Duration& limit) noexcept;

 public:
  DecidingWaitResult wait() const noexcept;
  WaitResult waitUntil(const TimePoint& limit) const noexcept;
  template <typename _ClockT, typename _DurationT,
            std::enable_if_t<!std::is_same_v<TimePoint, std::chrono::time_point<_ClockT, _DurationT>>, int> = 0>
  inline WaitResult waitUntil(const std::chrono::time_point<_ClockT, _DurationT>& limit) noexcept {
    return waitUntil(Chrono::clock_cast<Clock>(limit));
  }
  WaitResult waitFor(const Duration& limit) const noexcept;

 public:
  SharedFuture<DecidingWaitResult> result() const;

 public:
  /// Registers a new notify event handler, if the handler gets destroyed before a notification the callback is not
  /// invoked.
  Handler onNotify(Handler::Callback&& callback) const;
  Handler onCancel(Handler::Callback&& callback) const;

 private:
  struct Impl;
  std::shared_ptr<Impl> m_impl;
};

Event ifAnyNotified(std::initializer_list<Event> events);
Event ifAllNotified(std::initializer_list<Event> envents);
Event ifAnyCancelled(std::initializer_list<Event> events);

template <typename _T>
struct is_event : std::false_type {};

template <>
struct is_event<Event> : std::true_type {};

template <typename _T>
constexpr bool is_event_v = is_event<_T>::value;

}  // namespace Async
}  // namespace Xi
