// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Async/Event.hpp"

#include <utility>
#include <cassert>
#include <exception>

#include "Xi/Async/Mutex.hpp"
#include "Xi/Async/Sleep.hpp"
#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Promise.hpp"

namespace Xi::Async {

struct Event::Impl {
  enum struct State { Waiting, Notified, Cancelled };

  State state{State::Waiting};
  SharedFuture<DecidingWaitResult> future;
  mutable Mutex guard{/* */};
  mutable Boost::ConditionVariable gate{/* */};

  Impl();
  ~Impl();
  void triggerStateSwitch(State desiredState) noexcept;
};

Event::Impl::Impl() {
  /* */
}

Event::Impl::~Impl() {
  triggerStateSwitch(State::Cancelled);
}

void Event::Impl::triggerStateSwitch(State desiredState) noexcept {
  [[maybe_unused]] auto sharedImplLock = lock(guard);
  switch (state) {
    case Impl::State::Cancelled:
    case Impl::State::Notified:
      return;
    case Impl::State::Waiting:
      break;
  }
  state = desiredState;
  gate.notify_all();
}

Xi::Async::Event::Event() : m_impl{new Impl} {
  /* */
}

Event::Event(const Event &other) {
  [[maybe_unused]] auto otherLock = lock(other.m_impl->guard);
  m_impl = other.m_impl;
}

Event &Event::operator=(const Event &other) {
  if (this != std::addressof(other)) {
    [[maybe_unused]] auto thisLock = lock(m_impl->guard);
    [[maybe_unused]] auto otherLock = lock(other.m_impl->guard);
    m_impl = other.m_impl;
  }
  return *this;
}

Event::~Event() {
  /* */
}

void Event::notify() noexcept {
  m_impl->triggerStateSwitch(Impl::State::Notified);
}

void Event::notifyAt(const TimePoint &limit) noexcept {
  boost::fibers::fiber{[this_ = m_impl, limit] {
    sleepUntil(limit);
    this_->triggerStateSwitch(Impl::State::Notified);
  }}.detach();
}

void Event::notifyAfter(const Duration &limit) noexcept {
  notifyAt(Clock::now() + limit);
}

void Event::cancel() noexcept {
  m_impl->triggerStateSwitch(Impl::State::Cancelled);
}

void Event::cancelAt(const TimePoint &limit) noexcept {
  boost::fibers::fiber{[this_ = m_impl, limit] {
    sleepUntil(limit);
    this_->triggerStateSwitch(Impl::State::Cancelled);
  }}.detach();
}

void Event::cancelAfter(const Duration &limit) noexcept {
  cancelAt(Clock::now() + limit);
}

DecidingWaitResult Event::wait() const noexcept {
  auto sharedImplLock = lock(unique, m_impl->guard);
  switch (m_impl->state) {
    case Impl::State::Notified:
      return DecidingWaitResult::Success;
    case Impl::State::Cancelled:
      return DecidingWaitResult::Cancelled;
    case Impl::State::Waiting:
      break;
  }

  while (m_impl->state == Impl::State::Waiting) {
    m_impl->gate.wait(sharedImplLock);
  }

  switch (m_impl->state) {
    case Impl::State::Notified:
      return DecidingWaitResult::Success;
    case Impl::State::Cancelled:
      return DecidingWaitResult::Cancelled;
    case Impl::State::Waiting:
      std::terminate();
  }
  std::terminate();
}

WaitResult Event::waitUntil(const TimePoint &limit) const noexcept {
  auto sharedImplLock = lock(unique, m_impl->guard);
  switch (m_impl->state) {
    case Impl::State::Notified:
      return WaitResult::Success;
    case Impl::State::Cancelled:
      return WaitResult::Cancelled;
    case Impl::State::Waiting:
      break;
  }

  while (m_impl->state == Impl::State::Waiting) {
    const auto status = m_impl->gate.wait_until(sharedImplLock, limit);
    if (status == Boost::ConditionVariableStatus::timeout) {
      break;
    }
  }

  switch (m_impl->state) {
    case Impl::State::Notified:
      return WaitResult::Success;
    case Impl::State::Cancelled:
      return WaitResult::Cancelled;
    case Impl::State::Waiting:
      return WaitResult::Timeout;
  }
  std::terminate();
}

WaitResult Event::waitFor(const Duration &limit) const noexcept {
  return waitUntil(Clock::now() + limit);
}

SharedFuture<DecidingWaitResult> Event::result() const {
  [[maybe_unused]] auto sharedImplLock = lock(m_impl->guard);
  if (!m_impl->future.valid()) {
    Promise<DecidingWaitResult> promise{/* */};
    m_impl->future = promise.get_future().share();
    Boost::Fiber{[this_ = m_impl](Promise<DecidingWaitResult> p) {
                   [[maybe_unused]] auto sharedImplLock = lock(unique, this_->guard);
                   while (this_->state == Impl::State::Waiting) {
                     this_->gate.wait(sharedImplLock);
                   }
                   switch (this_->state) {
                     case Impl::State::Waiting:
                     case Impl::State::Notified:
                       p.set_value(DecidingWaitResult::Success);
                       return;
                     case Impl::State::Cancelled:
                       p.set_value(DecidingWaitResult::Cancelled);
                       return;
                   }
                   std::terminate();
                 },
                 std::move(promise)}
        .detach();
  }
  return m_impl->future;
}

Handler Event::onNotify(Handler::Callback &&callback) const {
  return makeHandler(std::move(callback), result(), DecidingWaitResult::Success);
}

Handler Event::onCancel(Handler::Callback &&callback) const {
  return makeHandler(std::move(callback), result(), DecidingWaitResult::Cancelled);
}

Event ifAnyNotified(std::initializer_list<Event> events) {
  Event reval;
  if (events.size() == 0) {
    reval.cancel();
  } else {
    auto cancelCounter = std::make_shared<std::atomic_size_t>(events.size());
    for (auto &event : events) {
      boost::fibers::fiber{[](std::shared_ptr<std::atomic_size_t> counter, Event e, Event composed) {
                             if (e.wait() == DecidingWaitResult::Success) {
                               composed.notify();
                             } else {
                               if (--*counter == 0) {
                                 composed.cancel();
                               }
                             }
                           },
                           cancelCounter, event, reval}
          .detach();
    }
  }
  return reval;
}

Event ifAllNotified(std::initializer_list<Event> events) {
  Event reval{/* */};
  if (events.size() == 0) {
    reval.notify();
  } else {
    auto notifiedCounter = std::make_shared<std::atomic_size_t>(events.size());
    for (auto &event : events) {
      boost::fibers::fiber{[](std::shared_ptr<std::atomic_size_t> counter, Event e, Event composed) {
                             if (e.wait() != DecidingWaitResult::Success) {
                               composed.cancel();
                             } else {
                               if (--*counter == 0) {
                                 composed.notify();
                               }
                             }
                           },
                           notifiedCounter, event, reval}
          .detach();
    }
  }
  return reval;
}

Event ifAnyCancelled(std::initializer_list<Event> events) {
  Event reval{};
  if (events.size() == 0) {
    reval.cancel();
  } else {
    auto notifiedCounter = std::make_shared<std::atomic_size_t>(events.size());
    for (auto &event : events) {
      boost::fibers::fiber{[](std::shared_ptr<std::atomic_size_t> counter, Event e, Event composed) {
                             if (e.wait() == DecidingWaitResult::Cancelled) {
                               composed.notify();
                             } else {
                               if (--*counter == 0) {
                                 composed.cancel();
                               }
                             }
                           },
                           notifiedCounter, event, reval}
          .detach();
    }
  }
  return reval;
}

}  // namespace Xi::Async
