// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Mutex.hpp"

#include "scheduler/Base.hpp"

namespace Xi::Async::Scheduler {

class RoundRobin final : public Base {
 public:
  XI_DELETE_COPY(RoundRobin)
  XI_DELETE_MOVE(RoundRobin)
  ~RoundRobin() override;

 public:
  explicit RoundRobin(SharedExecutor executor);
  void awakened(Boost::FiberContext* context, Properties& props) noexcept override;
  Boost::FiberContext* pick_next() noexcept override;
  bool has_ready_fibers() const noexcept override;
  void suspend_until(const TimePoint& tp) noexcept override;
  void notify() noexcept override;

 private:
  Boost::SteadyTimer m_suspendTimer;
  Boost::ReadyQueue m_readyQueue{};
  std::size_t m_readyCounter{0};
  Mutex m_guard{};
};

}  // namespace Xi::Async::Scheduler
