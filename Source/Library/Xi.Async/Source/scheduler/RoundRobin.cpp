// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "scheduler/RoundRobin.hpp"

#include <cassert>

#include "Xi/Async/Scheduler/Executor.hpp"

namespace Xi::Async::Scheduler {

RoundRobin::~RoundRobin() {
  /* */
}

RoundRobin::RoundRobin(SharedExecutor executor_) : Base(executor_), m_suspendTimer{executor()->localIo()} {
  /* */
}

void RoundRobin::awakened(Boost::FiberContext *context, Properties &props) noexcept {
  XI_UNUSED(props);
  context->ready_link(m_readyQueue);
  if (!context->is_context(Boost::FiberType::dispatcher_context)) {
    m_readyCounter += 1;
  }
}

Boost::FiberContext *RoundRobin::pick_next() noexcept {
  Boost::FiberContext *reval{nullptr};
  if (!m_readyQueue.empty()) {
    reval = &m_readyQueue.front();
    m_readyQueue.pop_front();
    assert(reval);
    assert(reval != Boost::FiberContext::active());
    if (!reval->is_context(boost::fibers::type::dispatcher_context)) {
      m_readyCounter -= 1;
    }
  }
  return reval;
}

bool RoundRobin::has_ready_fibers() const noexcept {
  return m_readyCounter > 0;
}

void RoundRobin::suspend_until(const TimePoint &tp) noexcept {
  if (TimePoint::max() != tp) {
    m_suspendTimer.expires_at(tp);
    m_suspendTimer.async_wait([](boost::system::error_code const &) { boost::this_fiber::yield(); });
  }
}

void RoundRobin::notify() noexcept {
  m_suspendTimer.async_wait([](boost::system::error_code const &) { boost::this_fiber::yield(); });
  m_suspendTimer.expires_at(Clock::now());
}

}  // namespace Xi::Async::Scheduler
