// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Async/Boost.hpp"
#include "Xi/Async/Clock.hpp"

#include "Xi/Async/Scheduler/Properties.hpp"

namespace Xi::Async::Scheduler {

XI_DECLARE_SMART_POINTER_CLASS(Executor)

class Base : public Boost::Algorithm<Properties> {
 public:
  XI_DELETE_COPY(Base)
  XI_DELETE_MOVE(Base)
  virtual ~Base() override;

  Boost::FiberProperties* new_properties(Boost::FiberContext* context) override final;

 public:
  virtual void awakened(Boost::FiberContext* context, Properties& props) noexcept override = 0;
  virtual Boost::FiberContext* pick_next() noexcept override = 0;
  virtual bool has_ready_fibers() const noexcept override = 0;
  virtual void suspend_until(const TimePoint&) noexcept override = 0;
  virtual void notify() noexcept override = 0;

 protected:
  explicit Base(SharedExecutor executor);

  SharedExecutor executor();
  SharedConstExecutor executor() const;

 private:
  SharedExecutor m_executor;
};

}  // namespace Xi::Async::Scheduler
