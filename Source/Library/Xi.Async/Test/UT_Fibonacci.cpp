// Copyright (c) 2019 by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>

#include <gmock/gmock.h>

#include <Xi/Async/Async.hpp>

#define XI_UNIT_TEST_SUITE Async_Stress

namespace {

uint64_t fibonacci(uint64_t i) {
  using namespace Xi;
  if (i < 2) {
    return i;
  } else {
    auto i_1 = Async::invoke(Async::Launch::Post, fibonacci, i - 1);
    auto i_2 = Async::invoke(Async::Launch::Post, fibonacci, i - 2);
    return i_1.get() + i_2.get();
  }
}

}  // namespace

TEST(XI_UNIT_TEST_SUITE, Fibonacci) {
  using namespace Xi;
  using namespace Xi::Async;

  const std::map<uint64_t, uint64_t> Oracle{{
      {0ULL, 0ULL},
      {1ULL, 1ULL},
      {2ULL, 1ULL},
      {3ULL, 2ULL},
      {4ULL, 3ULL},
      {5ULL, 5ULL},
      {6ULL, 8ULL},
      {7ULL, 13ULL},
  }};

  std::vector<Future<void>> worker;
  for (size_t i = 0; i < 10; ++i) {
    worker.emplace_back(Async::invoke(Launch::Post, [&]() {
      std::map<uint64_t, Future<uint64_t>> result;
      for (const auto& iOracle : Oracle) {
        result[iOracle.first] = Async::invoke(fibonacci, iOracle.first);
      }

      for (auto& iResult : result) {
        auto iresult = iResult.second.get();
        auto expected = Oracle.find(iResult.first)->second;
        EXPECT_EQ(iresult, expected);
      }
    }));
  }

  for (auto& iWorker : worker) {
    iWorker.get();
  }
}
