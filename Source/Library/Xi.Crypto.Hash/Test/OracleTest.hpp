// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>
#include <utility>
#include <string>

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>

namespace XiCryptoTest {
namespace {
template <typename _HashT>
void genericHashTest(const std::vector<std::pair<std::string, _HashT>> &oracle) {
  using Hash = _HashT;

  for (const auto &[data, expectedHash] : oracle) {
    auto res = Hash::compute(data);
    ASSERT_THAT(res, Xi::Testing::IsSuccess());
    EXPECT_TRUE(*res == expectedHash);
  }
}
}  // namespace
}  // namespace XiCryptoTest
