// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/HashError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Crypto::Hash, Hash)
XI_ERROR_CODE_DESC(OutOfMemory, "insufficient memory buffer")
XI_ERROR_CODE_DESC(InsufficientData, "not enough data for computation available")
XI_ERROR_CODE_DESC(Internal, "internal core routine failed")
XI_ERROR_CODE_CATEGORY_END()
