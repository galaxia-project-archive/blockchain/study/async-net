﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/Hash/Sha2.hh"

#include <openssl/sha.h>

#include <Xi/ErrorModel.hh>

int xi_crypto_hash_sha2_224(const xi_byte_t *data, size_t length, xi_crypto_hash_224 out) {
  SHA256_CTX ctx;
  if (SHA224_Init(&ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA224_Update(&ctx, (const unsigned char *)data, length) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA224_Final(out, &ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  return XI_RETURN_CODE_SUCCESS;
}

int xi_crypto_hash_sha2_256(const xi_byte_t *data, size_t length, xi_crypto_hash_256 out) {
  SHA256_CTX ctx;
  if (SHA256_Init(&ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA256_Update(&ctx, (const unsigned char *)data, length) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA256_Final(out, &ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  return XI_RETURN_CODE_SUCCESS;
}

int xi_crypto_hash_sha2_384(const xi_byte_t *data, size_t length, xi_crypto_hash_384 out) {
  SHA512_CTX ctx;
  if (SHA384_Init(&ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA384_Update(&ctx, (const unsigned char *)data, length) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA384_Final(out, &ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  return XI_RETURN_CODE_SUCCESS;
}

int xi_crypto_hash_sha2_512(const xi_byte_t *data, size_t length, xi_crypto_hash_512 out) {
  SHA512_CTX ctx;
  if (SHA512_Init(&ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA512_Update(&ctx, (const unsigned char *)data, length) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  if (SHA512_Final(out, &ctx) != 1) {
    return XI_RETURN_CODE_NO_SUCCESS;
  }
  return XI_RETURN_CODE_SUCCESS;
}
