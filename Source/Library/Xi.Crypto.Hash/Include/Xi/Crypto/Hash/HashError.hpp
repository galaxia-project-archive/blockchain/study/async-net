// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Crypto {
namespace Hash {

XI_ERROR_CODE_BEGIN(Hash)

/// Insufficient memory buffer provided.
XI_ERROR_CODE_VALUE(OutOfMemory, 0x0001)

/// Data provided is insufficient to perform the computation
XI_ERROR_CODE_VALUE(InsufficientData, 0x0002)

/// Internal error in one of the implementing libraries
XI_ERROR_CODE_VALUE(Internal, 0x00FF)

XI_ERROR_CODE_END(Hash, "hash computation error")

}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Crypto::Hash, Hash)
