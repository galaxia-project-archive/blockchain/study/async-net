﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#define XI_HASH_128_SIZE 16u
#define XI_HASH_128_BITS (XI_HASHSIZE * 8u)

#define XI_HASH_224_SIZE 28u
#define XI_HASH_224_BITS (XI_HASH_224_BITS * 8u)

#define XI_HASH_256_SIZE 32u
#define XI_HASH_256_BITS (XI_HASH_256_SIZE * 8u)

#define XI_HASH_384_SIZE 48u
#define XI_HASH_384_BITS (XI_HASH_384_BITS * 8u)

#define XI_HASH_512_SIZE 64u
#define XI_HASH_512_BITS (XI_HASH_512_SIZE * 8u)

#define XI_HASH_1600_SIZE 200u
#define XI_HASH_1600_BITS (XI_HASH_1600_SIZE * 8u)

#if defined(__cplusplus)
extern "C" {
#endif

typedef xi_byte_t xi_crypto_hash_128[XI_HASH_128_SIZE];
typedef xi_byte_t xi_crypto_hash_224[XI_HASH_224_SIZE];
typedef xi_byte_t xi_crypto_hash_256[XI_HASH_256_SIZE];
typedef xi_byte_t xi_crypto_hash_384[XI_HASH_384_SIZE];
typedef xi_byte_t xi_crypto_hash_512[XI_HASH_512_SIZE];
typedef xi_byte_t xi_crypto_hash_1600[XI_HASH_1600_SIZE];

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <cinttypes>
#include <initializer_list>
#include <utility>
#include <string_view>
#include <cstring>
#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Memory/Blob.hpp>
#include <Xi/Encoding/Base16.hh>

#include "Xi/Crypto/Hash/HashError.hpp"

namespace Xi {
namespace Crypto {
namespace Hash {

template <typename _HashT>
Result<_HashT> computeHash(ConstByteSpan data) {
  _HashT reval{};
  if (const auto ec = compute(data, reval); ec != HashError::Success) {
    return makeFailure(ec);
  }
  return makeSuccess<_HashT>(std::move(reval));
}

template <typename _T, size_t _Bits>
struct EnableHashFromThis : Memory::EnableBlobFromThis<_T, _Bits / 8> {
  static_assert((_Bits % 8) == 0, "hash number of bits must be a multiple of 8");

  using value_type = _T;

  static const value_type Null;

  static inline constexpr size_t bits() {
    return _Bits;
  }

  static inline Result<value_type> compute(ConstByteSpan data) {
    return computeHash<value_type>(data);
  }
  static inline Result<value_type> compute(std::string_view data) {
    return computeHash<value_type>(
        ConstByteSpan{reinterpret_cast<const ConstByteSpan::const_pointer>(data.data()), data.size()});
  }

  using Memory::EnableBlobFromThis<_T, _Bits / 8>::EnableBlobFromThis;

  inline void nullify() {
    this->fill(0);
  }
  inline bool isNull() const {
    return *this == Null;
  }
};

template <typename _T, size_t _Bits>
[[nodiscard]] std::string stringify(const EnableHashFromThis<_T, _Bits>& hash) {
  return Encoding::Base16::encode(hash.span());
}

template <typename _T, size_t _Bits>
Result<void> parse(const std::string& str, EnableHashFromThis<_T, _Bits>& out) {
  return Encoding::Base16::decodeStrict(str, out.span());
}

#define XI_CRYPTO_HASH_DECLARE_HASH_TYPE(NAME, BITS)                 \
  struct NAME : ::Xi::Crypto::Hash::EnableHashFromThis<NAME, BITS> { \
    using EnableHashFromThis::EnableHashFromThis;                    \
    XI_DEFAULT_COPY(NAME)                                            \
    XI_DEFAULT_MOVE(NAME)                                            \
  };

#define XI_CRYPTO_HASH_DECLARE_HASH_IMPLEMENTATION(CLASS, BITS) \
  template <>                                                   \
  const CLASS Xi::Crypto::Hash::EnableHashFromThis<CLASS, BITS>::Null{};

}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
