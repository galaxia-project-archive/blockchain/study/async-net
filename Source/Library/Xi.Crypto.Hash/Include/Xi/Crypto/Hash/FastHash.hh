﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Crypto/Hash/Hash.hh"
#include "Xi/Crypto/Hash/Keccak.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>

#define XI_HASH_FAST_HASH_SIZE XI_CRYPTO_HASH_KECCAK_HASH_SIZE
#define XI_HASH_FAST_HASH_BITS (XI_HASH_FAST_HASH_SIZE * 8u)

typedef xi_byte_t xi_crypto_hash_fast[XI_HASH_FAST_HASH_SIZE];
typedef struct xi_crypto_hash_keccak_state xi_crypto_hash_fast_hash_state;

#define xi_crypto_hash_fast_hash_init xi_crypto_hash_keccak_init
#define xi_crypto_hash_fast_hash_update xi_crypto_hash_keccak_update
#define xi_crypto_hash_fast_hash_finalize xi_crypto_hash_keccak_finish
#define xi_crypto_hash_fast_hash_copy xi_crypto_hash_keccak_copy
#define xi_crypto_hash_fast_hash xi_crypto_hash_keccak_256

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <type_traits>

namespace Xi {
namespace Crypto {
namespace Hash {

using Fast = Keccak::Hash256;

}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
