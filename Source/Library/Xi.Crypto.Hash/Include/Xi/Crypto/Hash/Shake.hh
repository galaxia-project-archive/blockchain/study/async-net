// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/Byte.hh>

#include "Xi/Crypto/Hash/Hash.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>

typedef struct evp_md_ctx_st xi_crypto_hash_shake_128_state;
xi_crypto_hash_shake_128_state *xi_crypto_hash_shake_128_init(void);
int xi_crypto_hash_shake_128_update(xi_crypto_hash_shake_128_state *state, const xi_byte_t *data,
                                    size_t length);
int xi_crypto_hash_shake_128_finalize(xi_crypto_hash_shake_128_state *state,
                                      xi_crypto_hash_128 out);
void xi_crypto_hash_shake_128_destroy(xi_crypto_hash_shake_128_state *state);
int xi_crypto_hash_shake_128(const xi_byte_t *data, size_t length, xi_crypto_hash_128 out);

typedef struct evp_md_ctx_st xi_crypto_hash_shake_256_state;
xi_crypto_hash_shake_256_state *xi_crypto_hash_shake_256_init(void);
int xi_crypto_hash_shake_256_update(xi_crypto_hash_shake_256_state *state, const xi_byte_t *data,
                                    size_t length);
int xi_crypto_hash_shake_256_finalize(xi_crypto_hash_shake_256_state *state,
                                      xi_crypto_hash_256 out);
void xi_crypto_hash_shake_256_destroy(xi_crypto_hash_shake_256_state *state);
int xi_crypto_hash_shake_256(const xi_byte_t *data, size_t length, xi_crypto_hash_256 out);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

namespace Xi {
namespace Crypto {
namespace Hash {
namespace Shake {
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash128, 128);
XI_CRYPTO_HASH_DECLARE_HASH_TYPE(Hash256, 256);

HashError compute(ConstByteSpan data, Hash128 &out);
HashError compute(ConstByteSpan data, Hash256 &out);
}  // namespace Shake
}  // namespace Hash
}  // namespace Crypto
}  // namespace Xi

#endif
