// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include "Xi/Serialization/Binary/BinaryError.hpp"
#include "Xi/Serialization/Binary/Options.hpp"
#include "Xi/Serialization/Binary/InputSerializer.hpp"
#include "Xi/Serialization/Binary/OutputSerializer.hpp"
