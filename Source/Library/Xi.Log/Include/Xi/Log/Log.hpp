﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#if !defined(XI_LOG_LOG_HPP)
#define XI_LOG_LOG_HPP 1

#include "Xi/Log/Category.hpp"
#include "Xi/Log/Level.hpp"
#include "Xi/Log/Context.hpp"
#include "Xi/Log/Registry.hpp"
#include "Xi/Log/Handle.hpp"

#define XI_LOGGER(CATEGORY) static ::Xi::Log::Handle LogHandle = ::Xi::Log::Registry::get(CATEGORY);

#define XI_LOG(LEVEL, ...) LogHandle.log<::Xi::Log::Level::LEVEL>(__VA_ARGS__);
#define XI_LOG_IF(LEVEL, COND, ...) \
  if (COND) {                       \
    XI_LOG(LEVEL, __VA_ARGS__)      \
  }
#define XI_LOG_IF_NOT(LEVEL, COND, ...) \
  if (!(COND)) {                        \
    XI_LOG(LEVEL, __VA_ARGS__)          \
  }

#define XI_LOG_TRACE(...) XI_LOG(Trace, __VA_ARGS__)
#define XI_LOG_TRACE_IF(COND, ...) XI_LOG_IF(Trace, COND, __VA_ARGS__)
#define XI_LOG_TRACE_IF_NOT(COND, ...) XI_LOG_IF_NOT(Trace, COND, __VA_ARGS__)

#define XI_LOG_DEBUG(...) XI_LOG(Debug, __VA_ARGS__)
#define XI_LOG_DEBUG_IF(COND, ...) XI_LOG_IF(Debug, COND, __VA_ARGS__)
#define XI_LOG_DEBUG_IF_NOT(COND, ...) XI_LOG_IF_NOT(Debug, COND, __VA_ARGS__)

#define XI_LOG_INFO(...) XI_LOG(Info, __VA_ARGS__)
#define XI_LOG_INFO_IF(COND, ...) XI_LOG_IF(Info, COND, __VA_ARGS__)
#define XI_LOG_INFO_IF_NOT(COND, ...) XI_LOG_IF_NOT(Info, COND, __VA_ARGS__)

#define XI_LOG_WARN(...) XI_LOG(Warn, __VA_ARGS__)
#define XI_LOG_WARN_IF(COND, ...) XI_LOG_IF(Warn, COND, __VA_ARGS__)
#define XI_LOG_WARN_IF_NOT(COND, ...) XI_LOG_IF_NOT(Warn, COND, __VA_ARGS__)

#define XI_LOG_ERROR(...) XI_LOG(Error, __VA_ARGS__)
#define XI_LOG_ERROR_IF(COND, ...) XI_LOG_IF(Error, COND, __VA_ARGS__)
#define XI_LOG_ERROR_IF_NOT(COND, ...) XI_LOG_IF_NOT(Error, COND, __VA_ARGS__)

#define XI_LOG_FATAL(...) XI_LOG(Fatal, __VA_ARGS__)
#define XI_LOG_FATAL_IF(COND, ...) XI_LOG_IF(Fatal, COND, __VA_ARGS__)
#define XI_LOG_FATAL_IF_NOT(COND, ...) XI_LOG_IF_NOT(Fatal, COND, __VA_ARGS__)

#endif  // !defined(XI_LOG_LOG_HPP)

#if defined(XI_LOG_VERBOSE)
#undef XI_LOG_VERBOSE
#endif

#if defined(XI_LOG_VERBOSE_IF)
#undef XI_LOG_VERBOSE_IF
#endif

#if defined(XI_LOG_VERBOSE_IF_NOT)
#undef XI_LOG_VERBOSE_IF_NOT
#endif

#if defined(XI_LOG_VERBOSE_ENABLE)

#define XI_LOG_VERBOSE(...) XI_LOG_DEBUG(__VA_ARGS__)
#define XI_LOG_VERBOSE_IF(COND, ...) XI_LOG_DEBUG_IF(COND, __VA_ARGS__)
#define XI_LOG_VERBOSE_IF_NOT(COND, ...) XI_LOG_DEBUG_IF_NOT(COND, __VA_ARGS__)

#undef XI_LOG_VERBOSE_ENABLE
#else

#include <Xi/Global.hh>

#define XI_LOG_VERBOSE(...)
#define XI_LOG_VERBOSE_IF(...)
#define XI_LOG_VERBOSE_IF_NOT(...)

#endif
