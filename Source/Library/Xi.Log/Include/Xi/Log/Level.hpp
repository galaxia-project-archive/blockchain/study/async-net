// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

namespace Xi {
namespace Log {

enum struct Level {
  None = 7,
  Fatal = 6,
  Error = 5,
  Warn = 4,
  Info = 3,
  Debug = 2,
  Trace = 1,
};

std::string toString(const Level level);

}  // namespace Log
}  // namespace Xi
