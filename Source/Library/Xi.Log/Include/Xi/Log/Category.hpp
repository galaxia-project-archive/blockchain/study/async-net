// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>
#include <string>
#include <string_view>

#include <Xi/Global.hh>

namespace Xi {
namespace Log {

XI_DECLARE_SMART_POINTER_CLASS(Category)

class Category {
 private:
  Category(std::string_view name, std::string_view fullName);
  friend class Registry;

 public:
  ~Category() = default;

  const std::string& name() const;
  const std::string& fullName() const;

  WeakCategory parent() const;
  const std::vector<WeakCategory>& children() const;

 private:
  std::string m_name;
  std::string m_fullName;
  WeakCategory m_parent;
  std::vector<WeakCategory> m_children;
};

}  // namespace Log
}  // namespace Xi
