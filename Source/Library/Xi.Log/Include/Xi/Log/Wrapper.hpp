// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <atomic>

#include "Xi/Log/ILogger.hpp"

namespace Xi {
namespace Log {

class Wrapper : public ILogger {
 public:
  explicit Wrapper(SharedILogger inner);

  virtual bool isFiltered(const Level level) const override;
  virtual void print(Context context, std::string_view message) override;

 private:
  SharedILogger m_inner;
};

}  // namespace Log
}  // namespace Xi
