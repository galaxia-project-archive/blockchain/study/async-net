// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <Xi/Extern/Push.hh>
#include <gmock/gmock.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Log/Registry.hpp>
#include <Xi/Log/Handle.hpp>

#if defined(_MSC_VER)
#pragma warning(disable : 4373 74)
#endif

#define XI_TESTSUITE Xi_Log_Handle

namespace {
class ILoggerMock : public Xi::Log::ILogger {
 public:
  MOCK_CONST_METHOD1(isFiltered, bool(const Xi::Log::Level));
  MOCK_METHOD2(print, void(Xi::Log::Context, std::string_view));
};
}  // namespace

TEST(XI_TESTSUITE, Filters) {
  using namespace ::testing;
  using namespace Xi::Log;

  auto mock = std::make_shared<ILoggerMock>();
  Registry::put(mock);
  auto handle = Registry::get("");

  EXPECT_CALL(*mock, print(_, _)).Times(0);
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Trace))).WillOnce(Return(true));
  handle.trace("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Debug))).WillOnce(Return(true));
  handle.debug("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Info))).WillOnce(Return(true));
  handle.info("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Warn))).WillOnce(Return(true));
  handle.warn("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Error))).WillOnce(Return(true));
  handle.error("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Fatal))).WillOnce(Return(true));
  handle.fatal("test");

  Registry::put(nullptr);
}

TEST(XI_TESTSUITE, Prints) {
  using namespace ::testing;
  using namespace Xi::Log;

  auto mock = std::make_shared<ILoggerMock>();
  Registry::put(mock);
  auto handle = Registry::get("");

  EXPECT_CALL(*mock, print(_, Eq("test"))).Times(6);
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Trace))).WillOnce(Return(false));
  handle.trace("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Debug))).WillOnce(Return(false));
  handle.debug("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Info))).WillOnce(Return(false));
  handle.info("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Warn))).WillOnce(Return(false));
  handle.warn("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Error))).WillOnce(Return(false));
  handle.error("test");
  EXPECT_CALL(*mock, isFiltered(Eq(Level::Fatal))).WillOnce(Return(false));
  handle.fatal("test");

  Registry::put(nullptr);
}

TEST(XI_TESTSUITE, SwitchesLogger) {
  using namespace ::testing;
  using namespace Xi::Log;

  auto handle = Registry::get("");
  auto mock0 = std::make_shared<ILoggerMock>();
  auto mock1 = std::make_shared<ILoggerMock>();

  Registry::put(mock0);
  EXPECT_CALL(*mock0, print(_, Eq("test"))).Times(1);
  EXPECT_CALL(*mock0, isFiltered(Eq(Level::Trace))).WillOnce(Return(false));
  handle.trace("test");

  Registry::put(mock1);
  EXPECT_CALL(*mock1, print(_, Eq("test"))).Times(1);
  EXPECT_CALL(*mock1, isFiltered(Eq(Level::Trace))).WillOnce(Return(false));
  handle.trace("test");

  Registry::put(nullptr);
}
