// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Level.hpp"

#include <cinttypes>

std::string Xi::Log::toString(const Xi::Log::Level level) {
  switch (level) {
    case Level::None:
      return "none";
    case Level::Fatal:
      return "fatal";
    case Level::Error:
      return "error";
    case Level::Warn:
      return "warn";
    case Level::Info:
      return "info";
    case Level::Debug:
      return "debug";
    case Level::Trace:
      return "trace";
  }
  return "";
}
