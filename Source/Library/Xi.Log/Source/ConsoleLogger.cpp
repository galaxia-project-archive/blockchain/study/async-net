// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/ConsoleLogger.hpp"

#include <Xi/Extern/Push.hh>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <Xi/Extern/Pop.hh>

#include "SpdLogger.hpp"

namespace Xi {
namespace Log {

ConsoleLoggerBuilder &ConsoleLoggerBuilder::withColoring() {
  m_colored = true;
  return *this;
}

ConsoleLoggerBuilder &ConsoleLoggerBuilder::withoutColoring() {
  m_colored = false;
  return *this;
}

ConsoleLoggerBuilder &ConsoleLoggerBuilder::withErrorStream() {
  m_errorStream = true;
  return *this;
}

ConsoleLoggerBuilder &ConsoleLoggerBuilder::withStandardStream() {
  m_errorStream = false;
  return *this;
}

SharedILogger ConsoleLoggerBuilder::build() {
  spdlog::sink_ptr sink{};
  if (m_colored) {
    if (m_errorStream) {
      sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
    } else {
      sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    }
  } else {
    if (m_errorStream) {
      sink = std::make_shared<spdlog::sinks::stderr_sink_mt>();
    } else {
      sink = std::make_shared<spdlog::sinks::stdout_sink_mt>();
    }
  }
  return SharedILogger{new ConsoleLogger{std::make_shared<SpdLogger>(sink)}};
}

}  // namespace Log
}  // namespace Xi
