// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Wrapper.hpp"

#include <Xi/ErrorModel.hh>

Xi::Log::Wrapper::Wrapper(Xi::Log::SharedILogger inner) : m_inner{inner} {
  XI_EXCEPTIONAL_IF(NullArgumentError, inner.get() == nullptr, "log wrappers required none null internal logger");
}

bool Xi::Log::Wrapper::isFiltered(const Xi::Log::Level level) const {
  return m_inner->isFiltered(level);
}

void Xi::Log::Wrapper::print(Xi::Log::Context context, std::string_view message) {
  m_inner->print(context, message);
}
