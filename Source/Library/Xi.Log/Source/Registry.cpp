﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Registry.hpp"

#include <memory>
#include <utility>
#include <mutex>
#include <iostream>

#if defined(_WIN32)
#include <Windows.h>
#endif

#include <Xi/String.hpp>

#include "Xi/Log/Handle.hpp"
#include "Xi/Log/Registry.hpp"
#include "Xi/Log/Filter.hpp"
#include "Xi/Log/ConsoleLogger.hpp"

namespace Xi::Log {
class Registry::LoggerWrapper final : public ILogger {
 public:
  SharedILogger logger;

  bool isFiltered(const Level level) const override {
    if (logger) {
      return logger->isFiltered(level);
    } else {
      return true;
    }
  }

  void print(Context context, std::string_view message) override {
    if (logger) {
      logger->print(std::move(context), message);
    }
  }

  void swap(SharedILogger newLogger) {
    logger = newLogger;  // TODO: CAS
  }
};

Registry::Registry()
    : m_categories{{"None", SharedCategory{new Category{"None", "None"}}}},
      m_logger{std::make_shared<LoggerWrapper>()},
      m_guard{} {
#if defined(_WIN32)
  SetConsoleOutputCP(CP_UTF8);
#endif
  doPut(std::make_shared<Filter>(ConsoleLoggerBuilder{}.withColoring().withStandardStream().build()));
}

Registry::~Registry() {
}

Handle Registry::get(std::string_view category) {
  return registry().doGet(category);
}

void Registry::put(SharedILogger logger) {
  registry().doPut(logger);
}

Handle Registry::doGet(std::string_view category) {
  [[maybe_unused]] auto lock = Async::lock(m_guard);

  auto path = split(category, ":./", make_copy);
  if (path.empty()) {
    path.push_back("None");
  }
  auto node = getCategory(path.front(), "");
  for (size_t i = 1; i < path.size(); ++i) {
    node = getCategory(path[i], node->fullName());
  }
  return Handle{node, m_logger};
}

void Registry::doPut(SharedILogger logger) {
  m_logger->swap(logger);
}

SharedCategory Registry::getCategory(const std::string& category, std::string_view path) {
  std::string fullPath{path.data(), path.size()};
  if (!fullPath.empty()) {
    fullPath += ".";
  }
  fullPath += category;
  if (auto search = m_categories[fullPath]) {
    return search;
  } else {
    return m_categories[fullPath] = SharedCategory{new Category{category, fullPath}};
  }
}

Registry& registry() {
  static std::unique_ptr<Registry> __Singleton{new Registry};
  return *__Singleton;
}
}  // namespace Xi::Log
