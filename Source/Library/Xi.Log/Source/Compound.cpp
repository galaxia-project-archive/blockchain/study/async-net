// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Compound.hpp"

#include <algorithm>
#include <iterator>

Xi::Log::Compound::Compound() {
}

Xi::Log::Compound::Compound(std::initializer_list<Xi::Log::SharedILogger> logger)

{
  std::copy_if(begin(logger), end(logger), back_inserter(m_logger),
               [](const auto& i) { return i.get() != nullptr; });
}

bool Xi::Log::Compound::isFiltered(const Xi::Log::Level level) const {
  return std::all_of(begin(m_logger), end(m_logger),
                     [level](const auto& iLogger) { return iLogger->isFiltered(level); });
}

void Xi::Log::Compound::print(Xi::Log::Context context, std::string_view message) {
  for (auto& iLogger : m_logger) {
    if (!iLogger->isFiltered(context.level())) {
      iLogger->print(context, message);
    }
  }
}
