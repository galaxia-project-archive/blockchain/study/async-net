// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Log/Filter.hpp"

const Xi::Log::Level Xi::Log::Filter::DefaultLevel {
#if defined(NDEBUG)
  Level::Info
#else
  Level::Trace
#endif
};

Xi::Log::Filter::Filter(SharedILogger inner) : Wrapper(inner), m_level{DefaultLevel} {
}

Xi::Log::Filter::Filter(SharedILogger inner, Level options) : Wrapper(inner), m_level{options} {
}

bool Xi::Log::Filter::isFiltered(const Xi::Log::Level _level) const {
  return static_cast<uint8_t>(level()) > static_cast<uint8_t>(_level);
}

Xi::Log::Level Xi::Log::Filter::level() const {
  return m_level.load(std::memory_order_consume);
}

void Xi::Log::Filter::setLevel(Xi::Log::Level level) {
  m_level.store(level, std::memory_order_consume);
}
