// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Network/Uri.hpp>
#include <Xi/String/FromString.hpp>

#include <Xi/Testing/Result.hpp>

#include <string>

#define XI_TESTSUITE T_Xi_Network_Uri

TEST(XI_TESTSUITE, FromString) {
  using namespace ::testing;
  using namespace ::Xi;
  using namespace ::Xi::Network;
  using namespace ::Xi::Testing;

  {
    const auto uri = fromString<Uri>("https://seeds.xi-project.com");
    ASSERT_THAT(uri, IsSuccess());
    EXPECT_EQ(uri->scheme(), "https");
    EXPECT_EQ(uri->authority(), "seeds.xi-project.com");
    EXPECT_EQ(uri->port(), Port::Any);
  }

  {
    const auto uri = fromString<Uri>("//seeds.xi-project.com:123");
    ASSERT_THAT(uri, IsSuccess());
    EXPECT_EQ(uri->authority(), "seeds.xi-project.com");
    EXPECT_EQ(uri->port(), Port{123});
  }

  {
    const auto uri = fromString<Uri>("tcp://127.0.0.1");
    ASSERT_THAT(uri, IsSuccess());
    EXPECT_EQ(uri->scheme(), "tcp");
    EXPECT_EQ(uri->authority(), "127.0.0.1");
    EXPECT_EQ(uri->port(), Port::Any);
  }

  {
    const auto uri = fromString<Uri>("tcp://127.0.0.1:77000");
    ASSERT_THAT(uri, IsFailure());
  }
}
