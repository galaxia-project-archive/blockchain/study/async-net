﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Uri.hpp"

#include <utility>
#include <sstream>

#include <Xi/Extern/Push.hh>
#include <uriparser/Uri.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/ErrorModel.hh>
#include <Xi/String.hpp>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network, Uri)
XI_ERROR_CODE_DESC(IllFormed, "provided uri is not valid encoded")
XI_ERROR_CODE_CATEGORY_END()

namespace {
std::string nativeToString(const UriTextRangeA &range) {
  return std::string{range.first, range.afterLast};
}

std::string nativeToString(UriPathSegmentA *xs, const std::string &delim) {
  UriPathSegmentStructA *head(xs);
  std::string accum;

  while (head) {
    accum += delim + nativeToString(head->text);
    head = head->next;
  }

  return accum;
}
}  // namespace

namespace Xi {
namespace Network {

struct Uri::_Impl {
  std::string raw{};
  std::string scheme{};
  std::string host{};
  Port port{};
  std::string path{};
  std::string query{};
  std::string fragment{};
  std::string target{};
};

Result<Uri> Uri::parse(const std::string &str) {
  XI_ERROR_TRY
  Uri reval{};
  UriUriA native{};
  const char *errorPosition = nullptr;
  if (const auto ec = uriParseSingleUriA(&native, str.c_str(), &errorPosition); ec != URI_SUCCESS) {
    XI_FAIL(UriError::IllFormed);
  } else {
    reval.m_impl->raw = str;
    reval.m_impl->scheme = nativeToString(native.scheme);
    reval.m_impl->host = nativeToString(native.hostText);
    reval.m_impl->path = nativeToString(native.pathHead, "/");
    reval.m_impl->query = nativeToString(native.query);
    reval.m_impl->fragment = nativeToString(native.fragment);

    {
      std::string port = nativeToString(native.portText);
      if (port.empty()) {
        reval.m_impl->port = Port::Any;
      } else {
        auto parsePort = fromString<Port>(port);
        XI_ERROR_PROPAGATE(parsePort)
        reval.m_impl->port = parsePort.take();
      }
    }

    {
      std::stringstream builder{};
      builder << reval.m_impl->path;
      if (!reval.m_impl->query.empty()) {
        builder << "?" << reval.m_impl->query;
      }
      if (!reval.m_impl->fragment.empty()) {
        builder << "#" << reval.m_impl->fragment;
      }

      reval.m_impl->target = builder.str();
    }

    return makeSuccess(std::move(reval));
  }
  XI_ERROR_CATCH
}

Uri::Uri() : m_impl{new _Impl} {
  /* */
}

Uri::Uri(const Uri &other) : m_impl{new _Impl{*other.m_impl}} {
  /* */
}

Uri &Uri::operator=(const Uri &other) {
  if (this != std::addressof(other)) {
    *m_impl = *other.m_impl;
  }
  return *this;
}

Uri::Uri(Uri &&other) : m_impl{std::move(other.m_impl)} {
  /* */
}

Uri &Uri::operator=(Uri &&other) {
  if (this != std::addressof(other)) {
    m_impl = std::move(other.m_impl);
  }
  return *this;
}

Uri::~Uri() {
  /* */
}

const std::string &Uri::scheme() const {
  return m_impl->scheme;
}

Result<Protocol> Uri::protocol() const {
  return fromString<Protocol>(scheme());
}

const std::string &Uri::host() const {
  return m_impl->host;
}

Port Uri::port() const {
  return m_impl->port;
}

const std::string &Uri::path() const {
  return m_impl->path;
}

const std::string &Uri::query() const {
  return m_impl->query;
}

const std::string &Uri::fragment() const {
  return m_impl->fragment;
}

const std::string &Uri::target() const {
  return m_impl->target;
}

std::string Uri::stringify() const {
  std::ostringstream builder{};
  if (!scheme().empty()) {
    builder << scheme();
  }
  builder << "//" << host();
  if (!port().isAny()) {
    builder << ":" << port();
  }
  builder << "/" << target();
  if (!query().empty()) {
    builder << "?" << query();
  }
  if (!fragment().empty()) {
    builder << "#" << fragment();
  }
  return builder.str();
}

bool isUri(const std::string &str) {
  return fromString<Uri>(str).isValue();
}

}  // namespace Network
}  // namespace Xi
