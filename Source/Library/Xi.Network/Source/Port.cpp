// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Network/Port.hpp"

#include <Xi/ErrorModel.hh>
#include <Xi/String.hpp>

#include <Xi/Log/Log.hpp>
XI_LOGGER("Network/Port")

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Network, Port)
XI_ERROR_CODE_DESC(IsAny, "port is zero")
XI_ERROR_CODE_DESC(NoDefaultPort, "the given protocol has no default port")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Network {

const Port Port::Any{0};

Result<Port> Port::parse(const std::string &str) {
  auto native = fromString<value_type>(str);
  if (native.isError()) {
    XI_LOG_ERROR("Failed to parse port '{}': {}.", str, native)
    return native.error();
  }
  XI_FAIL_IF(*native == 0, PortError::IsAny)
  return makeSuccess<Port>(*native);
}

Result<Port> Port::fromProtocol(const Protocol protocol) {
  switch (protocol) {
    case Protocol::Http:
      return makeSuccess(Port{80});
    case Protocol::Https:
      return makeSuccess(Port{443});
    case Protocol::Xip:
      return makeSuccess(Port{22868});
    case Protocol::Xips:
      return makeSuccess(Port{22869});

    default:
      XI_LOG_ERROR("No default port avaialable for protocol '{}'.", protocol)
      return makeFailure(PortError::NoDefaultPort);
  }
}

bool Port::isAny() const {
  return (*this) == Any;
}

std::string Port::stringify() const {
  return toString(native());
}

Port Port::orDefault(Port def) const {
  if (isAny()) {
    return def;
  } else {
    return *this;
  }
}

Result<Port> Port::orDefault(const Protocol protocol) const {
  if (isAny()) {
    return fromProtocol(protocol);
  } else {
    return makeSuccess(*this);
  }
}

Result<void> serialize(Port &value, const Serialization::Tag &name, Serialization::Serializer &serializer) {
  Port::value_type native = Port::Any.native();
  if (serializer.isOutputMode()) {
    native = value.native();
    XI_FAIL_IF(native == Port::Any.native(), PortError::IsAny)
  }
  XI_ERROR_PROPAGATE_CATCH(serializer(native, name))
  if (serializer.isInputMode()) {
    XI_FAIL_IF(native == Port::Any.native(), PortError::IsAny)
    value = Port{native};
  }
  XI_SUCCEED()
}

}  // namespace Network
}  // namespace Xi
