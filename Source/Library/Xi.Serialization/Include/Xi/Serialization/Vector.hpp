// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <vector>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _ValueT>
Result<void> serialize(std::vector<_ValueT>& value, const Tag& name, Serializer& serializer) {
  size_t size = value.size();
  XI_ERROR_PROPAGATE_CATCH(serializer.beginVector(size, name));
  if (serializer.isInputMode()) {
    value.resize(size);
  }
  for (size_t i = 0; i < size; ++i) {
    XI_ERROR_PROPAGATE_CATCH(serializer(value[i], Tag::Null));
  }
  XI_ERROR_PROPAGATE_CATCH(serializer.endVector());
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
