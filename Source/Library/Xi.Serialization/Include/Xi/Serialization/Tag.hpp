// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <string>
#include <vector>

#include <Xi/Global.hh>

namespace Xi {
namespace Serialization {
class Tag {
 public:
  using binary_type = uint64_t;
  using text_type = std::string;

 public:
  static inline constexpr size_t maximumFlags() {
    return 14;
  }

 public:
  static const Tag Null;
  static const binary_type NoBinaryTag;
  static const text_type NoTextTag;

 public:
  Tag(binary_type binary, text_type text);
  XI_DEFAULT_COPY(Tag);
  XI_DEFAULT_MOVE(Tag);
  ~Tag() = default;

  binary_type binary() const;
  const text_type &text() const;
  bool isNull() const;

  bool operator==(const Tag &rhs) const;
  bool operator!=(const Tag &rhs) const;

 private:
  binary_type m_binary;
  text_type m_text;
};

using TagVector = std::vector<Tag>;

}  // namespace Serialization
}  // namespace Xi
