// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/TypeSafe/Integral.hpp>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _ValueT, typename _CompType>
Result<void> serialize(TypeSafe::EnableIntegralFromThis<_ValueT, _CompType> &value, const Tag &name,
                       Serializer &serializer) {
  _ValueT _{0};
  if (serializer.isOutputMode()) {
    _ = value.native();
  }
  XI_ERROR_PROPAGATE_CATCH(serializer(_, name))
  if (serializer.isInputMode()) {
    value.this_compatible() = _CompType{_};
  }
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
