// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/TypeTrait/IsDetected.hpp>

namespace Xi {
namespace Serialization {

class Serializer;
class Tag;

template <typename _ValueT, typename... _Ts>
using extern_serialization_expression_t = decltype(serialize(std::declval<_ValueT &>(), std::declval<const Tag &>(),
                                                             std::declval<Serializer &>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_extern_serialization_expression_v =
    is_detected_v<extern_serialization_expression_t, _ValueT, _Ts...>;

template <typename _ValueT, typename... _Ts>
using complex_serialization_expression_t =
    decltype(std::declval<_ValueT>().serialize(std::declval<Serializer &>(), std::declval<_Ts>()...));
template <typename _ValueT, typename... _Ts>
static inline constexpr bool has_complex_serialization_expression_v =
    is_detected_v<complex_serialization_expression_t, _ValueT, _Ts...>;

}  // namespace Serialization
}  // namespace Xi
