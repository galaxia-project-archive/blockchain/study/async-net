// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Memory/Blob.hpp>

#include "Xi/Serialization/Serializer.hpp"

namespace Xi {
namespace Serialization {

template <typename _T, size_t _Size>
[[nodiscard]] Result<void> serialize(Memory::EnableBlobFromThis<_T, _Size> &value, const Tag &name,
                                     Serializer &serializer) {
  return serializer.blob(value.span(), name);
}

}  // namespace Serialization
}  // namespace Xi
