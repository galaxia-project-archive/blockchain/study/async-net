﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>

#include "Xi/Serialization/InputSerializer.hpp"
#include "Xi/Serialization/OutputSerializer.hpp"
#include "Xi/Serialization/Serializer.hpp"
#include "Xi/Serialization/Object.hpp"
#include "Xi/Serialization/Array.hpp"
#include "Xi/Serialization/Enum.hpp"
#include "Xi/Serialization/Flag.hpp"
#include "Xi/Serialization/Vector.hpp"
#include "Xi/Serialization/Set.hpp"
#include "Xi/Serialization/Variant.hpp"
#include "Xi/Serialization/Optional.hpp"
#include "Xi/Serialization/Blob.hpp"
#include "Xi/Serialization/Null.hpp"
#include "Xi/Serialization/Map.hpp"
#include "Xi/Serialization/TypeSafe.hpp"

#define XI_SERIALIZATION_COMPLEX_BEGIN(...) \
  ::Xi::Result<void> serialize(::Xi::Serialization::Serializer& serializer __VA_OPT__(, ) __VA_ARGS__) {
#define XI_SERIALIZATION_BASE(CLASS) XI_ERROR_PROPAGATE_CATCH(this->CLASS::serialize(serializer))
#define XI_SERIALIZATION_MEMBER(MEMBER, BINARY, TEXT)                  \
  static const ::Xi::Serialization::Tag __##MEMBER##Tag{BINARY, TEXT}; \
  XI_ERROR_PROPAGATE_CATCH(serializer(MEMBER, __##MEMBER##Tag))
#define XI_SERIALIZATION_COMPLEX_END \
  return ::Xi::makeSuccess();        \
  }

#define XI_SERIALIZATION_COMPLEX_EXTERN(...) \
  ::Xi::Result<void> serialize(::Xi::Serialization::Serializer& serializer __VA_OPT__(, ) __VA_ARGS__);
#define XI_SERIALIZATION_COMPLEX_EXTERN_BEGIN(CLASS, ...) \
  ::Xi::Result<void> CLASS::serialize(::Xi::Serialization::Serializer& serializer __VA_OPT__(, ) __VA_ARGS__) {
#define XI_SERIALIZATION_COMPLEX_EXTERN_END \
  return ::Xi::makeSuccess();               \
  }

#define XI_SERIALIZATION_FRIEND(CLASS) \
  friend ::Xi::Result<void> serialize(CLASS&, const ::Xi::Serialization::Tag&, ::Xi::Serialization::Serializer&);
