// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string_view>
#include <cinttypes>
#include <type_traits>

#include <Xi/TypeTrait/IsDetected.hpp>

#include "Xi/Serialization/Format.hpp"
#include "Xi/Serialization/Trait.hpp"
#include "Xi/Serialization/InputSerializer.hpp"
#include "Xi/Serialization/OutputSerializer.hpp"

///
/// For custom serialization provide a method 'void serialize(Serializer&, TYPE&, const
/// std::string_view)' For variant provide the binary and string tag using
/// XI_DECLARE_TEXT_SERIALIZATION_TAG or XI_DECLARE_BINARY_SERIALIZATION_TAG .
///

namespace Xi {
namespace Serialization {
class Serializer {
 public:
  enum struct Mode { Input, Output };

 public:
  explicit Serializer(InputSerializer &ser);
  explicit Serializer(OutputSerializer &ser);
  ~Serializer();

  Mode mode() const;
  bool isInputMode() const;
  bool isOutputMode() const;

  Format format() const;
  bool isBinaryFormat() const;
  bool isHumanReadableFromat() const;

  [[nodiscard]] Result<void> primitive(std::int8_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::uint8_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::int16_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::uint16_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::int32_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::uint32_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::int64_t &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(std::uint64_t &value, const Tag &nameTag);

  [[nodiscard]] Result<void> primitive(bool &value, const Tag &nameTag);

  [[nodiscard]] Result<void> primitive(float &value, const Tag &nameTag);
  [[nodiscard]] Result<void> primitive(double &value, const Tag &nameTag);

  [[nodiscard]] Result<void> string(std::string &value, const Tag &nameTag);
  [[nodiscard]] Result<void> binary(ByteVector &value, const Tag &nameTag);

  [[nodiscard]] Result<void> blob(ByteSpan value, const Tag &nameTag);

  [[nodiscard]] Result<void> maybe(bool &value, const Tag &nameTag);
  [[nodiscard]] Result<void> typeTag(Tag &tag, const Tag &nameTag);
  [[nodiscard]] Result<void> flag(TagVector &tag, const Tag &nameTag);

  [[nodiscard]] Result<void> beginComplex(const Tag &nameTag);
  [[nodiscard]] Result<void> endComplex();

  [[nodiscard]] Result<void> beginArray(size_t count, const Tag &nameTag);
  [[nodiscard]] Result<void> endArray();

  [[nodiscard]] Result<void> beginVector(size_t &count, const Tag &nameTag);
  [[nodiscard]] Result<void> endVector();

  template <typename _ValueT>
  [[nodiscard]] Result<void> operator()(_ValueT &value, const Tag &nameTag) {
#define XI_SERIALIZER_FORWARD_CASE(TYPE, METHOD) \
  if constexpr (std::is_same_v<TYPE, _ValueT>)   \
    return this->METHOD(value, nameTag);         \
  else
#define XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(TYPE) XI_SERIALIZER_FORWARD_CASE(TYPE, primitive)

    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int8_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint8_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int16_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint16_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int32_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint32_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::int64_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(std::uint64_t)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(bool)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(float)
    XI_SERIALIZER_PRIMITIVE_FORWARD_CASE(double)
    XI_SERIALIZER_FORWARD_CASE(std::string, string)
    return serialize(value, nameTag, *this);

#undef XI_SERIALIZER_PRIMITIVE_FORWARD_CASE
#undef XI_SERIALIZER_FORWARD_CASE
  }

 private:
  struct _Impl;
  std::unique_ptr<_Impl> m_impl;
};

template <typename _ComplexT, typename... _Ts,
          std::enable_if_t<has_complex_serialization_expression_v<_ComplexT, _Ts...>, int> = 0>
[[nodiscard]] Result<void> serialize(_ComplexT &value, const Tag &nameTag, Serializer &serializer, _Ts &&... args) {
  XI_ERROR_PROPAGATE_CATCH(serializer.beginComplex(nameTag));
  XI_ERROR_PROPAGATE_CATCH(value.serialize(serializer, std::forward<_Ts>(args)...));
  XI_ERROR_PROPAGATE_CATCH(serializer.endComplex());
  XI_SUCCEED()
}

}  // namespace Serialization
}  // namespace Xi
