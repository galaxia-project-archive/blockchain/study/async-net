// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <cinttypes>
#include <optional>

#include <Xi/Serialization/Optional.hpp>
#include <Xi/Serialization/Serialization.hpp>

#include "GenericSerializerTest.hpp"

namespace {
struct MaybeStorage {
  std::optional<int64_t> value;

  XI_SERIALIZATION_COMPLEX_BEGIN()
  XI_SERIALIZATION_MEMBER(value, 0x0001, "value")
  XI_SERIALIZATION_COMPLEX_END
};
}  // namespace

XI_GENERIC_SERIALIZER_TEST(OptionalNoValue) {
  using namespace Xi::Serialization;
  MaybeStorage maybe{};
  maybe.value = std::nullopt;
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}

XI_GENERIC_SERIALIZER_TEST(OptionalValue) {
  using namespace Xi::Serialization;
  MaybeStorage maybe{};
  maybe.value.emplace(-120);
  auto _maybe = _this.serializeAndDeserialize(maybe);
  ASSERT_TRUE(isSuccess(_maybe));
  EXPECT_EQ(_maybe->value, maybe.value);
}
