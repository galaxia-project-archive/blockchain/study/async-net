// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.
#include "Xi/Serialization/Object.hpp"

#include "Xi/Serialization/Null.hpp"

namespace Xi {
namespace Serialization {

namespace {
static Null placeholder{/* */};
}

Object::Object() : m_concept{new _Impl<Null>(placeholder)} {
  /* */
}

Result<void> Object::serialize(const Tag &name, Serializer &serializer) {
  return m_concept->serializeObject(name, serializer);
}

Result<void> serialize(Object &value, const Tag &name, Serializer &serializer) {
  return value.serialize(name, serializer);
}

}  // namespace Serialization
}  // namespace Xi
