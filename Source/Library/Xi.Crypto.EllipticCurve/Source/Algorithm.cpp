// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/Algorithm.hpp"

#include <Xi/ErrorModel.hh>

#include "bernstein/Bernstein.hh"

Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::powers(const Xi::Crypto::EllipticCurve::Scalar &base,
                                                                          size_t maxExponent) {
  if (maxExponent == 0) {
    return {};
  } else if (maxExponent == 1) {
    return {identity()};
  }
  ScalarVector res{};
  res.resize(maxExponent);

  res[0] = identity();
  res[1] = base;

  for (size_t i = 2; i < maxExponent; ++i) {
    res[i] = res[i - 1] * base;
  }
  return res;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::powersSum(Xi::Crypto::EllipticCurve::Scalar base,
                                                                       size_t maxExponent) {
  if (maxExponent == 0) {
    return zero();
  } else if (maxExponent == 1) {
    return identity();
  } else {
    Scalar reval = identity();
    const bool isPowerOf2 = (maxExponent & (maxExponent - 1)) == 0;

    if (isPowerOf2) {
      sc_add(reval.data(), reval.data(), base.data());
      while (maxExponent > 2) {
        sc_mul(base.data(), base.data(), base.data());
        sc_muladd(reval.data(), base.data(), reval.data(), reval.data());
        maxExponent /= 2;
      }
    } else {
      auto prev = base;
      for (size_t i = 1; i < maxExponent; ++i) {
        if (i > 1)
          sc_mul(prev.data(), prev.data(), base.data());
        sc_add(reval.data(), reval.data(), prev.data());
      }
    }

    return reval;
  }
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::innerProduct(
    Xi::Crypto::EllipticCurve::ConstScalarSpan lhs, Xi::Crypto::EllipticCurve::ConstScalarSpan rhs) {
  XI_EXCEPTIONAL_IF_NOT(Xi::InvalidSizeError, lhs.size() == rhs.size(), "inner product required vectors of same size");

  Scalar res = zero();
  for (size_t i = 0; i < lhs.size(); ++i) {
    sc_muladd(res.data(), lhs[i].data(), rhs[i].data(), res.data());
  }
  return res;
}

Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::hadamardProduct(
    Xi::Crypto::EllipticCurve::ConstScalarSpan lhs, Xi::Crypto::EllipticCurve::ConstScalarSpan rhs) {
  return lhs * rhs;
}
