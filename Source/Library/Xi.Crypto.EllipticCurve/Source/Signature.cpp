// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/Signature.hpp"

#include <cassert>
#include <cstring>

#include <Xi/ErrorModel.hh>
#include <Xi/Memory/Compare.hh>
#include <Xi/Log/Log.hpp>

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"
#include "Xi/Crypto/EllipticCurve/SignatureError.hpp"
#include "Xi/Crypto/EllipticCurve/KeyDerivationError.hpp"
#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "bernstein/Bernstein.hh"

XI_LOGGER("Crypto/EllipticCurve")

#pragma pack(push, 1)
struct signature_commitment {
  xi_byte_t h[XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE];
  xi_byte_t key[XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE];
  xi_byte_t commitment[XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE];
};
#pragma pack(pop)

const Xi::Crypto::EllipticCurve::Signature Xi::Crypto::EllipticCurve::Signature::Null{
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

Xi::Result<Xi::Crypto::EllipticCurve::Signature> Xi::Crypto::EllipticCurve::Signature::sign(
    const Xi::Crypto::EllipticCurve::Hash &messageHash, const Xi::Crypto::EllipticCurve::Point &publicKey,
    const Scalar &secretKey) {
  XI_ERROR_TRY
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, publicKey.isValid(), "cannot sign with invalid public key");
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, secretKey.isValid(), "cannot sign with invalid secret key");
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, secretKey.toPoint() == publicKey,
                        "public and secret key must match in signature computatio");

  ge_p3 tmp3;
  Scalar k;
  signature_commitment buf;

  std::memcpy(buf.h, messageHash.data(), XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE);
  std::memcpy(buf.key, publicKey.data(), XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE);

  Signature reval;

try_again:
  do {
    XI_RETURN_EC_IF_NOT(random(k), makeFailure(KeyDerivationError::RandomGenerationFailed));
  } while (sc_is_tiny(k.data(), 4) != false);
  ge_scalarmult_base(&tmp3, k.data());
  ge_p3_tobytes(buf.commitment, &tmp3);
  int ec =
      xi_crypto_elliptic_curve_hash(reinterpret_cast<Byte *>(&buf), sizeof(signature_commitment), reval.mutableFirst());
  XI_RETURN_EC_IF_NOT(ec == XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));
  sc_reduce32(reval.mutableFirst());
  if (!sc_isnonzero(reval.first())) {
    goto try_again;
  }
  sc_mulsub(reval.mutableSecond(), reval.first(), secretKey.data(), k.data());
  if (!sc_isnonzero(reval.second())) {
    goto try_again;
  }
  return makeSuccess(std::move(reval));
  XI_ERROR_CATCH
}

Xi::Result<Xi::Crypto::EllipticCurve::Signature> Xi::Crypto::EllipticCurve::Signature::sign(
    Xi::ConstByteSpan message, const Xi::Crypto::EllipticCurve::Point &publicKey, const Scalar &secretKey) {
  Hash messageHash;
  XI_RETURN_EC_IF_NOT(compute(message, messageHash) == Crypto::Hash::HashError::Success,
                      makeFailure(SignatureError::HashComputationFailed));
  return sign(messageHash, publicKey, secretKey);
}

bool Xi::Crypto::EllipticCurve::Signature::isValid() const {
  return sc_check(first()) != 0 && sc_check(second()) != 0;
}

bool Xi::Crypto::EllipticCurve::Signature::operator!() const {
  return !isValid();
}

Xi::Crypto::EllipticCurve::Signature::operator bool() const {
  return isValid();
}

bool Xi::Crypto::EllipticCurve::Signature::validate(const Xi::Crypto::EllipticCurve::Hash &hashesMessage,
                                                    const Xi::Crypto::EllipticCurve::Point &publicKey) const {
  XI_RETURN_EC_IF_NOT(publicKey.isValid(), false);
  ge_p2 tmp2;
  ge_p3 tmp3;
  signature_commitment buf;

  std::memcpy(buf.h, hashesMessage.data(), XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE);
  std::memcpy(buf.key, publicKey.data(), XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE);

  XI_RETURN_EC_IF_NOT(ge_frombytes_vartime(&tmp3, publicKey.data()) == XI_RETURN_CODE_SUCCESS, false);
  XI_RETURN_EC_IF_NOT(sc_check(first()) == 0, false);
  XI_RETURN_EC_IF_NOT(sc_check(second()) == 0, false);
  XI_RETURN_EC_IF(!sc_isnonzero(first()), false);
  ge_double_scalarmult_base_vartime(&tmp2, first(), &tmp3, second());
  ge_tobytes(buf.commitment, &tmp2);
  static const ByteArray<XI_CRYPTO_ELLIPTIC_CURVE_POINT_SIZE> infinity{
      {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
  XI_RETURN_EC_IF(Memory::secureCompare(buf.commitment, infinity), false);
  Hash bufHash;
  XI_RETURN_EC_IF_NOT(
      compute(asByteSpan(&buf, sizeof(signature_commitment)), bufHash) == Crypto::Hash::HashError::Success, false);
  auto c = Scalar::fromMessageHash(asByteSpan(&buf, sizeof(signature_commitment)));
  if (c.isError()) {
    XI_LOG_VERBOSE("Signature validation failed: {}", c.error());
    return false;
  }
  auto cScalar = c.take();
  sc_sub(cScalar.mutableData(), cScalar.data(), first());
  return sc_isnonzero(cScalar.data()) == 0;
}

bool Xi::Crypto::EllipticCurve::Signature::validate(Xi::ConstByteSpan message,
                                                    const Xi::Crypto::EllipticCurve::Point &publicKey) const {
  XI_RETURN_EC_IF_NOT(publicKey.isValid(), false);
  Hash messageHash{};
  if (const auto ec = compute(message, messageHash); ec != Xi::Crypto::Hash::HashError::Success) {
    XI_LOG_ERROR("Unable to validate signature due to failing hash computation: {}", ec);
    return false;
  }
  return validate(messageHash, publicKey);
}

const Xi::Byte *Xi::Crypto::EllipticCurve::Signature::first() const {
  return data();
}

const Xi::Byte *Xi::Crypto::EllipticCurve::Signature::second() const {
  return data() + XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE;
}

Xi::Byte *Xi::Crypto::EllipticCurve::Signature::mutableFirst() {
  return mutableData();
}

Xi::Byte *Xi::Crypto::EllipticCurve::Signature::mutableSecond() {
  return mutableData() + XI_CRYPTO_ELLIPTIC_CURVE_SCALAR_SIZE;
}
