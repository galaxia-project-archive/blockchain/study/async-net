// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#include "bernstein/Operations.hh"

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdlib.h>

void ge_p3_from_hash(ge_p3*, const xi_byte_t*);
int ge_p3_from_message(ge_p3*, const xi_byte_t*, size_t);

#if defined(__cplusplus)
}
#endif
