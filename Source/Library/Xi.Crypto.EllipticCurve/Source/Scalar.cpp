﻿// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Crypto/EllipticCurve/Scalar.hpp"

#include <cstring>
#include <utility>

#include <Xi/ErrorModel.hh>
#include <Xi/Encoding/VarInt.hh>

#include "Xi/Crypto/Random/Random.hh"
#include "Xi/Crypto/Random/Engine.hpp"
#include "Xi/Crypto/EllipticCurve/KeyDerivationError.hpp"
#include "bernstein/Bernstein.hh"

namespace {
static Xi::Crypto::EllipticCurve::Scalar sm(Xi::Crypto::EllipticCurve::Scalar y, int n,
                                            const Xi::Crypto::EllipticCurve::Scalar &x) {
  while (n--) {
    y *= y;
  }
  y *= x;
  return y;
}
}  // namespace

const Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::Null{
    {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
     0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}};

Xi::Result<Xi::Crypto::EllipticCurve::Scalar> Xi::Crypto::EllipticCurve::Scalar::fromMessageHash(
    Xi::ConstByteSpan message) {
  static_assert(Hash::bytes() == Scalar::bytes(), "hash and scalar size must match.");
  XI_ERROR_TRY
  Scalar reval;
  if (xi_crypto_elliptic_curve_hash(message.data(), message.size(), reval.mutableData()) != XI_RETURN_CODE_SUCCESS) {
    return makeFailure(KeyDerivationError::HashComputationFailed);
  }
  sc_reduce32(reval.mutableData());
  return makeSuccess(std::move(reval));
  XI_ERROR_CATCH
}

Xi::Result<Xi::Crypto::EllipticCurve::Scalar> Xi::Crypto::EllipticCurve::Scalar::generateRandom() {
  XI_ERROR_TRY
  Scalar res;
  XI_RETURN_EC_IF_NOT(random(res), makeFailure(KeyDerivationError::RandomGenerationFailed));
  return makeSuccess(std::move(res));
  XI_ERROR_CATCH
}

Xi::Result<Xi::Crypto::EllipticCurve::Scalar> Xi::Crypto::EllipticCurve::Scalar::generateRandom(
    Xi::ConstByteSpan seed) {
  XI_ERROR_TRY
  Scalar res;
  XI_RETURN_EC_IF_NOT(random(seed, res), makeFailure(KeyDerivationError::RandomGenerationFailed));
  return makeSuccess(std::move(res));
  XI_ERROR_CATCH
}

Xi::Result<Xi::Crypto::EllipticCurve::Scalar> Xi::Crypto::EllipticCurve::Scalar::fromKeyDerivation(
    const Xi::Crypto::EllipticCurve::Point &derivation, const uint64_t outputIndex) {
  ByteArray<Encoding::VarInt::maximumEncodingSize<size_t>()> buffer;
  auto size = Encoding::VarInt::encode(outputIndex, buffer);
  XI_RETURN_EC_IF(size.isError(), size.error());

  xi_crypto_elliptic_curve_hash_state state;
  int ec = xi_crypto_elliptic_curve_hash_init(&state);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));
  ec = xi_crypto_elliptic_curve_hash_update(&state, derivation.data(), derivation.size());
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));
  ec = xi_crypto_elliptic_curve_hash_update(&state, buffer.data(), *size);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));
  ec = xi_crypto_elliptic_curve_hash_update(&state, buffer.data(), *size);
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));

  Scalar reval;
  static_assert(scalarSize() == XI_CRYPTO_ELLIPTIC_CURVE_HASH_SIZE, "implementation assumes equality");
  ec = xi_crypto_elliptic_curve_hash_finish(&state, reval.mutableData());
  XI_RETURN_EC_IF(ec != XI_RETURN_CODE_SUCCESS, makeFailure(KeyDerivationError::HashComputationFailed));
  sc_reduce32(reval.mutableData());
  return makeSuccess(std::move(reval));
}

Xi::Result<Xi::Crypto::EllipticCurve::Scalar> Xi::Crypto::EllipticCurve::Scalar::fromKeyDerivation(
    const Xi::Crypto::EllipticCurve::Point &derivation, const uint64_t outputIndex,
    const Xi::Crypto::EllipticCurve::Scalar &base) {
  auto intermediateResult = fromKeyDerivation(derivation, outputIndex);
  XI_RETURN_EC_IF(intermediateResult.isError(), intermediateResult.error());
  Scalar reval;
  sc_add(reval.mutableData(), base.data(), intermediateResult->data());
  return makeSuccess(std::move(reval));
}

Xi::Crypto::EllipticCurve::Scalar::Scalar(Xi::Crypto::EllipticCurve::zero_t _) {
  std::memcpy(mutableData(), _.data(), scalarSize());
}

Xi::Crypto::EllipticCurve::Scalar::Scalar(Xi::Crypto::EllipticCurve::identity_t _) {
  std::memcpy(mutableData(), _.data(), scalarSize());
}

bool Xi::Crypto::EllipticCurve::Scalar::isValid() const {
  XI_RETURN_EC_IF_NOT(sc_check(data()) == 0, false);
  return true;
}

bool Xi::Crypto::EllipticCurve::Scalar::operator!() const {
  return !isValid();
}

Xi::Crypto::EllipticCurve::Scalar::operator bool() const {
  return isValid();
}

Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::Scalar::toPoint() const {
  return (*this) * basePoint();
}

void Xi::Crypto::EllipticCurve::Scalar::invert() {
  Scalar _1, _10, _100, _11, _101, _111, _1001, _1011, _1111;

  _1 = *this;
  sc_mul(_10.data(), _1.data(), _1.data());
  sc_mul(_100.data(), _10.data(), _10.data());
  sc_mul(_11.data(), _10.data(), _1.data());
  sc_mul(_101.data(), _10.data(), _11.data());
  sc_mul(_111.data(), _10.data(), _101.data());
  sc_mul(_1001.data(), _10.data(), _111.data());
  sc_mul(_1011.data(), _10.data(), _1001.data());
  sc_mul(_1111.data(), _100.data(), _1011.data());

  sc_mul(mutableData(), _1111.data(), _1.data());

  *this = sm(*this, 123 + 3, _101);
  *this = sm(*this, 2 + 2, _11);
  *this = sm(*this, 1 + 4, _1111);
  *this = sm(*this, 1 + 4, _1111);
  *this = sm(*this, 4, _1001);
  *this = sm(*this, 2, _11);
  *this = sm(*this, 1 + 4, _1111);
  *this = sm(*this, 1 + 3, _101);
  *this = sm(*this, 3 + 3, _101);
  *this = sm(*this, 3, _111);
  *this = sm(*this, 1 + 4, _1111);
  *this = sm(*this, 2 + 3, _111);
  *this = sm(*this, 2 + 2, _11);
  *this = sm(*this, 1 + 4, _1011);
  *this = sm(*this, 2 + 4, _1011);
  *this = sm(*this, 6 + 4, _1001);
  *this = sm(*this, 2 + 2, _11);
  *this = sm(*this, 3 + 2, _11);
  *this = sm(*this, 3 + 2, _11);
  *this = sm(*this, 1 + 4, _1001);
  *this = sm(*this, 1 + 3, _111);
  *this = sm(*this, 2 + 4, _1111);
  *this = sm(*this, 1 + 4, _1011);
  *this = sm(*this, 3, _101);
  *this = sm(*this, 2 + 4, _1111);
  *this = sm(*this, 3, _101);
  *this = sm(*this, 1 + 2, _11);
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::inverted() const {
  Scalar reval = *this;
  reval.invert();
  return reval;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::operator~() const {
  return inverted();
}

Xi::Crypto::EllipticCurve::Scalar &Xi::Crypto::EllipticCurve::Scalar::operator+=(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) {
  sc_add(mutableData(), data(), rhs.data());
  return *this;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::operator+(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) const {
  Scalar reval = *this;
  reval += rhs;
  return reval;
}

Xi::Crypto::EllipticCurve::Scalar &Xi::Crypto::EllipticCurve::Scalar::operator-=(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) {
  sc_sub(mutableData(), data(), rhs.data());
  return *this;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::operator-(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) const {
  Scalar reval = *this;
  reval -= rhs;
  return reval;
}

Xi::Crypto::EllipticCurve::Scalar &Xi::Crypto::EllipticCurve::Scalar::operator*=(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) {
  sc_mul(mutableData(), data(), rhs.data());
  return *this;
}

Xi::Crypto::EllipticCurve::Scalar Xi::Crypto::EllipticCurve::Scalar::operator*(
    const Xi::Crypto::EllipticCurve::Scalar &rhs) const {
  Scalar reval = *this;
  reval *= rhs;
  return reval;
}

Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::Scalar::operator*(
    Xi::Crypto::EllipticCurve::basePoint_t) const {
  Point reval;
  ge_p3 point;
  ge_scalarmult_base(&point, data());
  ge_p3_tobytes(reval.mutableData(), &point);
  return reval;
}

Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::Scalar::operator*(
    Xi::Crypto::EllipticCurve::commitment_t) const {
  ge_p2 R;
  ge_scalarmult(&R, data(), &ge_p3_H);
  Point aP;
  ge_tobytes(aP.mutableData(), &R);
  return aP;
}

Xi::Crypto::EllipticCurve::Point Xi::Crypto::EllipticCurve::operator*(Xi::Crypto::EllipticCurve::eight_t _,
                                                                      const Xi::Crypto::EllipticCurve::Point &rhs) {
  Point res = rhs;
  res *= _;
  return res;
}

Xi::Crypto::EllipticCurve::Point &Xi::Crypto::EllipticCurve::operator*=(Xi::Crypto::EllipticCurve::Point &out,
                                                                        Xi::Crypto::EllipticCurve::eight_t) {
  ge_p3 p3;
  XI_EXCEPTIONAL_IF_NOT(InvalidArgumentError, ge_frombytes_vartime(&p3, out.data()) == XI_RETURN_CODE_SUCCESS);
  ge_p2 p2;
  ge_p3_to_p2(&p2, &p3);
  ge_p1p1 p1;
  ge_mul8(&p1, &p2);
  ge_p1p1_to_p2(&p2, &p1);
  ge_tobytes(out.mutableData(), &p2);
  return out;
}

Xi::Crypto::EllipticCurve::PointVector Xi::Crypto::EllipticCurve::operator*(
    Xi::Crypto::EllipticCurve::eight_t _, const Xi::Crypto::EllipticCurve::ConstPointSpan &rhs) {
  PointVector reval{};
  reval.reserve(rhs.size());
  for (size_t i = 0; i < rhs.size(); ++i) {
    reval.emplace_back(_ * rhs[i]);
  }
  return reval;
}

Xi::Crypto::EllipticCurve::PointSpan Xi::Crypto::EllipticCurve::operator*=(Xi::Crypto::EllipticCurve::PointSpan &lhs,
                                                                           Xi::Crypto::EllipticCurve::eight_t _) {
  for (size_t i = 0; i < lhs.size(); ++i) {
    lhs[i] *= _;
  }
  return lhs;
}

#define XI_CRYPTO_ECLLIPTIC_CURVE_MAKE_SCALAR_VECTOR_OPERATIONS(OP)                                      \
  Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::operator OP(ConstScalarSpan lhs,    \
                                                                                 ConstScalarSpan rhs) {  \
    XI_EXCEPTIONAL_IF_NOT(InvalidSizeError, lhs.size() == rhs.size());                                   \
    ScalarVector reval{};                                                                                \
    reval.reserve(lhs.size());                                                                           \
    for (size_t i = 0; i < lhs.size(); ++i) {                                                            \
      reval.emplace_back(lhs[i] OP rhs[i]);                                                              \
    }                                                                                                    \
    return reval;                                                                                        \
  }                                                                                                      \
  Xi::Crypto::EllipticCurve::ScalarSpan Xi::Crypto::EllipticCurve::operator OP##=(ScalarSpan lhs,        \
                                                                                  ConstScalarSpan rhs) { \
    XI_EXCEPTIONAL_IF_NOT(InvalidSizeError, lhs.size() == rhs.size());                                   \
    for (size_t i = 0; i < lhs.size(); ++i) {                                                            \
      lhs[i] OP## = rhs[i];                                                                              \
    }                                                                                                    \
    return lhs;                                                                                          \
  }                                                                                                      \
  Xi::Crypto::EllipticCurve::ScalarVector Xi::Crypto::EllipticCurve::operator OP(                        \
      ConstScalarSpan lhs, const Xi::Crypto::EllipticCurve::Scalar &rhs) {                               \
    ScalarVector reval;                                                                                  \
    reval.reserve(lhs.size());                                                                           \
    for (size_t i = 0; i < lhs.size(); ++i) {                                                            \
      reval.emplace_back(lhs[i] OP rhs);                                                                 \
    }                                                                                                    \
    return reval;                                                                                        \
  }                                                                                                      \
  Xi::Crypto::EllipticCurve::ScalarSpan Xi::Crypto::EllipticCurve::operator OP##=(                       \
      ScalarSpan lhs, const Xi::Crypto::EllipticCurve::Scalar &rhs) {                                    \
    for (size_t i = 0; i < lhs.size(); ++i) {                                                            \
      lhs[i] OP## = rhs;                                                                                 \
    }                                                                                                    \
    return lhs;                                                                                          \
  }

XI_CRYPTO_ECLLIPTIC_CURVE_MAKE_SCALAR_VECTOR_OPERATIONS(+)
XI_CRYPTO_ECLLIPTIC_CURVE_MAKE_SCALAR_VECTOR_OPERATIONS(-)
XI_CRYPTO_ECLLIPTIC_CURVE_MAKE_SCALAR_VECTOR_OPERATIONS(*)

#undef XI_CRYPTO_ECLLIPTIC_CURVE_MAKE_SCALAR_VECTOR_OPERATIONS

bool Xi::Crypto::EllipticCurve::random(Xi::Crypto::EllipticCurve::Scalar &res) {
  XI_RETURN_EC_IF(sc_random(res.mutableData()) != XI_RETURN_CODE_SUCCESS, false);
  return true;
}

bool Xi::Crypto::EllipticCurve::random(Xi::ConstByteSpan seed, Xi::Crypto::EllipticCurve::Scalar &res) {
  XI_RETURN_EC_IF(sc_random_deterministic(res.mutableData(), seed.data(), seed.size_bytes()) != XI_RETURN_CODE_SUCCESS,
                  false);
  return false;
}
