// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <memory>

#include <Xi/ErrorModel.hh>
#include <Xi/Span.hpp>
#include <Xi/Memory/SecureBlob.hpp>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "Xi/Crypto/EllipticCurve/Hash.hh"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct Point : Memory::EnableSecureBlobFromThis<const Point, pointSize()> {
 public:
  static Result<Point> generateKeyImage(const Point& publicKey, const struct Scalar& secretKey);
  static Result<Point> generateKeyDerivation(const Point& publicKey,
                                             const struct Scalar& secretKey);
  static Result<Point> fromKeyDerivation(const Point& derivation, uint64_t output_index,
                                         const Point& base);

 public:
  static const Point Null;

  using Memory::EnableSecureBlobFromThis<const Point, pointSize()>::EnableSecureBlobFromThis;

  XI_DEFAULT_MOVE(Point);
  XI_DEFAULT_COPY(Point);

  bool isValid() const;

  operator bool() const;
  bool operator!() const;

  Point operator+(const Point& rhs) const;
  Point& operator+=(const Point& rhs);

 private:
  friend struct Scalar;
  friend Point operator*(eight_t, const Point&);
  friend Point& operator*=(Point&, eight_t);
};

using PointVector = std::vector<Point>;

XI_DECLARE_SPANS(Point)

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
