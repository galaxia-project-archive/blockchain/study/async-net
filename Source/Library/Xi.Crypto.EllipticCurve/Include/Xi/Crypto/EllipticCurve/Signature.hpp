// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>
#include <Xi/Memory/SecureBlob.hpp>

#include "Xi/Crypto/EllipticCurve/Constants.hh"
#include "Xi/Crypto/EllipticCurve/Hash.hh"
#include "Xi/Crypto/EllipticCurve/Point.hpp"
#include "Xi/Crypto/EllipticCurve/SignatureError.hpp"

namespace Xi {
namespace Crypto {
namespace EllipticCurve {

struct Signature : Memory::EnableSecureBlobFromThis<Signature, signatureSize()> {
 public:
  static Result<Signature> sign(const Hash& messageHash, const Point& publicKey,
                                const Scalar& secretKey);
  static Result<Signature> sign(ConstByteSpan message, const Point& publicKey,
                                const Scalar& secretKey);

  using EnableSecureBlobFromThis::EnableSecureBlobFromThis;

  static const Signature Null;

  bool isValid() const;
  operator bool() const;
  bool operator!() const;

  [[nodiscard]] bool validate(const Hash& hashesMessage, const Point& publicKey) const;
  [[nodiscard]] bool validate(ConstByteSpan message, const Point& publicKey) const;

  const Byte* first() const;
  const Byte* second() const;

 private:
  friend struct RingSignature;

  Byte* mutableFirst();
  Byte* mutableSecond();
};

using SignatureVector = std::vector<Signature>;

}  // namespace EllipticCurve
}  // namespace Crypto
}  // namespace Xi
