// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <Xi/Extern/Push.hh>
#include <gmock/gmock.h>
#include <Xi/Extern/Pop.hh>

#include <Xi/Testing/Result.hpp>
#include <Xi/String.hpp>
#include <Xi/Http/QualityValue.hpp>

#define XI_TESTSUITE Xi_Http_QualityValue

TEST(XI_TESTSUITE, LegitStringConversion) {
  using namespace Xi;
  using namespace Xi::Http;
  using namespace Xi::Testing;

  auto oracle = [](const std::string& str, double val) {
    auto value = fromString<QualityValue>(str);
    ASSERT_THAT(value, IsSuccess());
    EXPECT_EQ(*value, QualityValue{val});
  };

  oracle("0", 0.0);
  oracle("0.0", 0.0);
  oracle("0.00", 0.0);
  oracle("0.000", 0.0);
  oracle("1", 1.0);
  oracle("1.0", 1.0);
  oracle("1.00", 1.0);

  oracle("0.8", 0.8);
  oracle("0.78", 0.78);
  oracle("0.9", 0.9);
  oracle("0.112", 0.112);
  oracle("0.010", 0.01);
}
