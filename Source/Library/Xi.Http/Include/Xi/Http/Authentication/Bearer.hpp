// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(BearerAuthentication)
XI_ERROR_CODE_VALUE(TokenIsEmpty, 0x0001)
XI_ERROR_CODE_VALUE(TokenContainsSpaces, 0x000)
XI_ERROR_CODE_END(BearerAuthentication, "Http::BearerAuthenticationError")

class BearerAuthentication {
 public:
  static Result<BearerAuthentication> parse(const std::string& str);

 public:
  XI_DEFAULT_COPY(BearerAuthentication)
  XI_DEFAULT_MOVE(BearerAuthentication)
  ~BearerAuthentication() = default;

  const std::string& token() const;

  std::string stringify() const;

 private:
  explicit BearerAuthentication() = default;

  friend Result<BearerAuthentication> makeBearerAuthentication(const std::string&);

 private:
  std::string m_token{/* */};
};

Result<BearerAuthentication> makeBearerAuthentication(const std::string& token);

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, BearerAuthentication)
