// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <variant>
#include <string>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

#include "Xi/Http/Authentication/Basic.hpp"
#include "Xi/Http/Authentication/Bearer.hpp"

namespace Xi {
namespace Http {

XI_ERROR_CODE_BEGIN(Authentication)
XI_ERROR_CODE_VALUE(Empty, 0x0001)
XI_ERROR_CODE_VALUE(UnknownScheme, 0x0002)
XI_ERROR_CODE_END(Authentication, "Http::AuthenticationError")

struct Authentication : std::variant<BasicAuthentication, BearerAuthentication> {
  static Result<Authentication> parse(const std::string& str);
  std::string stringify() const;

  using variant::variant;
};

}  // namespace Http
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::Http, Authentication)
