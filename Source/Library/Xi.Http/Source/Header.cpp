// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Header.hpp"

#include <Xi/String.hpp>

namespace Xi {
namespace Http {

std::optional<Result<Authentication> > Header::authorization() const {
  const auto search = m_raw.find(HeaderField::Authorization);
  if (search == m_raw.end()) {
    return std::nullopt;
  } else {
    return fromString<Authentication>(search->second);
  }
}

bool Header::contains(const HeaderField field) const {
  return m_raw.find(field) != m_raw.end();
}

}  // namespace Http
}  // namespace Xi
