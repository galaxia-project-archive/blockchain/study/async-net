﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Encoding.hpp"

#include <map>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, Encoding)
XI_ERROR_CODE_DESC(Unknown, "http encoding name is not recognized")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

std::string stringify(const Encoding enc) {
  switch (enc) {
    case Encoding::Gzip:
      return "gzip";
    case Encoding::Compress:
      return "compress";
    case Encoding::Deflate:
      return "deflate";
    case Encoding::Identity:
      return "identity";
    case Encoding::Brotli:
      return "br";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

Result<Encoding> parse(const std::string &str) {
  if (str == "gzip" || str == "x-gzip") {
    XI_SUCCEED(Encoding::Gzip)
  } else if (str == "compress") {
    XI_SUCCEED(Encoding::Compress)
  } else if (str == "deflate") {
    XI_SUCCEED(Encoding::Deflate)
  } else if (str == "identity") {
    XI_SUCCEED(Encoding::Identity)
  } else if (str == "br") {
    XI_SUCCEED(Encoding::Brotli)
  } else {
    XI_FAIL(EncodingError::Unknown)
  }
}

}  // namespace Http
}  // namespace Xi
