// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/Authentication/Basic.hpp"

#include <Xi/String.hpp>
#include <Xi/Encoding/Base64.hh>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, BasicAuthentication)
XI_ERROR_CODE_DESC(NoSeperator, "basic authentication contains no seperator")
XI_ERROR_CODE_DESC(MultipleSeperators, "basic authentication contains multiple serperators")
XI_ERROR_CODE_DESC(InvalidEncoding, "basic authentication is not properly encoded using base64")
XI_ERROR_CODE_DESC(EmptyUsername, "username is empty")
XI_ERROR_CODE_DESC(EmptyPassword, "password is empty")
XI_ERROR_CODE_DESC(InvalidUsername, "username is invalid")
XI_ERROR_CODE_DESC(InvalidPassword, "password is invalid")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

Result<BasicAuthentication> BasicAuthentication::parse(const std::string &str) {
  auto decoded = Encoding::Base64::decode(trim(str));
  XI_FAIL_IF(decoded.isError(), BasicAuthenticationError::InvalidEncoding)
  std::string decodedStr{reinterpret_cast<const char *>(decoded->data()), decoded->size()};
  const auto splitPos = std::find(decodedStr.begin(), decodedStr.end(), ':');
  XI_FAIL_IF(splitPos == decodedStr.end(), BasicAuthenticationError::NoSeperator);
  XI_FAIL_IF(std::find(splitPos, decodedStr.end(), ':') != decodedStr.end(),
             BasicAuthenticationError::MultipleSeperators);
  return makeBasicAuthentication(std::string{decodedStr.begin(), splitPos}, std::string{splitPos, decodedStr.end()});
}

Result<BasicAuthentication> makeBasicAuthentication(const std::string &username, const std::string &password) {
  XI_FAIL_IF(username.empty(), BasicAuthenticationError::EmptyUsername);
  XI_FAIL_IF(username.find(':') != std::string::npos, BasicAuthenticationError::InvalidUsername);
  XI_FAIL_IF(password.empty(), BasicAuthenticationError::EmptyPassword);
  XI_FAIL_IF(password.find(':') != std::string::npos, BasicAuthenticationError::InvalidPassword);

  BasicAuthentication reval{};
  reval.m_username = username;
  reval.m_password = password;
  XI_SUCCEED(std::move(reval))
}

const std::string &BasicAuthentication::username() const {
  return m_username;
}

const std::string &BasicAuthentication::password() const {
  return m_password;
}

std::string BasicAuthentication::stringify() const {
  return Encoding::Base64::encode(asConstByteSpan(toString("{}:{}", username(), password())));
}

}  // namespace Http
}  // namespace Xi
