// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Http/QualityValue.hpp"

#include <sstream>

#include <Xi/String.hpp>
#include <Xi/String/Parse/Number.hh>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Http, QualityValue)
XI_ERROR_CODE_DESC(IllFormed, "quality value is ill formed")
XI_ERROR_CODE_DESC(OutOfRange, "quality value is out of range")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Http {

[[nodiscard]] bool parse_digit(char c, uint32_t &o) {
  XI_RETURN_EC_IF(c < '0', false);
  XI_RETURN_EC_IF(c > '9', false);
  o = static_cast<uint32_t>(c - '0');
  XI_RETURN_SC(true);
}

Result<QualityValue> QualityValue::parse(const std::string &str) {
  XI_FAIL_IF(str.size() < 1 || str.size() > 2 + Digits, QualityValueError::IllFormed);
  value_type native = 0;

  uint32_t buffer = 0;
  XI_FAIL_IF_NOT(parse_digit(str[0], buffer), QualityValueError::IllFormed)
  native += buffer * pow<value_type>(10, Digits);

  XI_FAIL_IF(str.size() > 1 && str[1] != '.', QualityValueError::IllFormed)
  for (uint32_t i = 2; i < str.size(); ++i) {
    XI_FAIL_IF_NOT(parse_digit(str[i], buffer), QualityValueError::IllFormed)
    native += buffer * pow<value_type>(10, Digits + 1 - i);
  }

  XI_FAIL_IF(native > Maximum, QualityValueError::OutOfRange)
  XI_SUCCEED(QualityValue{native})
}

QualityValue::QualityValue(value_type value) : m_value{value} {
  XI_EXCEPTIONAL_IF(OutOfRangeError, value > Maximum);
}

std::string QualityValue::stringify() const {
  if (m_value == Maximum) {
    return "1.0";
  }
  std::ostringstream builder{};
  builder << "0.";
  size_t digit = Digits;
  size_t remainder = m_value;
  do {
    size_t decimal = pow<size_t>(10, --digit);
    builder << (remainder / decimal);
    remainder %= decimal;
  } while (remainder > 0);
  return builder.str();
}

QualityValue::QualityValue(float value) : QualityValue(static_cast<value_type>(value * Maximum)) {
  /* */
}

QualityValue::QualityValue(double value) : QualityValue(static_cast<value_type>(value * Maximum)) {
  /* */
}

bool QualityValue::operator==(const QualityValue &rhs) const {
  return m_value == rhs.m_value;
}

bool QualityValue::operator!=(const QualityValue &rhs) const {
  return m_value != rhs.m_value;
}

bool QualityValue::operator<(const QualityValue &rhs) const {
  return m_value < rhs.m_value;
}

bool QualityValue::operator<=(const QualityValue &rhs) const {
  return m_value <= rhs.m_value;
}

bool QualityValue::operator>(const QualityValue &rhs) const {
  return m_value > rhs.m_value;
}

bool QualityValue::operator>=(const QualityValue &rhs) const {
  return m_value >= rhs.m_value;
}

}  // namespace Http
}  // namespace Xi

size_t std::hash<Xi::Http::QualityValue>::operator()(const Xi::Http::QualityValue &value) const {
  return std::hash<Xi::Http::QualityValue::value_type>{}(value.m_value);
}
