// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Encoding/Base64.hh"

#include <inttypes.h>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>

size_t xi_encoding_base64_encode_length(size_t sourceLength)
{
  return ((sourceLength + 2) / 3) * 4;
}

static const char xi_encoding_base64_encode_lookup[]
    = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int xi_encoding_base64_encode(char *encoded, const xi_byte_t *data, size_t dataLength)
{
  XI_RETURN_SC_IF(dataLength == 0, XI_RETURN_CODE_SUCCESS);
  XI_RETURN_EC_IF(encoded == NULL, XI_BASE64_ENCODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF(data == NULL, XI_BASE64_ENCODE_NULL_ARGUMENT);

  size_t i = 0;
  if(dataLength > 2)
  {
    // Encode regular octlets (3 -> 24bits -> 4 characters)
    for(i = 0; i < dataLength - 2; i += 3)
    {
      //                                            0b11111100.00000000.00000000
      *encoded++ = xi_encoding_base64_encode_lookup[(data[i] >> 2) & 0x3F];
      //                                            0b00000011.11110000.00000000
      *encoded++ = xi_encoding_base64_encode_lookup[((data[i] & 0x03) << 4) | ((data[i + 1] & 0xF0) >> 4)];
      //                                            0b00000000.00001111.11000000
      *encoded++ = xi_encoding_base64_encode_lookup[((data[i + 1] & 0x0F) << 2) | ((data[i + 2] & 0xC0) >> 6)];
      //                                            0b00000000.00000000.00111111
      *encoded++ = xi_encoding_base64_encode_lookup[data[i + 2] & 0x3F];
    }
  }

  // Not dividible by 3, proccess remainders
  if(i < dataLength)
  {
    //                                            0b11111100.00000000.00000000
    *encoded++ = xi_encoding_base64_encode_lookup[(data[i] >> 2) & 0x3F];

    // Last byte missing, only 2 bits remainng
    if(i == dataLength - 1)
    {
      //                                            0b00000011.XXXX0000.00000000
      *encoded++ = xi_encoding_base64_encode_lookup[((data[i] & 0x03) << 4)];
      *encoded++ = '=';
    }
    // Last two byte missing
    else
    {
      //                                            0b00000011.11110000.00000000
      *encoded++ = xi_encoding_base64_encode_lookup[((data[i] & 0x03) << 4) | ((data[i + 1] & 0xF0) >> 4)];
      //                                            0b00000000.00001111.XX000000
      *encoded++ = xi_encoding_base64_encode_lookup[((data[i + 1] & 0x0F) << 2)];
    }
    // 0b00000000.00000000.00XXXXXX
    *encoded++ = '=';
  }

  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

static const xi_byte_t xi_encoding_base64_decode_lookup[256] =
{
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 62, 64, 64, 64, 63,
  52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 64, 64, 64, 64, 64, 64,
  64,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
  15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 64, 64, 64, 64, 64,
  64, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
  41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64,
  64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64
};

int xi_encoding_base64_decode_length(const char *encoded, size_t encodedLength, size_t *out)
{
  if(encodedLength == 0)
  {
    *out = 0;
    XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
  }

  XI_RETURN_EC_IF(encoded == NULL, XI_BASE64_DECODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF(out == NULL, XI_BASE64_DECODE_NULL_ARGUMENT);

  XI_RETURN_EC_IF((encodedLength % 4) > 0, XI_BASE64_DECODE_INVALID_SIZE);

  if(encoded[encodedLength - 1] != '=')
  {
    *out = (encodedLength / 4) * 3;
  }
  else if(encoded[encodedLength - 2] == '=')
  {
    *out = (encodedLength / 4) * 3 - 2;
  }
  else
  {
    *out = (encodedLength / 4) * 3 - 1;
  }

  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}

int xi_encoding_base64_decode(xi_byte_t *decoded, const char *data, size_t dataLength)
{
  XI_RETURN_SC_IF(dataLength == 0, XI_RETURN_CODE_SUCCESS);
  XI_RETURN_EC_IF(decoded == NULL, XI_BASE64_DECODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF(data == NULL, XI_BASE64_DECODE_NULL_ARGUMENT);
  XI_RETURN_EC_IF((dataLength % 4) > 0, XI_BASE64_DECODE_INVALID_SIZE);

  size_t i = 0;
  while(dataLength - i > 4)
  {
    uint8_t chunks[4];
    for(size_t j = 0; j < 4; ++j)
    {
      chunks[j] = xi_encoding_base64_decode_lookup[((const uint8_t*)data)[i++]];
      XI_RETURN_EC_IF(chunks[j] > 63, XI_BASE64_DECODE_INVALID_CHAR);
    }

    *decoded++ = (xi_byte_t)((chunks[0] << 2) | (chunks[1] >> 4));
    *decoded++ = (xi_byte_t)((chunks[1] << 4) | (chunks[2] >> 2));
    *decoded++ = (xi_byte_t)((chunks[2] << 6) | (chunks[3] >> 0));
  }

  if(data[i + 3] != '=')
  {
    uint8_t chunks[4];
    for(size_t j = 0; j < 4; ++j)
    {
      chunks[j] = xi_encoding_base64_decode_lookup[((const uint8_t*)data)[i++]];
      XI_RETURN_EC_IF(chunks[j] > 63, XI_BASE64_DECODE_INVALID_CHAR);
    }

    *decoded++ = (xi_byte_t)((chunks[0] << 2) | (chunks[1] >> 4));
    *decoded++ = (xi_byte_t)((chunks[1] << 4) | (chunks[2] >> 2));
    *decoded++ = (xi_byte_t)((chunks[2] << 6) | (chunks[3] >> 0));
  }
  else if(data[i + 2] == '=')
  {
    uint8_t chunks[2];
    for(size_t j = 0; j < 2; ++j)
    {
      chunks[j] = xi_encoding_base64_decode_lookup[((const uint8_t*)data)[i++]];
      XI_RETURN_EC_IF(chunks[j] > 63, XI_BASE64_DECODE_INVALID_CHAR);
    }

    *decoded++ = (xi_byte_t)((chunks[0] << 2) | (chunks[1] >> 4));
  }
  else
  {
    uint8_t chunks[3];
    for(size_t j = 0; j < 3; ++j)
    {
      chunks[j] = xi_encoding_base64_decode_lookup[((const uint8_t*)data)[i++]];
      XI_RETURN_EC_IF(chunks[j] > 63, XI_BASE64_DECODE_INVALID_CHAR);
    }

    *decoded++ = (xi_byte_t)((chunks[0] << 2) | (chunks[1] >> 4));
    *decoded++ = (xi_byte_t)((chunks[1] << 4) | (chunks[2] >> 2));
  }

  XI_RETURN_SC(XI_RETURN_CODE_SUCCESS);
}
