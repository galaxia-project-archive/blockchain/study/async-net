// Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/Encoding/Base16.hh"

#include <Xi/ErrorModel.hh>

#include <utility>

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::Encoding::Base16, Decode)
XI_ERROR_CODE_DESC(NullArgument, "a null argument was provided to the inner implementation")
XI_ERROR_CODE_DESC(InvalidCharacter, "base16 encoding contains an illegal character")
XI_ERROR_CODE_DESC(InvalidSize, "base16 encoding size is not dividable by 4")
XI_ERROR_CODE_DESC(OutOfMemory, "base16 exceeded maximum storage capacity")
XI_ERROR_CODE_DESC(SizeMissmatch, "base16 encoding size is not expected size")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace Encoding {
namespace Base16 {

std::string encode(ConstByteSpan raw) {
  std::string reval{};
  reval.resize(xi_encoding_base16_encode_length(raw.size()));
  const auto ec = xi_encoding_base16_encode(reval.data(), raw.data(), raw.size());
  XI_EXCEPTIONAL_IF(NullArgumentError, ec == XI_BASE16_ENCODE_NULL_ARGUMENT)
  return reval;
}

size_t encodeSize(const size_t blobSize) {
  return xi_encoding_base16_encode_length(blobSize);
}

Result<ByteVector> decode(const std::string& raw) {
  ByteVector reval{};
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base16_decode_length(raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  reval.resize(revalSize);
  {
    const auto ec = xi_encoding_base16_decode(reval.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess(move(reval));
}

Result<size_t> decode(const std::string& raw, ByteSpan out) {
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base16_decode_length(raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  XI_FAIL_IF(revalSize > out.size(), DecodeError::OutOfMemory) {
    const auto ec = xi_encoding_base16_decode(out.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess(revalSize);
}

Result<void> decodeStrict(const std::string& raw, ByteSpan out) {
  size_t revalSize = 0;
  {
    const auto ec = xi_encoding_base16_decode_length(raw.size(), std::addressof(revalSize));
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
  }
  XI_FAIL_IF_NOT(revalSize == out.size(), DecodeError::OutOfMemory) {
    const auto ec = xi_encoding_base16_decode(out.data(), raw.data(), raw.size());
    XI_FAIL_IF(ec == XI_BASE16_DECODE_NULL_ARGUMENT, DecodeError::NullArgument)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_SIZE, DecodeError::InvalidSize)
    XI_FAIL_IF(ec == XI_BASE16_DECODE_INVALID_CHAR, DecodeError::InvalidCharacter)
  }
  return makeSuccess();
}

}  // namespace Base16
}  // namespace Encoding
}  // namespace Xi
