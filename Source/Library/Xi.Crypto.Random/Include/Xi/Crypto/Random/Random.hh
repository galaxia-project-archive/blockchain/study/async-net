﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/Byte.hh>

#if defined(__cplusplus)
extern "C" {
#endif

#include <stddef.h>

typedef struct xi_crypto_random_state xi_crypto_random_state;

xi_crypto_random_state *xi_crypto_random_state_create(void);

int xi_crypto_random_state_init(xi_crypto_random_state *state);
int xi_crypto_random_state_init_deterministic(xi_crypto_random_state *state, const xi_byte_t *seed, size_t seedLength);
void xi_crypto_random_state_permutation(xi_crypto_random_state *state);
void xi_crypto_random_state_permutation_deterministic(xi_crypto_random_state *state);
int xi_crypto_random_bytes_from_state(xi_byte_t *out, size_t count, xi_crypto_random_state *state);
int xi_crypto_random_bytes_from_state_deterministic(xi_byte_t *out, size_t count, xi_crypto_random_state *state);
void xi_crypto_random_state_destroy(xi_crypto_random_state *state);

int xi_crypto_random_system_bytes(xi_byte_t *out, size_t count);
int xi_crypto_random_bytes(xi_byte_t *out, size_t count);
int xi_crypto_random_bytes_determenistic(xi_byte_t *out, size_t count, const xi_byte_t *seed, size_t seedLength);

#if defined(__cplusplus)
}
#endif

#if defined(__cplusplus)

#include <type_traits>

#include <Xi/ErrorModel.hh>

#include "Xi/Crypto/Random/RandomError.hpp"
#include "Xi/Crypto/Random/Engine.hpp"

namespace Xi {
namespace Crypto {
namespace Random {

Result<ByteVector> generate(size_t count);
Result<ByteVector> generate(size_t count, ConstByteSpan seed);

RandomError generate(ByteSpan out);
RandomError generate(ByteSpan out, ConstByteSpan seed);

}  // namespace Random
}  // namespace Crypto
}  // namespace Xi
#endif
