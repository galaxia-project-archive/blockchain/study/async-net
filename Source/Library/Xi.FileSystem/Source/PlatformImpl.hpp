﻿// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <string>

#include <Xi/ErrorModel.hh>

namespace Xi::FileSystem::Detail {

Result<std::string> executablePath();
Result<std::string> applicationDataDirectory();

}  // namespace Xi::FileSystem::Detail
