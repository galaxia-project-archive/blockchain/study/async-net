// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/FileType.hpp"

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

std::string stringify(const FileType ftype) {
  switch (ftype) {
    case FileType::None:
      return "none";
    case FileType::Regular:
      return "regular";
    case FileType::SymbolicLink:
      return "symbolicLink";
    case FileType::Block:
      return "block";
    case FileType::Character:
      return "character";
    case FileType::Pipe:
      return "pipe";
    case FileType::Socket:
      return "socket";
    case FileType::Unknown:
      return "unknown";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace FileSystem
}  // namespace Xi
