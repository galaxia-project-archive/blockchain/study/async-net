// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/EntryGenerator.hpp"

namespace Xi {
namespace FileSystem {

EntryGenerator::iterator EntryGenerator::begin() const {
  return m_begin;
}

EntryGenerator::iterator EntryGenerator::end() const {
  return m_end;
}

EntryGenerator::iterator EntryGenerator::cbegin() const {
  return m_begin;
}

EntryGenerator::iterator EntryGenerator::cend() const {
  return m_end;
}

EntryGenerator::EntryGenerator(std::filesystem::directory_iterator begin_, std::filesystem::directory_iterator end_)
    : m_begin{begin_}, m_end{end_} {
  /* */
}

}  // namespace FileSystem
}  // namespace Xi
