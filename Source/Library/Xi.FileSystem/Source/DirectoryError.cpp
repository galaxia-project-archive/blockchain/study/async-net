// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/DirectoryError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, Directory)
XI_ERROR_CODE_DESC(Internal, "internal os error")
XI_ERROR_CODE_DESC(IsFile, "directory operation invoked actually points to a file")
XI_ERROR_CODE_DESC(NotFound, "directory was not found, ie. an enviroment variable is missing")
XI_ERROR_CODE_DESC(AlreadyExists, "directory already exists")
XI_ERROR_CODE_CATEGORY_END()
