// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/DirectoryType.hpp"

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

std::string stringify(DirectoryType dtype) {
  switch (dtype) {
    case DirectoryType::None:
      return "none";
    case DirectoryType::Regular:
      return "regular";
    case DirectoryType::SymbolicLink:
      return "symbolicLink";
    case DirectoryType::Unknown:
      return "unknown";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace FileSystem
}  // namespace Xi
