// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/SpecialDirectory.hpp"

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

std::string stringify(const SpecialDirectory dir) {
  switch (dir) {
    case SpecialDirectory::ApplicationData:
      return "ApplicationData";
    case SpecialDirectory::Temporary:
      return "Temporary";
    case SpecialDirectory::Executable:
      return "Executable";
  }
  XI_EXCEPTIONAL(InvalidEnumValueError)
}

}  // namespace FileSystem
}  // namespace Xi
