// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/FileSystemError.hpp"

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, FileSystem)
XI_ERROR_CODE_DESC(NotFound, "file system entry was not found")
XI_ERROR_CODE_CATEGORY_END()
