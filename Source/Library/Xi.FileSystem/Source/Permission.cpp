// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include "Xi/FileSystem/Permission.hpp"

#include <Xi/String.hpp>

namespace fs = std::filesystem;

XI_ERROR_CODE_CATEGORY_BEGIN(Xi::FileSystem, Permission)
XI_ERROR_CODE_DESC(IllFormed, "string representation is ill formed")
XI_ERROR_CODE_CATEGORY_END()

namespace Xi {
namespace FileSystem {

std::string stringify(const Permission perm) {
  std::string reval{9u, '-'};
  if (hasFlag(perm, Permission::OwnerRead)) {
    reval[0] = 'r';
  }
  if (hasFlag(perm, Permission::OwnerWrite)) {
    reval[1] = 'w';
  }
  if (hasFlag(perm, Permission::OwnerExecute)) {
    reval[2] = 'x';
  }
  if (hasFlag(perm, Permission::GroupRead)) {
    reval[3] = 'r';
  }
  if (hasFlag(perm, Permission::GroupWrite)) {
    reval[4] = 'w';
  }
  if (hasFlag(perm, Permission::GroupExecute)) {
    reval[5] = 'x';
  }
  if (hasFlag(perm, Permission::WorldRead)) {
    reval[6] = 'r';
  }
  if (hasFlag(perm, Permission::WorldWrite)) {
    reval[7] = 'w';
  }
  if (hasFlag(perm, Permission::WorldExecute)) {
    reval[8] = 'x';
  }
  return reval;
}

Result<void> parse(const std::string &str, Permission &out) {
  XI_FAIL_IF(str.size() != 9u, PermissionError::IllFormed)
  Permission result = Permission::None;
  if (str[0] != '-') {
    XI_FAIL_IF(str[0] != 'r', PermissionError::IllFormed);
    result |= Permission::OwnerRead;
  }
  if (str[1] != '-') {
    XI_FAIL_IF(str[1] != 'w', PermissionError::IllFormed);
    result |= Permission::OwnerWrite;
  }
  if (str[2] != '-') {
    XI_FAIL_IF(str[2] != 'x', PermissionError::IllFormed);
    result |= Permission::OwnerExecute;
  }
  if (str[3] != '-') {
    XI_FAIL_IF(str[3] != 'r', PermissionError::IllFormed);
    result |= Permission::GroupRead;
  }
  if (str[4] != '-') {
    XI_FAIL_IF(str[4] != 'w', PermissionError::IllFormed);
    result |= Permission::GroupWrite;
  }
  if (str[5] != '-') {
    XI_FAIL_IF(str[5] != 'x', PermissionError::IllFormed);
    result |= Permission::GroupExecute;
  }
  if (str[6] != '-') {
    XI_FAIL_IF(str[6] != 'r', PermissionError::IllFormed);
    result |= Permission::WorldRead;
  }
  if (str[7] != '-') {
    XI_FAIL_IF(str[7] != 'w', PermissionError::IllFormed);
    result |= Permission::WorldWrite;
  }
  if (str[8] != '-') {
    XI_FAIL_IF(str[8] != 'x', PermissionError::IllFormed);
    result |= Permission::WorldExecute;
  }
  out = result;
  XI_SUCCEED();
}

Permission fromStd(std::filesystem::perms stdperms) {
  Permission reval = Permission::None;
  if ((stdperms & fs::perms::owner_read) != fs::perms::none) {
    reval |= Permission::OwnerRead;
  }
  if ((stdperms & fs::perms::owner_write) != fs::perms::none) {
    reval |= Permission::OwnerWrite;
  }
  if ((stdperms & fs::perms::owner_exec) != fs::perms::none) {
    reval |= Permission::OwnerExecute;
  }
  if ((stdperms & fs::perms::group_read) != fs::perms::none) {
    reval |= Permission::GroupRead;
  }
  if ((stdperms & fs::perms::group_write) != fs::perms::none) {
    reval |= Permission::GroupWrite;
  }
  if ((stdperms & fs::perms::group_exec) != fs::perms::none) {
    reval |= Permission::GroupExecute;
  }
  if ((stdperms & fs::perms::others_read) != fs::perms::none) {
    reval |= Permission::WorldRead;
  }
  if ((stdperms & fs::perms::others_write) != fs::perms::none) {
    reval |= Permission::WorldWrite;
  }
  if ((stdperms & fs::perms::others_exec) != fs::perms::none) {
    reval |= Permission::WorldExecute;
  }
  return reval;
}

std::filesystem::perms toStd(Permission perms) {
  fs::perms reval = fs::perms::none;
  if (hasFlag(perms, Permission::OwnerRead)) {
    reval |= fs::perms::owner_read;
  }
  if (hasFlag(perms, Permission::OwnerWrite)) {
    reval |= fs::perms::owner_write;
  }
  if (hasFlag(perms, Permission::OwnerExecute)) {
    reval |= fs::perms::owner_exec;
  }
  if (hasFlag(perms, Permission::OwnerRead)) {
    reval |= fs::perms::group_read;
  }
  if (hasFlag(perms, Permission::GroupWrite)) {
    reval |= fs::perms::group_write;
  }
  if (hasFlag(perms, Permission::GroupExecute)) {
    reval |= fs::perms::group_exec;
  }
  if (hasFlag(perms, Permission::WorldRead)) {
    reval |= fs::perms::others_read;
  }
  if (hasFlag(perms, Permission::WorldWrite)) {
    reval |= fs::perms::others_write;
  }
  if (hasFlag(perms, Permission::WorldExecute)) {
    reval |= fs::perms::others_exec;
  }
  return reval;
}

}  // namespace FileSystem
}  // namespace Xi
