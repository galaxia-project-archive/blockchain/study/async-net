// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <filesystem>

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

/*!
 * \brief maximumSymbolicLinkRedirections The maximum count of continious symbolic links followed until an error is
 * returned.
 * \return maximum count of continious symbolic links supported.
 */
inline constexpr uint8_t maximumSymbolicLinkRedirections() {
  return 32;
}

/*!
 * \brief resolveSymbolicLink Tries to follow symbolic links until an none symbolic entry was found.
 * \param path The path to a potential symbolic link.
 * \param redirectionsThreshold Maximum number of symbolic links to follow, if exceeded an error is returned.
 * \return If one resultion is a file entry or does not exist the path to this entry otherwise an error.
 */
Result<std::filesystem::path> resolveSymbolicLink(const std::filesystem::path& path,
                                                  size_t redirectionsThreshold = maximumSymbolicLinkRedirections());

}  // namespace FileSystem
}  // namespace Xi
