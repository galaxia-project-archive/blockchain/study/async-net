// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <set>
#include <string>

#include <Xi/Global.hh>
#include <Xi/Serialization/Serialization.hpp>

namespace Xi {
namespace FileSystem {

/*!
 * \brief The FileFormat class wraps common extensions being used for a file format.
 */
class FileFormat {
 public:
  /*!
   * \brief The Kind enum describes a kind of format, ie. image, binary or text
   */
  enum struct Kind {
    /// Indicates a text based format.
    Text = 1,
    /// Indicates a binary based format, normally interpretation is application specific.
    Binary,
    /// Indicates the content of the file is compressed.
    Archive,
  };

 public:
  static const FileFormat Csv;
  static const FileFormat Xml;
  static const FileFormat Yaml;
  static const FileFormat Json;

  static const FileFormat Bin;

  static const FileFormat Zip;

 public:
  /*!
   * \brief FileFormat Constructs a new file format description for given extensions.
   * \param kind Format kind, ie. text based
   * \param extensions Extensions being used for this file format. (Should include the dot prefix entry wise)
   */
  explicit FileFormat(const Kind kind, const std::set<std::string>& extensions);

  /// Returns the kind (ie. text, binary) of this format.
  Kind kind() const;
  /// Returns all file extensions considered as a common extension for this format.
  const std::set<std::string>& extensions() const;
  /*!
   * \brief hasExtensions Queries all extensions of this format and checks if on corresponds to the given one.
   * \param The file extennsion to check agains.
   * \return True if one this format extenions corresponds to the given extension.
   */
  bool hasExtension(const std::string& ext) const;

  XI_SERIALIZATION_COMPLEX_EXTERN()
 private:
  Kind m_kind;
  std::set<std::string> m_extensions;
};

XI_SERIALIZATION_ENUM(FileFormat::Kind)

}  // namespace FileSystem
}  // namespace Xi

XI_SERIALIZATION_ENUM_RANGE(Xi::FileSystem::FileFormat::Kind, Text, Archive)
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileFormat::Kind, Text, "text")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileFormat::Kind, Binary, "binary")
XI_SERIALIZATION_ENUM_TAG(Xi::FileSystem::FileFormat::Kind, Archive, "archive")
