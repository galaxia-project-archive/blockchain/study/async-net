// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <Xi/ErrorModel.hh>

namespace Xi {
namespace FileSystem {

XI_ERROR_CODE_BEGIN(SymbolicLink)
/// Symbolic link resolution exceeded maximum redirections threshold.
XI_ERROR_CODE_VALUE(TooManyRedirections, 0x00001)
XI_ERROR_CODE_END(SymbolicLink, "FileSystem::SymbolicLinkError")

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, SymbolicLink)
