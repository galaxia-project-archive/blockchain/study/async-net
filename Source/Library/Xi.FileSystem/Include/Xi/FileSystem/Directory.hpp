// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <filesystem>
#include <string>
#include <optional>

#include <Xi/Global.hh>
#include <Xi/ErrorModel.hh>
#include <Xi/Serialization/Serialization.hpp>

#include "Xi/FileSystem/File.hpp"
#include "Xi/FileSystem/Entry.hpp"
#include "Xi/FileSystem/EntryGenerator.hpp"
#include "Xi/FileSystem/SpecialDirectory.hpp"
#include "Xi/FileSystem/DirectoryType.hpp"
#include "Xi/FileSystem/Permission.hpp"
#include "Xi/FileSystem/SpaceInfo.hpp"
#include "Xi/FileSystem/SegmentGenerator.hpp"

namespace Xi {
namespace FileSystem {

class File;

/*!
 * \brief The Directory class encapsulates the concept of a virtual(symlink) or physical directory on a hard drive.
 *
 * A directory does not necessiarly exist. The concept of a directory may only describe where a directory could exist.
 */
class Directory {
 public:
  static const std::string Seperator;

 public:
  /*!
   * \brief name Returns the last segment of this directory path.
   * \return The base name of this directory.
   */
  std::string name() const;

  /*!
   * \brief parent Queries the parent directory
   * \return None if this directory is a root directory otherwise the parent directory.
   */
  std::optional<Directory> parent() const;

  /*!
   * \brief permissions Queries the current permissions of this directory, only defined iff the directory exist.
   * \return The current file permissions or an error if the directory not exists.
   */
  Result<Permission> permissions() const;

  /*!
   * \brief setPermissions Tries to replace the current directory permissions.
   * \param perms The permissions to apply.
   * \return An error if the operation failed, ie. the directory does not exist.
   */
  Result<void> setPermissions(const Permission perms);

  /*!
   * \brief space Queries the current space information of this directory.
   * \return Space informations of this directory.
   *
   * \note Continous calls will always query the space info again, because they may change.
   */
  Result<SpaceInfo> space() const;

  /*!
   * \brief exists Queries whether this directory exists currently.
   * \return True if the directory exists otherwise false.
   */
  Result<bool> exists() const;

  /*!
   * \brief type Queries the current directory type.
   * \return The directory type if the directory exists and is a valid directory otherwise an error.
   */
  Result<DirectoryType> type() const;

  /*!
   * \brief segments Queries all segments of this directory path.
   * \return An iteratable object of all segments.
   */
  SegmentGenerator segments() const;

  /*!
   * \brief entries Queries all subentries of this direcotry path.
   * \return An iteratable object of all subentries.
   */
  Result<EntryGenerator> entries() const;

  /*!
   * \brief toString Constructs a string path representation of this directory.
   * \return The filesystem path of this directory.
   */
  std::string stringify() const;

  Result<File> relativeFile(const std::filesystem::path& sub) const;
  Result<File> relativeFile(const File& sub) const;
  Result<Directory> relativeDirectory(const std::filesystem::path& sub) const;
  Result<Directory> relativeDirectory(const Directory& sub) const;

  Result<Directory> relativePathTo(const Directory& dir) const;
  Result<File> relativePathTo(const File& file) const;

  /*!
   * \brief createRegular Constructs this directory.
   * \param perms Permissions to apply on the directory.
   * \return Success if the directory was created otherwise an error.
   *
   * \note Intermediate directories are constructed as well.
   */
  Result<void> createRegular(const Permission perms = Permission::OwnerReadWrite);

  /*!
   * \brief createRegularIfNotExists Constructs this directory if it does not already exist.
   * \param perms Permission to apply on the directory.
   * \return Success if the directory was created or already exists, otherwise an error.
   *
   * \note Intermediate directories are constructed as well.
   */
  Result<void> createRegularIfNotExists(const Permission perms = Permission::OwnerReadWrite);

  /*!
   * \brief createSymlink Constructs this directory as a symbolic link to another directory.
   * \param target The other directory this directory shall point to as a symbolic link.
   * \return Success if the symbolic was created otherwise false.
   */
  Result<void> createSymlink(const Directory& target);

  /*!
   * \copydoc createSymlink
   * \param perms Permissions to be set on the symbolic link.
   */
  Result<void> createSymlink(const Directory& target, const Permission perms);

  bool operator==(const Directory& rhs) const;
  bool operator!=(const Directory& rhs) const;

  bool operator<(const Directory& rhs) const;
  bool operator<=(const Directory& rhs) const;
  bool operator>(const Directory& rhs) const;
  bool operator>=(const Directory& rhs) const;

 public:
  explicit Directory() = default;
  XI_DEFAULT_COPY(Directory)
  XI_DEFAULT_MOVE(Directory)
  ~Directory() = default;

 private:
  /*!
   * \brief Directory Constructs a directory for a given path, no check is performed.
   * \param path The directory filesystem path.
   */
  explicit Directory(const std::filesystem::path& path);

  /*!
   * \brief queryStatus Uses the standard routines to query the current directory status.
   * \return The current directory status.
   */
  Result<std::filesystem::file_status> queryStatus() const;

  friend class File;
  friend Result<Directory> makeDirectory(const std::filesystem::path&);

 private:
  /// Stores this directory filesystem path representation.
  std::filesystem::path m_path;
};

/*!
 * \brief makeDirectory Creates a directory from a given path.
 * \param path The filesystem path of the directory to create.
 * \return The correcponding directory entity on success.
 */
Result<Directory> makeDirectory(const std::filesystem::path& path);

/*!
 * \brief makeDirectory Creates a directory from a special kind of system directories.
 * \param special The special directory path to query.
 * \return The corresponding directory entity on success.
 */
Result<Directory> makeDirectory(const SpecialDirectory special);

Result<void> serialize(Directory& value, const Serialization::Tag& name, Serialization::Serializer& serializer);

}  // namespace FileSystem
}  // namespace Xi
