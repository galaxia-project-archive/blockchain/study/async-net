// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#pragma once

#include <cinttypes>
#include <string>
#include <filesystem>

#include <Xi/ErrorModel.hh>
#include <Xi/TypeSafe/Flag.hpp>
#include <Xi/Serialization/Flag.hpp>

namespace Xi {
namespace FileSystem {

XI_ERROR_CODE_BEGIN(Permission)
XI_ERROR_CODE_VALUE(IllFormed, 0x0101)
XI_ERROR_CODE_END(Permission, "FileSystem::PermissionError")

enum struct Permission : uint16_t {
  OwnerRead = 1 << 0,
  OwnerWrite = 1 << 1,
  OwnerExecute = 1 << 3,

  GroupRead = 1 << 4,
  GroupWrite = 1 << 5,
  GroupExecute = 1 << 6,

  WorldRead = 1 << 7,
  WorldWrite = 1 << 8,
  WorldExecute = 1 << 9,

  None = 0,
  OwnerReadWrite = OwnerRead | OwnerWrite,
  OwnerAll = OwnerRead | OwnerWrite | OwnerExecute,
  GroupReadWrite = GroupRead | GroupWrite,
  GroupAll = GroupRead | GroupWrite | GroupExecute,
  WorldReadWrite = WorldRead | WorldWrite,
  WorldAll = WorldRead | WorldWrite | WorldExecute,
};

XI_TYPESAFE_FLAG_MAKE_OPERATIONS(Permission)
XI_SERIALIZATION_FLAG(Permission)

Permission fromStd(std::filesystem::perms stdperms);
std::filesystem::perms toStd(Permission perms);

std::string stringify(const Permission perm);
Result<void> parse(const std::string& str, Permission& out);

}  // namespace FileSystem
}  // namespace Xi

XI_ERROR_CODE_OVERLOADS(Xi::FileSystem, Permission)

XI_SERIALIZATION_FLAG_RANGE(Xi::FileSystem::Permission, OwnerRead, WorldExecute)
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, OwnerRead, "owner_read")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, OwnerWrite, "owner_write")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, OwnerExecute, "owner_execute")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, GroupRead, "group_read")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, GroupWrite, "group_write")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, GroupExecute, "group_execute")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, WorldRead, "world_read")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, WorldWrite, "world_write")
XI_SERIALIZATION_FLAG_TAG(Xi::FileSystem::Permission, WorldExecute, "world_execute")
