// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/FileSystem/File.hpp>

#define XI_TEST_SUITE Xi_FileSystem_File

TEST(XI_TEST_SUITE, CreateTemporary) {
  using namespace Xi::Testing;
  using namespace Xi::FileSystem;

  auto tempFile = makeTemporaryFile();
  ASSERT_THAT(tempFile, IsSuccess());

  auto tempExists = tempFile->exists();
  ASSERT_THAT(tempExists, IsSuccess());
  EXPECT_FALSE(*tempExists);

  auto tempRemoval = tempFile->remove();
  EXPECT_THAT(tempRemoval, IsSuccess());

  auto tempTouch = tempFile->touch();
  ASSERT_THAT(tempTouch, IsSuccess());

  tempExists = tempFile->exists();
  ASSERT_THAT(tempExists, IsSuccess());
  EXPECT_TRUE(*tempExists);

  tempRemoval = tempFile->remove();
  EXPECT_THAT(tempRemoval, IsSuccess());
}
