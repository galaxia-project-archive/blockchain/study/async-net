// Copyright (c) 2019-present by Michael Herwig <michael.herwig@hotmail.de>
// All rights reserved.

#include <gmock/gmock.h>

#include <Xi/Testing/Result.hpp>
#include <Xi/FileSystem/File.hpp>
#include <Xi/FileSystem/Directory.hpp>

#define XI_TEST_SUITE Xi_FileSystem_Directory

TEST(XI_TEST_SUITE, CreateTemporary) {
  using namespace Xi::Testing;
  using namespace Xi::FileSystem;

  auto tempDir = makeDirectory(SpecialDirectory::Temporary);
  ASSERT_THAT(tempDir, IsSuccess());

  auto tempName = makeTemporaryFilename();
  ASSERT_THAT(tempName, IsSuccess());

  auto dir = tempDir->relativeDirectory(*tempName);
  ASSERT_THAT(dir, IsSuccess());
}
