# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_YAMLCPP)
  return()
endif()
set(CMAKE_XI_PACKAGE_YAMLCPP TRUE)

hunter_add_package(yaml-cpp)
find_package(yaml-cpp CONFIG REQUIRED)

mark_as_advanced(
  yaml-cpp_DIR
)
