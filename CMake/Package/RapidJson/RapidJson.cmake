# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_RAPIDJSON)
  return()
endif()
set(CMAKE_XI_PACKAGE_RAPIDJSON TRUE)

hunter_add_package(RapidJSON)
find_package(RapidJSON CONFIG REQUIRED)

mark_as_advanced(
  RapidJSON_DIR
)
