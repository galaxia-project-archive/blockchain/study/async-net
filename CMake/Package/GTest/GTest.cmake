# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_GTEST_GTEST)
    return()
endif()
set(CMAKE_XI_PACKAGE_GTEST_GTEST TRUE)

hunter_add_package(GTest)
find_package(GMock CONFIG REQUIRED)

mark_as_advanced(GMock_DIR)
