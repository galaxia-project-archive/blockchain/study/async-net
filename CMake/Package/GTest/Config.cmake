# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  GTest
  VERSION "1.8.0-hunter-p11"
)
