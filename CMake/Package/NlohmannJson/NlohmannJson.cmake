# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_NLOHMANN_JSON)
  return()
endif()
set(CMAKE_XI_PACKAGE_NLOHMANN_JSON TRUE)

hunter_add_package(nlohmann_json)
find_package(nlohmann_json CONFIG REQUIRED)

mark_as_advanced(
  nlohmann_json_DIR
)
