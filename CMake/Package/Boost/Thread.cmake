# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST_THREAD)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST_THREAD TRUE)

hunter_add_package(
  Boost

  COMPONENTS
    thread
)

find_package(Boost CONFIG REQUIRED thread)