# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_BOOST_CONTEXT)
  return()
endif()
set(CMAKE_XI_PACKAGE_BOOST_CONTEXT TRUE)

hunter_add_package(
  Boost

  COMPONENTS
    context
)

find_package(Boost CONFIG REQUIRED context)