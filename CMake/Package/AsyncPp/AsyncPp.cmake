# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_ASYNCPP)
  return()
endif()
set(CMAKE_XI_PACKAGE_ASYNCPP TRUE)

hunter_add_package(Async++)
find_package(Async++ CONFIG REQUIRED)

if(NOT BUILD_SHARED_LIBS)
  target_compile_definitions(
    Async++::Async++

    INTERFACE
      LIBASYNC_STATIC
  )
endif()

mark_as_advanced(
  Async++_DIR
)
