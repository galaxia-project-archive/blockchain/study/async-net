# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

hunter_config(
  spdlog

  VERSION
    "1.3.1-p0"

  CMAKE_ARGS
    SPDLOG_FMT_EXTERNAL=ON
)
