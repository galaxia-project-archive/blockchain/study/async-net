# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_CXXOPTS)
  return()
endif()
set(CMAKE_XI_PACKAGE_CXXOPTS TRUE)

hunter_add_package(cxxopts)
find_package(cxxopts CONFIG REQUIRED)

mark_as_advanced(
  cxxopts_DIR
)
