# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_PACKAGE_ZLIB)
    return()
endif()
set(CMAKE_XI_PACKAGE_ZLIB TRUE)

hunter_add_package(ZLIB)
find_package(ZLIB CONFIG REQUIRED)


mark_as_advanced(
  ZLIB_DIR
)
