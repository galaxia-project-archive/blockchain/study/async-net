# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_XI_NETWORK)
  return()
endif()
set(CMAKE_XI_LIBRARY_XI_NETWORK TRUE)

xi_include(Xi/Make/Library)
xi_include(Extern/UriParser)
xi_include(Package/Boost)
xi_include(Package/Boost/IoStreams)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Async)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Endianess)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.Network

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Endianess
    Xi::Serialization

  PRIVATE_LIBRARIES
    Xi::Log
    Xi::Async

    uriparser::uriparser
    Boost::boost
    Boost::iostreams

  TEST_LIBRARY
    Xi::Async::Testing
)
