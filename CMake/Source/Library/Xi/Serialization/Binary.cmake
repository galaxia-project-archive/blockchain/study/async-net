# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_SERIALIZATION_BINARY)
  return()
endif()
set(CMAKE_XI_LIBRARY_SERIALIZATION_BINARY TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/TypeSafe)
xi_include(Source/Library/Xi/Endianess)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Encoding)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.Serialization.Binary

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::TypeSafe
    Xi::Endianess
    Xi::Stream
    Xi::Serialization

  PRIVATE_LIBRARIES
    Xi::Encoding
)
