# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_SERIALIZATION_JSON)
  return()
endif()
set(CMAKE_XI_LIBRARY_SERIALIZATION_JSON TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/RapidJson)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Encoding)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.Serialization.Json

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Stream
    Xi::Serialization

  PRIVATE_LIBRARIES
    Xi::Encoding
    RapidJSON::rapidjson
)
