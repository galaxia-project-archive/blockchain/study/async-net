# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_SERIALIZATION_YAML)
  return()
endif()
set(CMAKE_XI_LIBRARY_SERIALIZATION_YAML TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/YamlCpp)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Encoding)
xi_include(Source/Library/Xi/Serialization)

xi_make_library(
  Xi.Serialization.Yaml

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Serialization

  PRIVATE_LIBRARIES
    Xi::Encoding

    yaml-cpp::yaml-cpp
)

mark_as_advanced(
  yaml-cpp_DIR
)
