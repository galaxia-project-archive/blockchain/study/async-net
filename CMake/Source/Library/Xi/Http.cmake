# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_HTTP)
  return()
endif()
set(CMAKE_XI_LIBRARY_HTTP TRUE)

xi_include(Xi/Make/Library)
xi_include(Package/Boost)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/TypeSafe)
xi_include(Source/Library/Xi/Stream)
xi_include(Source/Library/Xi/Encoding)
xi_include(Source/Library/Xi/Log)

xi_make_library(
  Xi.Http

  PUBLIC_LIBRARIES
    Xi::Core
    Xi::Stream
    Xi::TypeSafe

  PRIVATE_LIBRARIES
    Xi::Encoding
    Xi::Log

    Boost::boost
)
