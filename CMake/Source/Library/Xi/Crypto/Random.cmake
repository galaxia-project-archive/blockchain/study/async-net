# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_LIBRARY_CRYPTO_RANDOM)
  return()
endif()
set(CMAKE_XI_LIBRARY_CRYPTO_RANDOM TRUE)

xi_include(Xi/Make/Library)
xi_include(Source/Library/Xi/Core)
xi_include(Source/Library/Xi/Endianess)
xi_include(Source/Library/Xi/Log)
xi_include(Source/Library/Xi/Memory)
xi_include(Source/Library/Xi/Crypto/Hash)

xi_make_library(
  Xi.Crypto.Random

  PUBLIC_LIBRARIES
    Xi::Core

  PRIVATE_LIBRARIES
    Xi::Log
    Xi::Memory
    Xi::Endianess
    Xi::Crypto::Hash
)
