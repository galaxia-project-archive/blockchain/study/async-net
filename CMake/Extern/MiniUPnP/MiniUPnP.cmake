# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_EXTERN_MINIUPNP)
  return()
endif()
set(CMAKE_XI_EXTERN_MINIUPNP TRUE)

set(UPNPC_BUILD_STATIC ON CACHE BOOL "Build static library" FORCE)
set(UPNPC_BUILD_SHARED OFF CACHE BOOL "Build shared library" FORCE)
set(UPNPC_BUILD_TESTS OFF CACHE BOOL "Build test executables" FORCE)
set(UPNPC_BUILD_SAMPLE OFF CACHE BOOL "Build sample executables" FORCE)

add_subdirectory(${XI_ROOT_DIR}/Extern/MiniUPnP/miniupnpc EXCLUDE_FROM_ALL)
target_include_directories(libminiupnpc-static INTERFACE ${XI_ROOT_DIR}/Extern/MiniUPnP)
add_library(miniupnp::miniupnpc ALIAS libminiupnpc-static)

mark_as_advanced(
  UPNPC_BUILD_STATIC
  UPNPC_BUILD_SHARED
  UPNPC_BUILD_TESTS
  UPNPC_BUILD_SAMPLE
  NO_GETADDRINFO
)
