# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_OPTION_TREAT_WARNING_AS_ERROR)
    return()
endif()
set(CMAKE_XI_OPTION_TREAT_WARNING_AS_ERROR TRUE)

option(XI_TREAT_WARNING_AS_ERROR "Iff enabled adds flags to treat compiler warnings as errors." OFF)
