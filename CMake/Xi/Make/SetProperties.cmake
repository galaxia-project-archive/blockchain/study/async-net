# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_SETPROPERTIES)
    return()
endif()
set(CMAKE_XI_MAKE_SETPROPERTIES TRUE)

xi_include(Xi/Version)
xi_include(Xi/Compiler/Classify/Identifier)

# xi_make_set_properties(
#   <target> [<target>...]              << Targets to be configured.
# )
# Sets default properties on targets used by all xi applications.
function(xi_make_set_properties)
  set_target_properties(
    ${ARGN}

    PROPERTIES
      VERSION "${XI_VERSION}"
      ARCHIVE_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib/"
      LIBRARY_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/lib/"
      RUNTIME_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/bin/"
  )

  if(XI_COMPILER_MSVC)
    set_target_properties(
      ${ARGN}

      PROPERTIES
        LINK_FLAGS "/IGNORE:4099"
    )
  endif()
endfunction()
