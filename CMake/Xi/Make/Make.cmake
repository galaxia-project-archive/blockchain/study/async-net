# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_MAKE)
    return()
endif()
set(CMAKE_XI_MAKE_MAKE TRUE)

xi_include(Xi/Make/FileSearch)
xi_include(Xi/Make/SetProperties)
xi_include(Xi/Make/Version)
xi_include(Xi/Make/UnitTest)
xi_include(Xi/Make/Library)
xi_include(Xi/Make/CodeDoc)
