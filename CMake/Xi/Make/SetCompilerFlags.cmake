# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_SET_COMPILER_FLAGS)
  return()
endif()
set(CMAKE_XI_MAKE_SET_COMPILER_FLAGS TRUE)

xi_include(Xi/Compiler)

# xi_make_set_compiler_flags(
#   [<source_file> ...]                 < source files to embedd additional xi compiler flags
# )
#
# This function will determine the source file type and embedd additional xi compiler flags.
function(xi_make_set_compiler_flags)
  foreach(sourceFile ${ARGN})
    get_filename_component(sourceFileExt ${sourceFile} LAST_EXT)
    if(sourceFileExt STREQUAL ".c")
      set_source_files_properties(
        ${sourceFile}

        COMPILE_FLAGS
          ${XI_C_FLAGS}
      )
    elseif(sourceFileExt STREQUAL ".cpp")
      set_source_files_properties(
        ${sourceFile}

        COMPILE_FLAGS
          ${XI_CXX_FLAGS}
        )
    endif()
  endforeach()
endfunction()
