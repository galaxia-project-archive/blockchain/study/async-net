# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_RESOURCE)
  return()
endif()
set(CMAKE_XI_MAKE_RESOURCE TRUE)

include(CMakeParseArguments)

xi_include(Xi/Log)
xi_include(Xi/Make/SetCompilerFlags)
xi_include(Source/Tool/Xi/Resource/Builder)
xi_include(Source/Library/Xi/Resource)

set(XI_RESOURCE_ROOT_DIR "${XI_ROOT_DIR}/Resource" CACHE INTERNAL "" FORCE)

# xi_make_resource(
#   <name>                        < Target name of embedded resources (dots are used to indicate namespaces)
#   [INPUTS <input>...]           < Input files to process
#   [OUTPUT <path>]               < Output path used to emit generated files (default: CURRENT_BINARY_DIR/<name>)
# )
function(xi_make_resource resource_name_)
  cmake_parse_arguments(
    XI_MAKE_RESOURCE
      ""
      "OUTPUT"
      "INPUTS"

    ${ARGN}
  )

  set(resource_name "Resource.${resource_name_}")
  set(resource_target_name "Resource.${resource_name_}.Generate")
  string(REPLACE "." "::" resouce_namespace ${resource_name})
  string(REPLACE "." "/" resouce_namespace_path ${resource_name})

  set(resource_output "${CMAKE_CURRENT_BINARY_DIR}/resource/${resource_name_}")
  if(XI_MAKE_RESOURCE_OUTPUT)
    set(resource_output "${XI_MAKE_RESOURCE_OUTPUT}")
  endif()
  set(resource_include_dir "${resource_output}/Include")
  set(resource_header_dir "${resource_include_dir}/${resource_namespace_path}")
  file(MAKE_DIRECTORY ${resource_header_dir})
  set(resource_source_dir "${resource_output}/Source")
  file(MAKE_DIRECTORY ${resource_source_dir})

  set(resource_inputs)
  set(resource_header_files)
  set(resource_source_files)

  foreach(resource_input_ ${XI_MAKE_RESOURCE_INPUTS})
    set(resource_input ${resource_input_})
    if(NOT EXISTS ${resource_input})
      set(resource_input "${XI_RESOURCE_ROOT_DIR}/${resource_input}")
    endif()
    list(APPEND resource_inputs "${resource_input}")

    get_filename_component(resource_input_name ${resource_input} NAME_WE)
    set(resource_header_file "${resource_header_dir}/${resource_input_name}.hpp")
    list(APPEND resource_header_files ${resource_header_file})
    set(resource_source_file "${resource_source_dir}/${resource_input_name}.cpp")
    list(APPEND resource_source_files ${resource_source_file})

    add_custom_target(
      ${resource_target_name}

      COMMAND
        Xi::Resource::Builder
          --inputs ${resource_input}
          --output ${resource_output}

      OUTPUT
        ${resource_header_file}
        ${resource_source_file}

      SOURCES
        ${resource_input}

      BYPRODUCTS
        ${resource_header_files}
        ${resource_source_files}

      DEPENDS
        Xi::Resource::Builder

      VERBATIM
      USES_TERMINAL
    )
  endforeach()

  xi_make_set_compiler_flags(${resource_header_files} ${resource_source_files})

  add_library(
    ${resource_name}

    STATIC
      ${resource_header_files}
      ${resource_source_files}
  )

  add_dependencies(${resource_name} ${resource_target_name})

  target_link_libraries(
    ${resource_name}

    PRIVATE
      Xi::Resource
  )

  target_include_directories(
    ${resource_name}

    PUBLIC
      ${resource_include_dir}
  )

  target_sources(
    ${resource_name}

    PRIVATE
      ${resource_inputs}
  )

  add_library(${resouce_namespace} ALIAS ${resource_name})
endfunction()
