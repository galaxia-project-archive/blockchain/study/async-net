# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_MAKE_PLACEHOLDER)
  return()
endif()
set(CMAKE_XI_MAKE_PLACEHOLDER TRUE)

set(XI_MAKE_PLACEHOLDER "${CMAKE_CURRENT_LIST_DIR}/Placeholder.cpp" CACHE INTERNAL "" FORCE)
