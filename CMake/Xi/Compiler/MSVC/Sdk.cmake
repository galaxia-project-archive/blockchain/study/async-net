# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_COMPILER_MSVC_SDK)
  return()
endif()
set(CMAKE_XI_COMPILER_MSVC_SDK TRUE)

xi_include(Xi/Log)
xi_include(Xi/Compiler/Flag/Add)

set(XI_WINDOWS_SDK "Windows 7" CACHE STRING
  "\
    Possible values are: \
      * Windows NT 4.0 \
      * Windows 2000 \
      * Windows XP \
      * Windows Server 2003 \
      * Windows Server 2008 \
      * Windows Vista \
      * Windows 7 \
      * Windows 8 \
      * Windows 8.1 \
      * Windows 10 \
  ")


if(XI_WINDOWS_SDK STREQUAL "Windows NT 4.0")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0400")
elseif(XI_WINDOWS_SDK STREQUAL "Windows 2000")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0500")
elseif(XI_WINDOWS_SDK STREQUAL "Windows XP")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0501")
elseif(XI_WINDOWS_SDK STREQUAL "Windows Server 2003")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0600")
elseif(XI_WINDOWS_SDK STREQUAL "Windows Server 2008")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0600")
elseif(XI_WINDOWS_SDK STREQUAL "Windows Vista")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0600")
elseif(XI_WINDOWS_SDK STREQUAL "Windows 7")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0601")
elseif(XI_WINDOWS_SDK STREQUAL "Windows 8")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0602")
elseif(XI_WINDOWS_SDK STREQUAL "Windows 8.1")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0603")
elseif(XI_WINDOWS_SDK STREQUAL "Windows 10")
  set(XI_COMPILER_MSVC_WIN32_WINNT "0x0A00")
else()
  xi_fatal("Unknown windows sdk '${XI_WINDOWS_SDK}'")
endif()

xi_status(
  "Windows Environment:"
    "CMAKE_SYSTEM_VERSION=${CMAKE_SYSTEM_VERSION}"
    "WIN32_WINNT=${XI_COMPILER_MSVC_WIN32_WINNT}"
)

xi_compiler_flag_add(
  "/DWINVER=${XI_COMPILER_MSVC_WIN32_WINNT}"
  "/D_WIN32_WINNT=${XI_COMPILER_MSVC_WIN32_WINNT}"
)
