# Copyright (c) 2018-present by Michael Herwig <michael.herwig@hotmail.de>
# All rights reserved.

if(DEFINED CMAKE_XI_ESCAPE)
  return()
endif()
set(CMAKE_XI_ESCAPE TRUE)

# xi_escape(
#   <out_var>                   < Output variable name to store the escaped string
#   <value>                     < Value to escape
# )
#
# Escape replaces common characters to escape within c++ string.
function(xi_escape out_var value)
  set(result ${value})
  string(REPLACE "\\" "\\\\" value ${value})
  string(REPLACE "\r" "" value ${value})
  string(REPLACE "\t" "\\t" value ${value})
  string(REPLACE "\n" "\\n" value ${value})
  set(${out_var} "${result}" PARENT_SCOPE)
endfunction()
